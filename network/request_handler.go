package network

import (
	"context"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/4s/triaging-service/domain/types"

	log "bitbucket.org/4s/go-logging"
	"bitbucket.org/4s/go-stability"
)

// Client is an interface for HTTPClients.
type Client interface {
	Do(req *http.Request) (*http.Response, error)
}

// RequestHandler is an interface for RequestHandlers
type RequestHandler interface {
	GetCarePlan(
		ctx context.Context,
		identifier types.Identifier,
		session string,
		token string,
	) ([]byte, error)
}

// RestRequestHandler is a struct representing a RequestHandler for HTTP-requests
type RestRequestHandler struct {
	client                Client
	StabilityAgent        stability.Agent
	patientCareServiceURL string
}

// NewRestRequestHandler functions as a constructor that returns a new instance of RestRequestHandler
func NewRestRequestHandler(ctx context.Context, patientCareServiceURL string) RequestHandler {
	stabilityAgent := stability.NewAgent("ThresholdSet")

	requestHandler := RestRequestHandler{
		&http.Client{},
		stabilityAgent,
		patientCareServiceURL,
	}
	return &requestHandler
}

// SetHTTPClient sets the HTTPClient of the RestRequestHandler
func (rh *RestRequestHandler) SetHTTPClient(httpClient Client) {
	rh.client = httpClient
}

// GetCarePlan retrieves CarePlan from server based on identifier
func (rh *RestRequestHandler) GetCarePlan(
	ctx context.Context,
	identifier types.Identifier,
	session string,
	token string,
) ([]byte, error) {

	// execute DIAS setToken request
	err := rh.executeDiasSecurityRequest(ctx, rh.patientCareServiceURL, token, session)
	if err != nil {
		log.WithContext(ctx).Warnf("DIAS setToken request to %v failed with error: %v", rh.patientCareServiceURL, err.Error())
	}

	// compose searchstring
	searchstring := rh.patientCareServiceURL + "/CarePlan?" + "identifier=" + string(*identifier.System) + "|" + string(*identifier.Value)

	var headers = make(map[string]string)
	headers["SESSION"] = session
	headers["Content-Type"] = "application/json"

	body, err := rh.executeRequest(ctx, "GET", searchstring, headers, nil)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func (rh *RestRequestHandler) executeRequest(
	ctx context.Context,
	method string,
	url string,
	headers map[string]string,
	reader io.Reader,
) ([]byte, error) {

	var body []byte
	// execute request with retry exponential backoff and circuitbreaker
	log.WithContext(ctx).Tracef("Executing request %s on url %s", method, url)
	err := rh.StabilityAgent.Do(
		func() error {
			req, err := http.NewRequest(
				method,
				url,
				reader,
			)
			if err != nil {
				return err
			}

			// Add headers
			for k, v := range headers {
				req.Header.Add(k, v)
			}

			// execute request and get response
			resp, err := rh.client.Do(req)
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			if resp.StatusCode < 200 || resp.StatusCode > 299 {
				return errors.New(string(resp.StatusCode) + " - " + resp.Status)
			}

			body, err = ioutil.ReadAll(resp.Body)
			if err != nil {
				log.WithContext(ctx).Error("Error while reading response body", err)
				return err
			}

			return nil
		},
	)
	if err != nil {
		log.WithContext(ctx).Error("An error occurred while executing the request: ", err)
		return nil, err
	}
	return body, nil
}

func (rh *RestRequestHandler) executeDiasSecurityRequest(
	ctx context.Context,
	url string,
	token string,
	sessionID string,
) error {
	if len(token) == 0 {
		return errors.New("SetToken call failed as the token was empty")
	}
	if len(sessionID) == 0 {
		return errors.New("SetToken call failed as the sessionId was empty")
	}
	setTokenURL := strings.Replace(url, "baseR4", "setToken", 1)
	log.WithContext(ctx).Warnf("Executing request %s on url %s, session: %s", "POST", setTokenURL, sessionID)

	client := &http.Client{}
	req, err := http.NewRequest("POST", setTokenURL, strings.NewReader(token))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "text/plain")
	req.Header.Add("SESSION", sessionID)
	req.Header.Add("User-Agent", os.Getenv("SERVICE_NAME"))

	resp, err := client.Do(req)
	defer func() {
		if resp != nil {
			resp.Body.Close()
		}
	}()

	return err
}
