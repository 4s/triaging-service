package mocks

import (
	"context"

	"bitbucket.org/4s/triaging-service/domain/types"
	"bitbucket.org/4s/triaging-service/network"
)

var testCarePlanBundlePath = "../test/testdata/test_careplan_bundle.json"

// MockRequestHandler is a mock version of a RequestHandler
type MockRequestHandler struct {
	file []byte
}

// NewMockRequestHandler functions as a constructor that returns a new instance of MockRequestHandler
func NewMockRequestHandler(ctx context.Context, file []byte) network.RequestHandler {
	mockRequestHandler := MockRequestHandler{
		file,
	}
	return &mockRequestHandler
}

// // GetThresholdSetForObservation returns a thresholdSet for test observation
// func (rh *MockRequestHandler) GetThresholdSetForObservation(
// 	ctx context.Context,
// 	careplanIdentifier types.Identifier,
// 	observationSystem *types.URI,
// 	observationCode *types.Code,
// ) ([]byte, error) {
// 	return rh.file, nil
// }

// // GetThresholdSetForQuestionnaireResponse returns a thresholdSet for test questionnaireResponse
// func (rh *MockRequestHandler) GetThresholdSetForQuestionnaireResponse(
// 	ctx context.Context,
// 	careplanIdentifier types.Identifier,
// 	questionnaireCanonical types.Canonical,
// ) ([]byte, error) {
// 	return rh.file, nil
// }

// GetCarePlan returns a careplan for test
func (rh *MockRequestHandler) GetCarePlan(
	ctx context.Context,
	careplanIdentifier types.Identifier,
	session string,
	token string,
) ([]byte, error) {
	return rh.file, nil
}
