package helpers

import "io/ioutil"

// GetJSON - Get JSON from file
func GetJSON(jsonFile string) ([]byte, error) {

	file, err := ioutil.ReadFile(jsonFile)
	if err != nil {
		return nil, err
	}

	return []byte(file), nil
}
