package helpers

import (
	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
)

// Config is a struct representing the environment variables used in Triaging-service.
type Config struct {
	ServiceName string `env:"SERVICE_NAME"`
	FhirVersion string `env:"FHIR_VERSION"`
	LogLevel    string `env:"LOG_LEVEL"`

	// Kafka
	EnableKafka          string `env:"ENABLE_KAFKA"`
	KafkaBootstrapServer string `env:"KAFKA_BOOTSTRAO"`

	// KafkaConsumer
	KafkaSessionTimeoutMS string `env:"KAFKA_SESSION_TIMEOUT_MS"`
	KafkaGroupID          string `env:"KAFKA_GROUP_ID"`

	// KafkaProducer
	KafkaAcks    string `env:"KAFKA_ACKS"`
	KafkaRetries string `env:"KAFKA_RETRIES"`

	// Consumer health
	EnableHealth     string `env:"ENABLE_HEALTH"`
	HealthFilePath   string `env:"HEALTH_FILE_PATH"`
	HealthIntervalMS string `env:"HEALTH_INTERVAL_MS"`

	// FHIR Dataservices
	PatientCareServiceURL string `env:"PATIENTCARE_SERVICE_URL"`

	// Coding systems
	OfficialTaskIdentifierSystem string `env:"OFFICIAL_TASK_IDENTIFIER_SYSTEM"`
	TaskTypeCodingSystem         string `env:"TASK_TYPE_CODING_SYSTEM"`
	TaskReasonCodeSystem         string `env:"TASK_REASON_CODE_SYSTEM"`

	//Prometheus metrics
	PrometheusPushgateway string `env:"PROMETHEUS_PUSHGATEWAY"`
	PrometheusJob         string `env:"PROMETHEUS_JOB"`
}

// ReadEnvFromFile reads environment variables from a .env file and sets them as environmentvariables on the host
func ReadEnvFromFile(path string) error {
	if err := godotenv.Load(path); err != nil {
		return err
	}
	conf := &Config{}
	err := env.Parse(conf)
	if err != nil {
		return err
	}
	return nil
}
