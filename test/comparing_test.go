package test

import (
	"errors"
	"testing"

	"bitbucket.org/4s/triaging-service/domain/types"
)

var eq = int32(0)
var lt = int32(-1)
var gt = int32(1)

func TestComparator(t *testing.T) {
	testCases := []struct {
		comparison *int32
		operator   types.Code
		want       bool
		wantErr    error
	}{
		{&eq, "exists", true, nil},
		{&eq, "=", true, nil},
		{&eq, "!=", false, nil},
		{&eq, ">", false, nil},
		{&eq, ">=", true, nil},
		{&eq, "<", false, nil},
		{&eq, "<=", true, nil},

		{&gt, "exists", true, nil},
		{&gt, "=", false, nil},
		{&gt, "!=", true, nil},
		{&gt, ">", true, nil},
		{&gt, ">=", true, nil},
		{&gt, "<", false, nil},
		{&gt, "<=", false, nil},

		{&lt, "exists", true, nil},
		{&lt, "=", false, nil},
		{&lt, "!=", true, nil},
		{&lt, ">", false, nil},
		{&lt, ">=", false, nil},
		{&lt, "<", true, nil},
		{&lt, "<=", true, nil},

		{nil, "exists", true, nil},
		{nil, "=", false, nil},
		{nil, "!=", true, nil},
		{nil, ">", false, errors.New("Operator '>' not applicable when values are incomparable")},
		{nil, ">=", false, errors.New("Operator '>=' not applicable when values are incomparable")},
		{nil, "<", false, errors.New("Operator '<' not applicable when values are incomparable")},
		{nil, "<=", false, errors.New("Operator '<=' not applicable when values are incomparable")},
	}
	for _, tc := range testCases {
		result, err := new(types.Comparator).SetComparison(tc.comparison).MatchOperator(tc.operator)
		if err != nil && err.Error() != tc.wantErr.Error() {
			t.Errorf("Comparison failed with wrong error: %v", err.Error())
		}
		if result != tc.want {
			t.Errorf("Comparison: %v, operator: %v. outcome: %v, expected: %v", tc.comparison, tc.operator, result, tc.want)
		}
	}
}

func TestCompareBoolean(t *testing.T) {

	testCases := []struct {
		boolOne types.Boolean
		boolTwo types.Boolean
		want    *int32
	}{
		{true, true, &eq},
		{true, false, nil},
		{false, true, nil},
	}
	for _, tc := range testCases {
		result := tc.boolOne.CompareTo(&tc.boolTwo)
		if result == nil {
			if tc.want != nil {
				t.Errorf(
					"First code: %v, Second code: %v. comparison: %v, expected: %v",
					tc.boolOne,
					tc.boolTwo,
					*result,
					*tc.want,
				)
			}
		} else if *result != *tc.want {
			t.Errorf(
				"First code: %v, Second code: %v. comparison: %v, expected: %v",
				tc.boolOne,
				tc.boolTwo,
				*result,
				*tc.want,
			)
		}
	}
}

func TestCompareCode(t *testing.T) {
	testCases := []struct {
		codeOne types.Code
		codeTwo types.Code
		want    *int32
	}{
		{"same", "same", &eq},
		{"same", "different", nil},
		{"different", "same", nil},
	}
	for _, tc := range testCases {
		result := tc.codeOne.CompareTo(&tc.codeTwo)
		if result == nil {
			if tc.want != nil {
				t.Errorf(
					"First code: %v, Second code: %v. comparison: %v, expected: %v",
					tc.codeOne,
					tc.codeTwo,
					*result,
					*tc.want,
				)
			}
		} else if *result != *tc.want {
			t.Errorf(
				"First code: %v, Second code: %v. comparison: %v, expected: %v",
				tc.codeOne,
				tc.codeTwo,
				*result,
				*tc.want,
			)
		}
	}
}

func TestCompareCodeableConcept(t *testing.T) {
	var system = types.URI("system")
	var otherSystem = types.URI("otherSystem")
	var code = types.Code("code")
	var otherCode = types.Code("otherCode")

	codeableConceptOneCoding := new(types.Coding)
	codeableConceptOneCoding.System = &system
	codeableConceptOneCoding.Code = &code

	codeableConceptOneCodings := new(types.Codings)
	*codeableConceptOneCodings = append(*codeableConceptOneCodings, *codeableConceptOneCoding)

	codeableConceptOne := new(types.CodeableConcept)
	codeableConceptOne.Coding = codeableConceptOneCodings

	codeableConceptTwoCoding := new(types.Coding)
	codeableConceptTwoCoding.System = &system
	codeableConceptTwoCoding.Code = &code

	codeableConceptTwoCodings := new(types.Codings)
	*codeableConceptTwoCodings = append(*codeableConceptTwoCodings, *codeableConceptTwoCoding)

	codeableConceptTwo := new(types.CodeableConcept)
	codeableConceptTwo.Coding = codeableConceptTwoCodings

	codeableConceptThreeCoding := new(types.Coding)
	codeableConceptThreeCoding.System = &otherSystem
	codeableConceptThreeCoding.Code = &otherCode

	codeableConceptThreeCodings := new(types.Codings)
	*codeableConceptThreeCodings = append(*codeableConceptThreeCodings, *codeableConceptThreeCoding)

	codeableConceptThree := new(types.CodeableConcept)
	codeableConceptThree.Coding = codeableConceptThreeCodings

	result := codeableConceptOne.CompareTo(codeableConceptTwo)
	if result == nil || *result != 0 {
		t.Errorf("First codeableConcept: %v, Second codeableConcept: %v, comparison: %v, expected: %v", codeableConceptOne, codeableConceptTwo, result, 0)
	}
	result = codeableConceptOne.CompareTo(codeableConceptThree)
	if result != nil {
		t.Errorf("First codeableConcept: %v, Second codeableConcept: %v, comparison: %v, expected: %v", codeableConceptOne, codeableConceptTwo, result, nil)
	}
}

func TestCompareCoding(t *testing.T) {
	var system = types.URI("system")
	var otherSystem = types.URI("otherSystem")
	var code = types.Code("code")
	var otherCode = types.Code("otherCode")

	codingOne := new(types.Coding)
	codingOne.System = &system
	codingOne.Code = &code

	codingTwo := new(types.Coding)
	codingTwo.System = &system
	codingTwo.Code = &code

	codingThree := new(types.Coding)
	codingThree.System = &otherSystem
	codingThree.Code = &otherCode

	result := codingOne.CompareTo(codingTwo)
	if result == nil || *result != 0 {
		t.Errorf("First coding: %v, Second coding: %v, comparison: %v, expected: %v", codingOne, codingTwo, result, 0)
	}
	result = codingOne.CompareTo(codingThree)
	if result != nil {
		t.Errorf("First coding: %v, Second coding: %v, comparison: %v, expected: %v", codingOne, codingThree, result, nil)
	}
}

func TestCompareDate(t *testing.T) {
	testCases := []struct {
		dateOne types.Date
		dateTwo types.Date
		want    *int32
	}{
		{"1990-05-22", "1990-05-22", &eq},
		{"1990-05-22", "1990-07-22", &lt},
		{"1990-07-22", "1990-05-22", &gt},
	}
	for _, tc := range testCases {
		result, err := tc.dateOne.CompareTo(&tc.dateTwo)
		if err != nil {
			t.Errorf(
				"An error was thrown when comparing dates. First date: %v, Second date: %v. error: %v, expected: %v",
				tc.dateOne,
				tc.dateTwo,
				err.Error(),
				*tc.want,
			)
		}
		if result == nil {
			if tc.want != nil {
				t.Errorf(
					"First date: %v, Second date: %v. comparison: %v, expected: %v",
					tc.dateOne,
					tc.dateTwo,
					result,
					*tc.want,
				)
			}
		} else if *result != *tc.want {
			t.Errorf(
				"First date: %v, Second date: %v. comparison: %v, expected: %v",
				tc.dateOne,
				tc.dateTwo,
				*result,
				tc.want,
			)
		}
	}
}

func TestCompareDatetime(t *testing.T) {
	testCases := []struct {
		dateTimeOne types.DateTime
		dateTimeTwo types.DateTime
		want        *int32
	}{
		{"1990-05-22T05:23:14Z", "1990-05-22T05:23:14Z", &eq},
		{"1990-05-22T05:23:14Z", "1990-07-22T05:23:14Z", &lt},
		{"1990-07-22T05:23:14Z", "1990-05-22T05:23:14Z", &gt},
	}
	for _, tc := range testCases {
		result, err := tc.dateTimeOne.CompareTo(&tc.dateTimeTwo)
		if err != nil {
			t.Errorf(
				"An error was thrown when comparing dates. First code: %v, Second code: %v. error: %v, expected: %v",
				tc.dateTimeOne,
				tc.dateTimeTwo,
				err.Error(),
				*tc.want,
			)
		}
		if result == nil {
			if tc.want != nil {
				t.Errorf(
					"First code: %v, Second code: %v. comparison: %v, expected: %v",
					tc.dateTimeOne,
					tc.dateTimeTwo,
					*result,
					*tc.want,
				)
			}
		} else if *result != *tc.want {
			t.Errorf(
				"First code: %v, Second code: %v. comparison: %v, expected: %v",
				tc.dateTimeOne,
				tc.dateTimeTwo,
				*result,
				tc.want,
			)
		}
	}
}

func TestCompareDecimal(t *testing.T) {
	testCases := []struct {
		decimalOne types.Decimal
		decimalTwo types.Decimal
		want       *int32
	}{
		{0.0, 0.0, &eq},
		{0.0, 1.2, &lt},
		{2.4, 1.2, &gt},
	}
	for _, tc := range testCases {
		result := tc.decimalOne.CompareTo(&tc.decimalTwo)
		if result == nil {
			if tc.want != nil {
				t.Errorf(
					"First decimal: %v, Second decimal: %v. comparison: %v, expected: %v",
					tc.decimalOne,
					tc.decimalTwo,
					result,
					*tc.want,
				)
			}
		} else if *result != *tc.want {
			t.Errorf(
				"First decimal: %v, Second decimal: %v. comparison: %v, expected: %v",
				tc.decimalOne,
				tc.decimalTwo,
				*result,
				tc.want,
			)
		}
	}
}

func TestCompareIdentifier(t *testing.T) {
	var use = types.Code("use")
	var otherUse = types.Code("otherUse")
	var system = types.URI("system")
	var otherSystem = types.URI("otherSystem")
	var value = types.String("value")
	var otherValue = types.String("otherValue")

	identifierOne := new(types.Identifier)
	identifierOne.Use = &use
	identifierOne.System = &system
	identifierOne.Value = &value
	identifierOne.Period = new(types.Period)

	identifierTwo := new(types.Identifier)
	identifierTwo.Use = &use
	identifierTwo.System = &system
	identifierTwo.Value = &value
	identifierTwo.Period = new(types.Period)

	identifierThree := new(types.Identifier)
	identifierThree.Use = &otherUse
	identifierThree.System = &otherSystem
	identifierThree.Value = &otherValue
	identifierThree.Type = new(types.CodeableConcept)
	identifierOne.Period = new(types.Period)

	result, err := identifierOne.CompareTo(identifierTwo)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing identifiers. First identifier: %v, Second identifier: %v. error: %v, expected: %v",
			identifierOne,
			identifierTwo,
			err.Error(),
			0,
		)
	}
	if result == nil || *result != 0 {
		t.Errorf(
			"First identifier: %v, Second identifier: %v, comparison: %v, expected: %v",
			identifierOne,
			identifierTwo,
			result,
			0,
		)
	}
	result, err = identifierOne.CompareTo(identifierThree)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing identifiers. First identifier: %v, Second identifier: %v. error: %v, expected: %v",
			identifierOne,
			identifierThree,
			err.Error(),
			nil,
		)
	}
	if result != nil {
		t.Errorf(
			"First identifier: %v, Second identifier: %v, comparison: %v, expected: %v",
			identifierOne,
			identifierTwo,
			result,
			nil,
		)
	}
}

func TestCompareInteger(t *testing.T) {
	testCases := []struct {
		integerOne types.Integer
		integerTwo types.Integer
		want       *int32
	}{
		{0, 0, &eq},
		{0, 1, &lt},
		{2, 1, &gt},
	}
	for _, tc := range testCases {
		result := tc.integerOne.CompareTo(&tc.integerTwo)
		if result == nil {
			if tc.want != nil {
				t.Errorf(
					"First integer: %v, Second integer: %v. comparison: %v, expected: %v",
					tc.integerOne,
					tc.integerTwo,
					result,
					*tc.want,
				)
			}
		} else if *result != *tc.want {
			t.Errorf(
				"First integer: %v, Second integer: %v. comparison: %v, expected: %v",
				tc.integerOne,
				tc.integerTwo,
				*result,
				tc.want,
			)
		}
	}
}

func TestComparePeriod(t *testing.T) {
	var start = types.DateTime("1990-07-22T05:23:14Z")
	var otherStart = types.DateTime("2002-07-22T05:23:14Z")
	var end = types.DateTime("1990-08-22T05:23:14Z")
	var otherEnd = types.DateTime("2002-07-24T05:23:14Z")

	periodOne := new(types.Period)
	periodOne.Start = &start
	periodOne.End = &end

	periodTwo := new(types.Period)
	periodTwo.Start = &start
	periodTwo.End = &end

	periodThree := new(types.Period)
	periodThree.Start = &otherStart
	periodThree.End = &otherEnd

	result, err := periodOne.CompareTo(periodTwo)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing periods. First period: %v, Second period: %v. error: %v, expected: %v",
			periodOne,
			periodTwo,
			err.Error(),
			0,
		)
	}
	if result == nil || *result != 0 {
		t.Errorf(
			"First period: %v, Second period: %v, comparison: %v, expected: %v",
			periodOne,
			periodTwo,
			result,
			0,
		)
	}
	result, err = periodOne.CompareTo(periodThree)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing periods. First period: %v, Second period: %v. error: %v, expected: %v",
			periodOne,
			periodThree,
			err.Error(),
			nil,
		)
	}
	if result != nil {
		t.Errorf(
			"First period: %v, Second period: %v, comparison: %v, expected: %v",
			periodOne,
			periodThree,
			result,
			nil,
		)
	}
}

func TestCompareQuantity(t *testing.T) {
	var value = types.Decimal(1.2)
	var otherValue = types.Decimal(2.5)
	var comparator = types.Code("=")
	var otherComparator = types.Code("!=")
	var code = types.Code("kg")
	var otherCode = types.Code("lb")
	var system = types.URI("system")
	var otherSystem = types.URI("otherSystem")

	quantityOne := new(types.Quantity)
	quantityOne.Value = &value
	quantityOne.Comparator = &comparator
	quantityOne.Code = &code
	quantityOne.System = &system

	quantityTwo := new(types.Quantity)
	quantityTwo.Value = &value
	quantityTwo.Comparator = &comparator
	quantityTwo.Code = &code
	quantityTwo.System = &system

	quantityThree := new(types.Quantity)
	quantityThree.Value = &otherValue
	quantityThree.Comparator = &otherComparator
	quantityThree.Code = &otherCode
	quantityThree.System = &otherSystem

	result, err := quantityOne.CompareTo(quantityTwo)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing quantitys. First quantity: %v, Second quantity: %v. error: %v, expected: %v",
			quantityOne,
			quantityTwo,
			err.Error(),
			0,
		)
	}
	if result == nil || *result != 0 {
		t.Errorf(
			"First quantity: %v, Second quantity: %v, comparison: %v, expected: %v",
			quantityOne,
			quantityTwo,
			result,
			0,
		)
	}
	result, err = quantityOne.CompareTo(quantityThree)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing quantitys. First quantity: %v, Second quantity: %v. error: %v, expected: %v",
			quantityOne,
			quantityThree,
			err.Error(),
			nil,
		)
	}
	if result != nil {
		t.Errorf(
			"First quantity: %v, Second quantity: %v, comparison: %v, expected: %v",
			quantityOne,
			quantityThree,
			result,
			nil,
		)
	}
}

func TestCompareReference(t *testing.T) {
	var reference = types.String("reference")
	var otherReference = types.String("otherReference")
	var referenceType = types.String("type")
	var otherReferenceType = types.String("otherType")
	var display = types.String("display")
	var otherDisplay = types.String("otherDisplay")

	referenceOne := new(types.Reference)
	referenceOne.Reference = &reference
	referenceOne.Reference = &referenceType
	referenceOne.Display = &display

	referenceTwo := new(types.Reference)
	referenceTwo.Reference = &reference
	referenceTwo.Reference = &referenceType
	referenceTwo.Display = &display

	referenceThree := new(types.Reference)
	referenceThree.Reference = &otherReference
	referenceThree.Reference = &otherReferenceType
	referenceThree.Display = &otherDisplay

	result, err := referenceOne.CompareTo(referenceTwo)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing references. First reference: %v, Second reference: %v. error: %v, expected: %v",
			"referenceOne",
			"referenceTwo",
			err.Error(),
			0,
		)
	}
	if result == nil || *result != 0 {
		t.Errorf(
			"First reference: %v, Second reference: %v, comparison: %v, expected: %v",
			"referenceOne",
			"referenceTwo",
			result,
			0,
		)
	}
	result, err = referenceOne.CompareTo(referenceThree)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing references. First reference: %v, Second reference: %v. error: %v, expected: %v",
			"referenceOne",
			"referenceThree",
			err.Error(),
			nil,
		)
	}
	if result != nil {
		t.Errorf(
			"First reference: %v, Second reference: %v, comparison: %v, expected: %v",
			"referenceOne",
			"referenceThree",
			*result,
			nil,
		)
	}
}

func TestCompareRange(t *testing.T) {
	var quantityLowValue = types.Decimal(2.3)
	var otherQuantityLowValue = types.Decimal(65.5)
	var quantityLowSystem = types.URI("http://unitsofmeasure.org ")
	var otherQuantityLowSystem = types.URI("http://loinc.org ")
	var quantityLowCode = types.Code("kg")
	var otherQuantityLowCode = types.Code("29463-7")

	var quantityHighValue = types.Decimal(5.7)
	var otherQuantityHighValue = types.Decimal(75.2)
	var quantityHighSystem = types.URI("http://unitsofmeasure.org ")
	var otherQuantityHighSystem = types.URI("http://loinc.org ")
	var quantityHighCode = types.Code("kg")
	var otherQuantityHighCode = types.Code("29463-7 ")

	quantityLow := new(types.Quantity)
	quantityLow.Value = &quantityLowValue
	quantityLow.System = &quantityLowSystem
	quantityLow.Code = &quantityLowCode

	otherQuantityLow := new(types.Quantity)
	otherQuantityLow.Value = &otherQuantityLowValue
	otherQuantityLow.System = &otherQuantityLowSystem
	otherQuantityLow.Code = &otherQuantityLowCode

	quantityHigh := new(types.Quantity)
	quantityHigh.Value = &quantityHighValue
	quantityHigh.System = &quantityHighSystem
	quantityHigh.Code = &quantityHighCode

	otherQuantityHigh := new(types.Quantity)
	otherQuantityHigh.Value = &otherQuantityHighValue
	otherQuantityHigh.System = &otherQuantityHighSystem
	otherQuantityHigh.Code = &otherQuantityHighCode

	var rangeOne = new(types.Range)
	rangeOne.Low = quantityLow
	rangeOne.High = quantityHigh

	var rangeTwo = new(types.Range)
	rangeTwo.Low = quantityLow
	rangeTwo.High = quantityHigh

	var rangeThree = new(types.Range)
	rangeThree.Low = otherQuantityLow
	rangeThree.High = otherQuantityHigh

	result, err := rangeOne.CompareTo(rangeTwo)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing ranges. First range: %v, Second range: %v. error: %v, expected: %v",
			"rangeOne",
			"rangeTwo",
			err.Error(),
			0,
		)
	}
	if result == nil || *result != 0 {
		t.Errorf(
			"First range: %v, Second range: %v, comparison: %v, expected: %v",
			"rangeOne",
			"rangeTwo",
			result,
			0,
		)
	}
	result, err = rangeOne.CompareTo(rangeThree)
	if err != nil {
		t.Errorf(
			"An error was thrown when comparing ranges. First range: %v, Second range: %v. error: %v, expected: %v",
			"rangeOne",
			"rangeThree",
			err.Error(),
			nil,
		)
	}
	if result != nil {
		t.Errorf(
			"First range: %v, Second range: %v, comparison: %v, expected: %v",
			"rangeOne",
			"rangeThree",
			*result,
			nil,
		)
	}
}

func TestCompareString(t *testing.T) {
	testCases := []struct {
		stringOne types.String
		stringTwo types.String
		want      *int32
	}{
		{"string", "string", &eq},
		{"string", "otherString", nil},
		{"otherString", "string", nil},
	}
	for _, tc := range testCases {
		result := tc.stringOne.CompareTo(&tc.stringTwo)
		if result == nil {
			if tc.want != nil {
				t.Errorf(
					"First string: %v, Second string: %v. comparison: %v, expected: %v",
					tc.stringOne,
					tc.stringTwo,
					result,
					*tc.want,
				)
			}
		} else if *result != *tc.want {
			t.Errorf(
				"First string: %v, Second string: %v. comparison: %v, expected: %v",
				tc.stringOne,
				tc.stringTwo,
				*result,
				tc.want,
			)
		}
	}
}

func TestCompareTime(t *testing.T) {
	testCases := []struct {
		timeOne types.Time
		timeTwo types.Time
		want    *int32
	}{
		{"07:14:33", "07:14:33", &eq},
		{"07:14:33", "13:22:24", &lt},
		{"13:22:24", "07:14:33", &gt},
	}
	for _, tc := range testCases {
		result, err := tc.timeOne.CompareTo(&tc.timeTwo)
		if err != nil {
			t.Errorf(
				"An error was thrown when comparing times. First time: %v, Second time: %v. error: %v, expected: %v",
				tc.timeOne,
				tc.timeTwo,
				err.Error(),
				*tc.want,
			)
		}
		if result == nil {
			if tc.want != nil {
				t.Errorf(
					"First time: %v, Second time: %v. comparison: %v, expected: %v",
					tc.timeOne,
					tc.timeTwo,
					result,
					*tc.want,
				)
			}
		} else if *result != *tc.want {
			t.Errorf(
				"First time: %v, Second time: %v. comparison: %v, expected: %v",
				tc.timeOne,
				tc.timeTwo,
				*result,
				tc.want,
			)
		}
	}
}

func TestCompareURI(t *testing.T) {
	testCases := []struct {
		URIOne types.URI
		URITwo types.URI
		want   *int32
	}{
		{"urn:oasis:names:specification:docbook:dtd:xml:4.1.2", "urn:oasis:names:specification:docbook:dtd:xml:4.1.2", &eq},
		{"urn:oasis:names:specification:docbook:dtd:xml:4.1.2", "ldap://[2001:db8::7]/c=GB?objectClass?one", nil},
		{"ldap://[2001:db8::7]/c=GB?objectClass?one", "urn:oasis:names:specification:docbook:dtd:xml:4.1.2", nil},
	}
	for _, tc := range testCases {
		result := tc.URIOne.CompareTo(&tc.URITwo)
		if result == nil {
			if tc.want != nil {
				t.Errorf(
					"First URI: %v, Second URI: %v. comparison: %v, expected: %v",
					tc.URIOne,
					tc.URITwo,
					result,
					*tc.want,
				)
			}
		} else if *result != *tc.want {
			t.Errorf(
				"First URI: %v, Second URI: %v. comparison: %v, expected: %v",
				tc.URIOne,
				tc.URITwo,
				*result,
				tc.want,
			)
		}
	}
}
