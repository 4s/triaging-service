package test

import (
	"context"
	"errors"
	"os"
	"strings"
	"testing"
	"time"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	mock "bitbucket.org/4s/go-messaging/implementations/mock"
	"bitbucket.org/4s/triaging-service/metrics"
	"bitbucket.org/4s/triaging-service/network"
	"bitbucket.org/4s/triaging-service/network/mocks"
	"bitbucket.org/4s/triaging-service/processing"
	"bitbucket.org/4s/triaging-service/service"
	"bitbucket.org/4s/triaging-service/test/helpers"
)

var qrE2ETestVariables struct {
	// context variables
	ctx context.Context
	// producer variables
	producerTopic messaging.Topic
	mockProducer  mock.EventProducer
	// consumer variables
	consumerTopics messaging.Topics
	mockConsumer   mock.EventConsumer
	// processing variables
	timeout                   time.Duration
	testQuestionnaireResponse string
	testCarePlanBundle        string
	eventProcessor            processing.MyEventProcessor
	requestHandler            network.RequestHandler
	triagingService           service.TriagingService
}

func TestQuestionnaireResponseE2E(t *testing.T) {
	// initialize test variables
	err := initE2EQR()
	if err != nil {
		t.Errorf("Test failed: %v", err.Error())
	}

	// create topic for testProducer
	qrE2ETestVariables.producerTopic = messaging.NewTopic()
	qrE2ETestVariables.producerTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")
	log.WithContext(qrE2ETestVariables.ctx).Debug("Producer topic set to: " + qrE2ETestVariables.producerTopic.String())

	// create test message
	message := messaging.NewMessage()
	message.SetBodyType("FHIR").
		SetContentVersion("R4").
		SetCorrelationID("wF1FdPFP1X8").
		SetTransactionID("488f8476-fa5d-44e3-8f26-e185f08c3bae")
	json, err := helpers.GetJSON(qrE2ETestVariables.testQuestionnaireResponse)
	if err != nil {
		t.Errorf("Could not create get QuestionnaireResponse json: %v", err.Error())
	}
	message.SetBody(string(json))

	// produce testmessage
	err = qrE2ETestVariables.mockProducer.Produce(qrE2ETestVariables.producerTopic, message)
	if err != nil {
		t.Errorf("Error while producing testmessage: %v", err.Error())
	}

	// run triagingservice
	err = qrE2ETestVariables.triagingService.Triage(qrE2ETestVariables.ctx)
	if err != nil {
		t.Errorf("Error while running Triage: %v", err)
	}

	// wait for producer to produce message to outputtopic
	outputtopic := messaging.NewTopic()
	outputtopic.SetOperation(messaging.Update).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")
	future := qrE2ETestVariables.mockProducer.GetFutureTopic(qrE2ETestVariables.ctx, outputtopic, qrE2ETestVariables.timeout)
	mess := <-future

	// test outputted message
	if mess == (messaging.Message{}) {
		t.Errorf("No message received from Kafka")
	}
	if !strings.Contains(mess.GetBody(), string(`{"url":"https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/111771649/QuestionnaireResponse+interpretation+extension","valueCodeableConcept":{"coding":[{"system":"http://hl7.org/fhir/v2/0078","code":"N","display":"Normal"}]}}`)) {
		t.Errorf("Message body does not contain a pathological interpretation")
		t.Errorf(mess.GetBody())
	}
	if !strings.Contains(mess.GetBody(), string(`{"url":"https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/111706117/Outcome+thresholdSetCanonical+extension","valueCanonical":"canonical://uuid/Basic/3b3754cf-91c1-4e1d-a879-32fef7f1f177|1.0.0"}`)) {
		t.Errorf("Message body does not contain a canonical reference to threshold")
		t.Errorf(mess.GetBody())
	}
}

func initE2EQR() error {
	// create ctx for logging
	qrE2ETestVariables.ctx = context.Background()
	var err error

	qrE2ETestVariables.testQuestionnaireResponse = "./testdata/test_questionnaire_response.json"
	qrE2ETestVariables.testCarePlanBundle = "./testdata/test_careplan_bundle_qr.json"
	qrE2ETestVariables.timeout = time.Duration(5 * time.Millisecond)

	// create EventProcessor
	qrE2ETestVariables.eventProcessor, err = processing.NewMyEventProcessor(qrE2ETestVariables.ctx)
	if err != nil {
		return errors.New("Could not create myEventProcessor: " + err.Error())
	}

	mockCarePlanBundle, err := helpers.GetJSON(qrE2ETestVariables.testCarePlanBundle)
	if err != nil {
		log.WithContext(qrE2ETestVariables.ctx).Error("Error while loading test thresholdset from file")
		os.Exit(1)
	}
	qrE2ETestVariables.requestHandler = mocks.NewMockRequestHandler(qrE2ETestVariables.ctx, mockCarePlanBundle)
	qrE2ETestVariables.eventProcessor.SetRequestHandler(qrE2ETestVariables.requestHandler)

	// create MockConsumer
	qrE2ETestVariables.mockConsumer, err = mock.NewEventConsumer(qrE2ETestVariables.ctx, qrE2ETestVariables.eventProcessor)
	if err != nil {
		return errors.New("Could not create testConsumer: " + err.Error())
	}

	// create MockProducer
	qrE2ETestVariables.mockProducer, err = mock.NewEventProducer(qrE2ETestVariables.ctx, "mock producer")
	if err != nil {
		return errors.New("Could not create testProducer: " + err.Error())
	}

	// create consumerTopics
	observationTopic := messaging.NewTopic()
	observationTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("Observation")

	questionnaireResponseTopic := messaging.NewTopic()
	questionnaireResponseTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")

	qrE2ETestVariables.consumerTopics = append(qrE2ETestVariables.consumerTopics, observationTopic)
	qrE2ETestVariables.consumerTopics = append(qrE2ETestVariables.consumerTopics, questionnaireResponseTopic)
	qrE2ETestVariables.mockConsumer.SubscribeTopics(qrE2ETestVariables.consumerTopics)

	// Create triaging service
	qrE2ETestVariables.triagingService = service.NewTriagingService(qrE2ETestVariables.ctx)

	// create consumeAndProcess with mockConsumer and mockProducer
	consumeAndProcess, err := messaging.NewConsumeAndProcess(
		qrE2ETestVariables.ctx,
		&qrE2ETestVariables.mockConsumer,
		&qrE2ETestVariables.mockProducer,
	)
	if err != nil {
		return errors.New("Cannot create ConsumeAndProcess: " + err.Error())
	}

	interceptorAdaptor, err := metrics.NewPrometheusInterceptorAdaptor(context.Background())
	if err == nil {
		consumeAndProcess.SetInterceptors(interceptorAdaptor)
	}

	// set consumeAndProcess for triagingservice
	qrE2ETestVariables.triagingService.SetConsumeAndProcess(consumeAndProcess)

	// set timeout for service
	qrE2ETestVariables.triagingService.SetTimeout(qrE2ETestVariables.timeout)

	return nil
}
