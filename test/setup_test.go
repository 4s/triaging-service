package test

import (
	"fmt"
	"os"
	"testing"

	"bitbucket.org/4s/triaging-service/test/helpers"
)

// TestMain sets up the testing environment
func TestMain(m *testing.M) {
	fmt.Println("Setting up tests")
	err := helpers.ReadEnvFromFile("../service.env")
	if err != nil {
		fmt.Println("Error while reading from service.env. Shutting down")
		os.Exit(2)
	}
	err2 := helpers.ReadEnvFromFile("../deploy.env")
	if err2 != nil {
		fmt.Println("Error while reading from deploy.env. Shutting down")
		os.Exit(2)
	}
	code := m.Run()
	os.Exit(code)
}
