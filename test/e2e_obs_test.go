package test

import (
	"context"
	"errors"
	"os"
	"strings"
	"testing"
	"time"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	mock "bitbucket.org/4s/go-messaging/implementations/mock"
	"bitbucket.org/4s/triaging-service/network"
	"bitbucket.org/4s/triaging-service/network/mocks"
	"bitbucket.org/4s/triaging-service/processing"
	"bitbucket.org/4s/triaging-service/service"
	"bitbucket.org/4s/triaging-service/test/helpers"
)

var obsE2ETestVariables struct {
	// context variables
	ctx context.Context
	// producer variables
	producerTopic messaging.Topic
	mockProducer  mock.EventProducer
	// consumer variables
	consumerTopics messaging.Topics
	mockConsumer   mock.EventConsumer
	// processing variables
	timeout         time.Duration
	eventProcessor  processing.MyEventProcessor
	requestHandler  network.RequestHandler
	triagingService service.TriagingService
}

func TestObservationE2E(t *testing.T) {
	// initialize test variables

	testObservation := "./testdata/test_observation.json"
	testCarePlanBundle := "./testdata/test_careplan_bundle_obs.json"
	err := initE2EObs(testObservation, testCarePlanBundle)
	if err != nil {
		t.Errorf("Test failed: %v", err.Error())
	}

	// create topic for testProducer
	obsE2ETestVariables.producerTopic = messaging.NewTopic()
	obsE2ETestVariables.producerTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("Observation")
	log.WithContext(obsE2ETestVariables.ctx).Debug("Producer topic set to: " + obsE2ETestVariables.producerTopic.String())

	// create test message
	message := messaging.NewMessage()
	message.SetBodyType("FHIR").
		SetContentVersion("R4").
		SetCorrelationID("wF1FdPFP1X8").
		SetTransactionID("488f8476-fa5d-44e3-8f26-e185f08c3bae")
	json, err := helpers.GetJSON(testObservation)
	if err != nil {
		t.Errorf("Could not create get Observation json: %v", err.Error())
	}
	message.SetBody(string(json))

	// produce testmessage
	err = obsE2ETestVariables.mockProducer.Produce(obsE2ETestVariables.producerTopic, message)
	if err != nil {
		t.Errorf("Error while producing testmessage: %v", err.Error())
	}

	// run triagingservice
	err = obsE2ETestVariables.triagingService.Triage(obsE2ETestVariables.ctx)
	if err != nil {
		t.Errorf("Error while running Triage: %v", err)
	}

	// wait for producer to produce message to outputtopic
	outputtopic := messaging.NewTopic()
	outputtopic.SetOperation(messaging.Update).
		SetDataCategory(messaging.FHIR).
		SetDataType("Observation")
	future := obsE2ETestVariables.mockProducer.GetFutureTopic(obsE2ETestVariables.ctx, outputtopic, obsE2ETestVariables.timeout)
	mess := <-future

	if mess == (messaging.Message{}) {
		t.Errorf("No message received from Kafka")
	}
	if !strings.Contains(mess.GetBody(), string(`"interpretation":[{"coding":[{"system":"http://hl7.org/fhir/v2/0078","code":"N","display":"Normal"}]}]`)) {
		t.Errorf("Message body does not contain a normal interpretation")
		t.Errorf(mess.GetBody())
	}
}

func TestObservationE2EComponent(t *testing.T) {
	// initialize test variables

	testObservation := "./testdata/test_observation_component.json"
	testCarePlanBundle := "./testdata/test_careplan_bundle_obs_component.json"
	err := initE2EObs(testObservation, testCarePlanBundle)
	if err != nil {
		t.Errorf("Test failed: %v", err.Error())
	}

	// create topic for testProducer
	obsE2ETestVariables.producerTopic = messaging.NewTopic()
	obsE2ETestVariables.producerTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("Observation")
	log.WithContext(obsE2ETestVariables.ctx).Debug("Producer topic set to: " + obsE2ETestVariables.producerTopic.String())

	// create test message
	message := messaging.NewMessage()
	message.SetBodyType("FHIR").
		SetContentVersion("R4").
		SetCorrelationID("wF1FdPFP1X8").
		SetTransactionID("488f8476-fa5d-44e3-8f26-e185f08c3bae")
	json, err := helpers.GetJSON(testObservation)
	if err != nil {
		t.Errorf("Could not create get Observation json: %v", err.Error())
	}
	message.SetBody(string(json))

	// produce testmessage
	err = obsE2ETestVariables.mockProducer.Produce(obsE2ETestVariables.producerTopic, message)
	if err != nil {
		t.Errorf("Error while producing testmessage: %v", err.Error())
	}

	// run triagingservice
	err = obsE2ETestVariables.triagingService.Triage(obsE2ETestVariables.ctx)
	if err != nil {
		t.Errorf("Error while running Triage: %v", err)
	}

	// wait for producer to produce message to outputtopic
	outputtopic := messaging.NewTopic()
	outputtopic.SetOperation(messaging.Update).
		SetDataCategory(messaging.FHIR).
		SetDataType("Observation")
	future := obsE2ETestVariables.mockProducer.GetFutureTopic(obsE2ETestVariables.ctx, outputtopic, obsE2ETestVariables.timeout)
	mess := <-future

	if mess == (messaging.Message{}) {
		t.Errorf("No message received from Kafka")
	}
	if !strings.Contains(mess.GetBody(), string(`"interpretation":[{"coding":[{"system":"http://hl7.org/fhir/v2/0078","code":"N","display":"Normal"}]}]`)) {
		t.Errorf("Message body does not contain a normal interpretation")
		t.Errorf(mess.GetBody())
	}
}

func initE2EObs(testObservation string, testCarePlanBundle string) error {
	// create ctx for logging
	obsE2ETestVariables.ctx = context.Background()
	var err error

	obsE2ETestVariables.timeout = time.Duration(20 * time.Millisecond)

	// create ctx for logging

	// create EventProcessor
	obsE2ETestVariables.eventProcessor, err = processing.NewMyEventProcessor(obsE2ETestVariables.ctx)
	if err != nil {
		return errors.New("Could not create myEventProcessor: " + err.Error())
	}

	mockCarePlanBundle, err := helpers.GetJSON(testCarePlanBundle)
	if err != nil {
		log.WithContext(obsE2ETestVariables.ctx).Error("Error while loading test thresholdset from file")
		os.Exit(1)
	}
	obsE2ETestVariables.requestHandler = mocks.NewMockRequestHandler(obsE2ETestVariables.ctx, mockCarePlanBundle)
	obsE2ETestVariables.eventProcessor.SetRequestHandler(obsE2ETestVariables.requestHandler)

	// create MockConsumer
	obsE2ETestVariables.mockConsumer, err = mock.NewEventConsumer(obsE2ETestVariables.ctx, obsE2ETestVariables.eventProcessor)
	if err != nil {
		return errors.New("Could not create testConsumer: " + err.Error())
	}

	// create MockProducer
	obsE2ETestVariables.mockProducer, err = mock.NewEventProducer(obsE2ETestVariables.ctx, "mock producer")
	if err != nil {
		return errors.New("Could not create testProducer: " + err.Error())
	}

	// create consumerTopics
	observationTopic := messaging.NewTopic()
	observationTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("Observation")

	questionnaireResponseTopic := messaging.NewTopic()
	questionnaireResponseTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("Observation")

	obsE2ETestVariables.consumerTopics = append(obsE2ETestVariables.consumerTopics, observationTopic)
	obsE2ETestVariables.consumerTopics = append(obsE2ETestVariables.consumerTopics, questionnaireResponseTopic)
	obsE2ETestVariables.mockConsumer.SubscribeTopics(obsE2ETestVariables.consumerTopics)

	// Create triaging service
	obsE2ETestVariables.triagingService = service.NewTriagingService(obsE2ETestVariables.ctx)

	// create consumeAndProcess with mockConsumer and mockProducer
	consumeAndProcess, err := messaging.NewConsumeAndProcess(
		obsE2ETestVariables.ctx,
		&obsE2ETestVariables.mockConsumer,
		&obsE2ETestVariables.mockProducer,
	)
	if err != nil {
		return errors.New("Cannot create ConsumeAndProcess: " + err.Error())
	}

	// set consumeAndProcess for triagingservice
	obsE2ETestVariables.triagingService.SetConsumeAndProcess(consumeAndProcess)

	// set timeout for service
	obsE2ETestVariables.triagingService.SetTimeout(obsE2ETestVariables.timeout)

	return nil
}
