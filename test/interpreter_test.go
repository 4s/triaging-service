package test

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"bitbucket.org/4s/triaging-service/domain"
	"bitbucket.org/4s/triaging-service/domain/types"
	"bitbucket.org/4s/triaging-service/test/helpers"
	"github.com/google/go-cmp/cmp"
)

func TestInterpretingQuestionnaireResponse(t *testing.T) {
	var ctx = context.Background()

	var testThresholdSet = "./testdata/test_thresholdset_qr.json"
	var testQuestionnaireResponse = "./testdata/test_questionnaire_response.json"

	// ThresholdSet
	thresholdJSON, err := helpers.GetJSON(testThresholdSet)
	if err != nil {
		t.Errorf("Failed retrieving json from file")
	}
	thresholdSet := new(types.ThresholdSet)
	err = json.Unmarshal(thresholdJSON, thresholdSet)
	if err != nil {
		t.Errorf("Error while unmarshalling thresholdset")
	}
	// QuestionnaireResponse
	questionnaireResponseJSON, err := helpers.GetJSON(testQuestionnaireResponse)
	if err != nil {
		t.Errorf("Failed retrieving json from file")
	}
	questionnaireResponse := new(types.QuestionnaireResponse)
	err = json.Unmarshal(questionnaireResponseJSON, questionnaireResponse)
	if err != nil {
		t.Errorf("Error while unmarshalling questionnaire response")
	}

	interpretation, err := domain.InterpretQuestionnaireResponse(ctx, *questionnaireResponse, *thresholdSet)
	if err != nil {
		t.Errorf("An error occurred while interpreting: %v", err)
	}

	pathologicalInterpretation := getPathologicalInterpretation()
	if !cmp.Equal(interpretation, *pathologicalInterpretation) {
		t.Errorf(
			"The interpretation failed. \n"+
				"System: %v, expected: %v \n"+
				"Code: %v, expected %v \n"+
				"Display: %v, expected %v \n",
			*(*(interpretation).Coding)[0].System, *(*pathologicalInterpretation.Coding)[0].System,
			*(*(interpretation).Coding)[0].Code, *(*pathologicalInterpretation.Coding)[0].Code,
			*(*(interpretation).Coding)[0].Display, *(*pathologicalInterpretation.Coding)[0].Display,
		)
	}
}

func TestCreateInterpretationFromEffectiveTypesPathological(t *testing.T) {
	effectiveTypes := []types.Code{}
	// only pathological match
	effectiveTypes = append(effectiveTypes, types.Code("pathological"))
	interpretation, err := domain.CreateInterpretationFromEffectiveTypes(effectiveTypes)
	if err != nil {
		t.Errorf("An error occurred while creating interpretation from effectiveTypes-array: %v", err)
	}

	pathologicalInterpretation := getPathologicalInterpretation()
	if !cmp.Equal(interpretation, pathologicalInterpretation) {
		t.Errorf(
			"The interpretation failed. \n"+
				"System: %v, expected: %v \n"+
				"Code: %v, expected %v \n"+
				"Display: %v, expected %v \n",
			*(*(interpretation).Coding)[0].System, *(*pathologicalInterpretation.Coding)[0].System,
			*(*(interpretation).Coding)[0].Code, *(*pathologicalInterpretation.Coding)[0].Code,
			*(*(interpretation).Coding)[0].Display, *(*pathologicalInterpretation.Coding)[0].Display,
		)
	}
	// pathological and abnormal match
	effectiveTypes = append(effectiveTypes, types.Code("abnormal"))
	interpretation, err = domain.CreateInterpretationFromEffectiveTypes(effectiveTypes)
	if err != nil {
		t.Errorf("An error occurred while creating interpretation from effectiveTypes-array: %v", err)
	}

	if !cmp.Equal(interpretation, pathologicalInterpretation) {
		t.Errorf(
			"The interpretation failed. \n"+
				"System: %v, expected: %v \n"+
				"Code: %v, expected %v \n"+
				"Display: %v, expected %v \n",
			*(*(interpretation).Coding)[0].System, *(*pathologicalInterpretation.Coding)[0].System,
			*(*(interpretation).Coding)[0].Code, *(*pathologicalInterpretation.Coding)[0].Code,
			*(*(interpretation).Coding)[0].Display, *(*pathologicalInterpretation.Coding)[0].Display,
		)
	}

	// pathological, abnormal and normal match
	effectiveTypes = append(effectiveTypes, types.Code("normal"))
	interpretation, err = domain.CreateInterpretationFromEffectiveTypes(effectiveTypes)
	if err != nil {
		t.Errorf("An error occurred while creating interpretation from effectiveTypes-array: %v", err)
	}

	if !cmp.Equal(interpretation, pathologicalInterpretation) {
		t.Errorf(
			"The interpretation failed. \n"+
				"System: %v, expected: %v \n"+
				"Code: %v, expected %v \n"+
				"Display: %v, expected %v \n",
			*(*(interpretation).Coding)[0].System, *(*pathologicalInterpretation.Coding)[0].System,
			*(*(interpretation).Coding)[0].Code, *(*pathologicalInterpretation.Coding)[0].Code,
			*(*(interpretation).Coding)[0].Display, *(*pathologicalInterpretation.Coding)[0].Display,
		)
	}
}

func TestCreateInterpretationFromEffectiveTypesAbnormal(t *testing.T) {
	effectiveTypes := []types.Code{}

	// only abnormal match
	effectiveTypes = append(effectiveTypes, types.Code("abnormal"))
	interpretation, err := domain.CreateInterpretationFromEffectiveTypes(effectiveTypes)
	if err != nil {
		t.Errorf("An error occurred while creating interpretation from effectiveTypes-array: %v", err)
	}

	abnormalInterpretation := getAbnormalInterpretation()
	if !cmp.Equal(interpretation, abnormalInterpretation) {
		t.Errorf(
			"The interpretation failed. \n"+
				"System: %v, expected: %v \n"+
				"Code: %v, expected %v \n"+
				"Display: %v, expected %v \n",
			*(*(interpretation).Coding)[0].System, *(*abnormalInterpretation.Coding)[0].System,
			*(*(interpretation).Coding)[0].Code, *(*abnormalInterpretation.Coding)[0].Code,
			*(*(interpretation).Coding)[0].Display, *(*abnormalInterpretation.Coding)[0].Display,
		)
	}

	// abnormal and normal match
	effectiveTypes = append(effectiveTypes, types.Code("normal"))
	interpretation, err = domain.CreateInterpretationFromEffectiveTypes(effectiveTypes)
	if err != nil {
		t.Errorf("An error occurred while creating interpretation from effectiveTypes-array: %v", err)
	}

	if !cmp.Equal(interpretation, abnormalInterpretation) {
		t.Errorf(
			"The interpretation failed. \n"+
				"System: %v, expected: %v \n"+
				"Code: %v, expected %v \n"+
				"Display: %v, expected %v \n",
			*(*(interpretation).Coding)[0].System, *(*abnormalInterpretation.Coding)[0].System,
			*(*(interpretation).Coding)[0].Code, *(*abnormalInterpretation.Coding)[0].Code,
			*(*(interpretation).Coding)[0].Display, *(*abnormalInterpretation.Coding)[0].Display,
		)
	}
}

func TestCreateInterpretationFromEffectiveTypesNormal(t *testing.T) {
	effectiveTypes := []types.Code{}
	effectiveTypes = append(effectiveTypes, types.Code("normal"))
	interpretation, err := domain.CreateInterpretationFromEffectiveTypes(effectiveTypes)
	if err != nil {
		t.Errorf("An error occurred while creating interpretation from effectiveTypes-array: %v", err)
	}

	normalInterpretation := getNormalInterpretation()
	if !cmp.Equal(interpretation, normalInterpretation) {
		t.Errorf(
			"The interpretation failed. \n"+
				"System: %v, expected: %v \n"+
				"Code: %v, expected %v \n"+
				"Display: %v, expected %v \n",
			*(*(interpretation).Coding)[0].System, *(*normalInterpretation.Coding)[0].System,
			*(*(interpretation).Coding)[0].Code, *(*normalInterpretation.Coding)[0].Code,
			*(*(interpretation).Coding)[0].Display, *(*normalInterpretation.Coding)[0].Display,
		)
	}
}

func TestHandleTypesReference(t *testing.T) {
	first := types.Reference{}
	second := types.String("")

	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Reference")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Reference",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Reference", err.Error())
	}
}

func TestHandleTypesCoding(t *testing.T) {
	first := types.Coding{}
	second := types.String("")

	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Coding")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Coding",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Coding", err.Error())
	}
}

func TestHandleTypesURI(t *testing.T) {
	first := types.URI("")
	second := types.String("")

	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "URI")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "URI",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "URI", err.Error())
	}
}

func TestHandleTypesDecimal(t *testing.T) {
	first := types.Decimal(1.1)
	second := types.String("")

	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Decimal")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Decimal",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Decimal", err.Error())
	}
}

func TestHandleTypesDate(t *testing.T) {
	first := types.Date("1990-05-22")
	second := types.String("")

	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Date")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Date",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Date", err.Error())
	}
}

func TestHandleTypesQuantity(t *testing.T) {
	first := types.Quantity{}
	second := types.String("")

	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Quantity")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Quantity",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Quantity", err.Error())
	}
}

func TestHandleTypesCodeableConcept(t *testing.T) {
	first := types.CodeableConcept{}
	second := types.String("")
	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "CodeableConcept")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "CodeableConcept",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "CodeableConcept", err.Error())
	}
}

func TestHandleTypesString(t *testing.T) {
	first := types.String("")
	second := types.Integer(1)
	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "String")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.Integer", "String",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "String", err.Error())
	}
}

func TestHandleTypesBoolean(t *testing.T) {
	first := types.Boolean(true)
	second := types.String("")
	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Boolean")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Boolean",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Boolean", err.Error())
	}
}

func TestHandleTypesInteger(t *testing.T) {
	first := types.Integer(1)
	second := types.String("")
	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Integer")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Integer",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Integer", err.Error())
	}
}

func TestHandleTypesRange(t *testing.T) {
	first := types.Range{}
	second := types.String("")
	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Range")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Range",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Range", err.Error())
	}
}

func TestHandleTypesTime(t *testing.T) {
	first := types.Time("15:04:05")
	second := types.String("")
	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Time")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Time",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Time", err.Error())
	}
}

func TestHandleTypesDateTime(t *testing.T) {
	first := types.DateTime("1990-05-22T05:23:14Z")
	second := types.String("")
	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "DateTime")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "DateTime",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "DateTime", err.Error())
	}
}

func TestHandleTypesPeriod(t *testing.T) {
	first := types.Period{}
	second := types.String("")
	operator := types.Code("=")
	_, err := domain.HandleTypes(&first, &second, operator)
	if err == nil {
		t.Errorf("HandleTypes failed with %v: should throw error, but didn't", "Period")
	}
	if err != nil && err.Error() != fmt.Sprintf(
		"Outcome value is of type *types.%v, but effectiveWhen value is of type *types.String", "Period",
	) {
		t.Errorf("HandleTypes failed with %v. wrong errormessage: %v", "Period", err.Error())
	}
}

func TestLoopThroughItemsNestedInAnswer(t *testing.T) {
	ctx := context.Background()

	var testQuestionnaireResponseNested = "./testdata/test_questionnaire_response_nested_one.json"
	// QuestionnaireResponse
	questionnaireResponseJSON, err := helpers.GetJSON(testQuestionnaireResponseNested)
	if err != nil {
		t.Errorf("Failed retrieving json from file")
	}
	questionnaireResponse := new(types.QuestionnaireResponse)
	err = json.Unmarshal(questionnaireResponseJSON, questionnaireResponse)
	if err != nil {
		t.Errorf("Error while unmarshalling questionnaire response")
	}

	effectiveWhen := types.EffectiveWhen{}
	component := types.Code("question-nest")
	componentCoding := types.Coding{}
	effectiveWhen.Component = &componentCoding
	effectiveWhen.Component.System = &domain.EffectiveWhenCodingSystems.ComponentQRCodingSystemURL
	effectiveWhen.Component.Code = &component
	operator := types.Code("=")
	effectiveWhen.Operator = &operator
	valueInteger := types.Integer(2)
	effectiveWhen.ValueInteger = &valueInteger

	effectiveBehavior := types.Code("any")
	inEffect, exists, err := domain.LoopThroughItems(
		ctx,
		*questionnaireResponse.Item,
		effectiveWhen,
		&effectiveBehavior,
		false,
	)
	if err != nil {
		t.Fatalf("Loop through items failed: %v", err)
	}
	if !exists {
		t.Fatalf("Exists is false and should be true")
	}
	if !inEffect {
		t.Fatalf("InEffect is false and should be true")
	}
}

func TestLoopThroughItemsNestedInItem(t *testing.T) {

	ctx := context.Background()

	var testQuestionnaireResponseNested = "./testdata/test_questionnaire_response_nested_two.json"
	// QuestionnaireResponse
	questionnaireResponseJSON, err := helpers.GetJSON(testQuestionnaireResponseNested)
	if err != nil {
		t.Errorf("Failed retrieving json from file")
	}
	questionnaireResponse := new(types.QuestionnaireResponse)
	err = json.Unmarshal(questionnaireResponseJSON, questionnaireResponse)
	if err != nil {
		t.Errorf("Error while unmarshalling questionnaire response")
	}

	effectiveWhen := types.EffectiveWhen{}
	component := types.Code("question-nest")
	componentCoding := types.Coding{}
	effectiveWhen.Component = &componentCoding
	effectiveWhen.Component.System = &domain.EffectiveWhenCodingSystems.ComponentQRCodingSystemURL
	effectiveWhen.Component.Code = &component
	operator := types.Code("=")
	effectiveWhen.Operator = &operator
	valueInteger := types.Integer(2)
	effectiveWhen.ValueInteger = &valueInteger

	effectiveBehavior := types.Code("any")
	inEffect, exists, err := domain.LoopThroughItems(
		ctx,
		*questionnaireResponse.Item,
		effectiveWhen,
		&effectiveBehavior,
		false,
	)
	if err != nil {
		t.Fatalf("Loop through items failed: %v", err)
	}
	if !exists {
		t.Fatalf("Exists is false and should be true")
	}
	if !inEffect {
		t.Fatalf("InEffect is false and should be true")
	}
}

// HELPERS

func getPathologicalInterpretation() *types.CodeableConcept {
	system := types.URI("http://hl7.org/fhir/v2/0078")
	code := types.Code("AA")
	display := types.String("Pathological")

	coding := types.Coding{}
	coding.System = &system
	coding.Code = &code
	coding.Display = &display
	interpretation := new(types.CodeableConcept)
	codings := new(types.Codings)
	*codings = append(*codings, coding)
	interpretation.Coding = codings

	return interpretation
}

func getAbnormalInterpretation() *types.CodeableConcept {
	system := types.URI("http://hl7.org/fhir/v2/0078")
	code := types.Code("A")
	display := types.String("Abnormal")

	coding := types.Coding{}
	coding.System = &system
	coding.Code = &code
	coding.Display = &display
	interpretation := new(types.CodeableConcept)
	codings := new(types.Codings)
	*codings = append(*codings, coding)
	interpretation.Coding = codings

	return interpretation
}

func getNormalInterpretation() *types.CodeableConcept {
	system := types.URI("http://hl7.org/fhir/v2/0078")
	code := types.Code("N")
	display := types.String("Normal")

	coding := types.Coding{}
	coding.System = &system
	coding.Code = &code
	coding.Display = &display
	interpretation := new(types.CodeableConcept)
	codings := new(types.Codings)
	*codings = append(*codings, coding)
	interpretation.Coding = codings

	return interpretation
}
