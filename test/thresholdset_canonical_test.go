package test

import (
	"context"
	"testing"

	"bitbucket.org/4s/triaging-service/domain"
	"bitbucket.org/4s/triaging-service/domain/types"
	"bitbucket.org/4s/triaging-service/processing"
)

func TestThresholdSetCanonical(t *testing.T) {
	ctx := context.Background()

	identifier := new(types.Identifier)
	system := types.URI("urn:ietf:rfc:3986")
	value := types.String("3b3754cf-91c1-4e1d-a879-32fef7f1f177")
	identifier.System = &system
	identifier.Value = &value
	identifiers := []types.Identifier{
		*identifier,
	}

	version := types.String("1.0.0")

	eventProcessor, err := processing.NewMyEventProcessor(ctx)
	if err != nil {
		t.Fatalf("EventProcessor could not be created: %v", err)
	}

	extension, err := eventProcessor.CreateThresholdSetCanonical(&identifiers, &version)
	if err != nil {
		t.Fatalf("Extension could not be created: %v", err)
	}
	if extension.URL == nil {
		t.Fatalf("Extension does not have a URL field")
	}
	if *extension.URL != domain.OutcomeExtensions.ThresholdSetCanonicalURL {
		t.Fatalf("Extension does not have the correct url: %v", *extension.URL)
	}
	if extension.ValueCanonical == nil {
		t.Fatalf("Extension does not have a ValueCanonical field")
	}
	if *extension.ValueCanonical != "canonical://uuid/Basic/3b3754cf-91c1-4e1d-a879-32fef7f1f177|1.0.0" {
		t.Fatalf("Extension does not have the correct url: %v", *extension.ValueCanonical)
	}
}
