// +build integration

package test

import (
	"context"
	"testing"

	"github.com/google/go-cmp/cmp"

	"bitbucket.org/4s/triaging-service/domain/types"
	"bitbucket.org/4s/triaging-service/network"
	"bitbucket.org/4s/triaging-service/test/helpers"
)

func TestGetCarePlan(t *testing.T) {
	ctx := context.Background()

	testCarePlanBundle := "./testdata/test_careplan_bundle.json"

	requestHandler := network.NewRestRequestHandler(ctx, "http://localhost:8070/baseR4")

	system := types.URI("urn:ietf:rfc:3986")
	value := types.String("d9636b26-63b5-48ef-961e-8539c086d111")
	careplanIdentifier := types.Identifier{
		System: &system,
		Value:  &value,
	}

	bundleByte, err := requestHandler.GetCarePlan(
		ctx,
		careplanIdentifier,
		"session",
		"token",
	)
	testByte, err := helpers.GetJSON(testCarePlanBundle)
	if err != nil {
		t.Fatalf("Test failed")
	}
	if cmp.Equal(bundleByte, testByte) {
		t.Fatalf("Test failed")
	}
}
