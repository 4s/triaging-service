// +build integration

package test

import (
	"context"
	"errors"
	"os"
	"strings"
	"testing"
	"time"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	mock "bitbucket.org/4s/go-messaging/implementations/mock"
	"bitbucket.org/4s/triaging-service/network"
	"bitbucket.org/4s/triaging-service/processing"
	"bitbucket.org/4s/triaging-service/service"
	"bitbucket.org/4s/triaging-service/test/helpers"
)

// context variables
var ctx context.Context

// producer variables
var producerTopic messaging.Topic
var mockProducer mock.EventProducer

// consumer variables
var consumerTopics messaging.Topics
var mockConsumer mock.EventConsumer

// processing variables
var timeout = time.Duration(1 * time.Second)
var testQuestionnaireResponse = "./testdata/test_questionnaire_response.json"
var eventProcessor processing.MyEventProcessor
var requestHandler network.RequestHandler
var triagingService service.TriagingService

func TestQuestionnaireResponseE2EIntegration(t *testing.T) {
	// initialize test variables
	err := initE2E()
	if err != nil {
		t.Errorf("Test failed: %v", err.Error())
	}

	// create topic for testProducer
	producerTopic = messaging.NewTopic()
	producerTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")
	log.WithContext(ctx).Debug("Producer topic set to: " + producerTopic.String())

	// create test message
	message := messaging.NewMessage()
	message.SetBodyType("FHIR").
		SetContentVersion("R4").
		SetCorrelationID("wF1FdPFP1X8").
		SetTransactionID("488f8476-fa5d-44e3-8f26-e185f08c3bae")
	json, err := helpers.GetJSON(testQuestionnaireResponse)
	if err != nil {
		t.Errorf("Could not create get QuestionnaireResponse json: %v", err.Error())
	}
	message.SetBody(string(json))

	// produce testmessage
	err = mockProducer.Produce(producerTopic, message)
	if err != nil {
		t.Errorf("Error while producing testmessage: %v", err.Error())
	}

	// run triagingservice
	err = triagingService.Triage(ctx)
	if err != nil {
		t.Errorf("Error while running Triage: %v", err)
	}

	// wait for producer to produce message to outputtopic
	outputtopic := messaging.NewTopic()
	outputtopic.SetOperation(messaging.Update).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")
	future := mockProducer.GetFutureTopic(ctx, outputtopic, timeout)
	mess := <-future

	if mess == (messaging.Message{}) {
		t.Errorf("No message received from Kafka")
	}
	if !strings.Contains(mess.GetBody(), string(`{"url":"https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/111771649/QuestionnaireResponse+interpretation+extension","valueCodeableConcept":{"coding":[{"system":"http://hl7.org/fhir/v2/0078","code":"N","display":"Normal"}]}}`)) {
		t.Errorf("Message body does not contain a normal interpretation")
		t.Errorf(mess.GetBody())
	}
	if !strings.Contains(mess.GetBody(), string(`{"url":"https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/111706117/Outcome+thresholdSetCanonical+extension","valueCanonical":"urn:Basic:urn\\:ietf\\:rfc\\:3986:3b3754cf-91c1-4e1d-a879-32fef7f1f177|1.0.0"}`)) {
		t.Errorf("Message body does not contain a canonical reference to threshold")
		t.Errorf(mess.GetBody())
	}
}

func initE2E() error {
	var err error

	// create ctx for logging
	ctx = context.Background()

	// create EventProcessor
	eventProcessor, err = processing.NewMyEventProcessor(ctx)
	if err != nil {
		return errors.New("Could not create myEventProcessor: " + err.Error())
	}

	// create MockConsumer
	mockConsumer, err := mock.NewEventConsumer(ctx, eventProcessor)
	if err != nil {
		return errors.New("Could not create testConsumer: " + err.Error())
	}

	// create MockProducer
	mockProducer, err = mock.NewEventProducer(ctx, "mock producer")
	if err != nil {
		return errors.New("Could not create testProducer: " + err.Error())
	}

	patientCareServiceURL := os.Getenv("PATIENTCARE_SERVICE_URL")
	if patientCareServiceURL == "" {
		return errors.New("No PATIENTCARE_SERVICE_URL set")
	}
	requestHandler = network.NewRestRequestHandler(ctx, patientCareServiceURL)

	eventProcessor.SetRequestHandler(requestHandler)

	// create consumerTopics
	observationTopic := messaging.NewTopic()
	observationTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("Observation")

	questionnaireResponseTopic := messaging.NewTopic()
	questionnaireResponseTopic.SetOperation(messaging.DataCreated).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")

	consumerTopics = append(consumerTopics, observationTopic)
	consumerTopics = append(consumerTopics, questionnaireResponseTopic)
	mockConsumer.SubscribeTopics(consumerTopics)

	// Create triaging service
	triagingService = service.NewTriagingService(ctx)

	// create consumeAndProcess with mockConsumer and mockProducer
	consumeAndProcess, err := messaging.NewConsumeAndProcess(
		ctx,
		&mockConsumer,
		&mockProducer,
	)
	if err != nil {
		return errors.New("Cannot create ConsumeAndProcess: " + err.Error())
	}

	// set consumeAndProcess for triagingservice
	triagingService.SetConsumeAndProcess(consumeAndProcess)

	// set timeout for service
	triagingService.SetTimeout(timeout)

	return nil
}
