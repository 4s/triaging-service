package test

import (
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"

	"bitbucket.org/4s/triaging-service/domain"
	"bitbucket.org/4s/triaging-service/domain/types"
	"bitbucket.org/4s/triaging-service/test/helpers"
)

func TestConvertToThresholdSet(t *testing.T) {
	var testThresholdSetExtension = "./testdata/test_thresholdset_extension_qr.json"
	var testThresholdSet = "./testdata/test_thresholdset_qr.json"

	thresholdExtensionJSON, err := helpers.GetJSON(testThresholdSetExtension)
	if err != nil {
		t.Errorf("Failed retrieving json from file")
	}
	basic := new(types.Basic)
	json.Unmarshal(thresholdExtensionJSON, basic)

	thresholdJSON, err := helpers.GetJSON(testThresholdSet)
	if err != nil {
		t.Errorf("Failed retrieving json from file")
	}
	thresholdSet := new(types.ThresholdSet)
	json.Unmarshal(thresholdJSON, &thresholdSet)

	convertedThresholdSet, err := domain.ConvertToThresholdSet(*basic)
	if err != nil {
		t.Errorf(err.Error())
	}

	convertedThresholdSetByte, _ := json.Marshal(convertedThresholdSet)
	convertedThresholdSetJSON := string(convertedThresholdSetByte[:])
	thresholdSetByte, _ := json.Marshal(*thresholdSet)
	thresholdSetJSON := string(thresholdSetByte[:])

	if !cmp.Equal(convertedThresholdSetJSON, thresholdSetJSON) {
		t.Errorf("ThresholdSets do not match."+
			"\n have: %v,"+
			"\n want: %v",
			convertedThresholdSetJSON, thresholdSetJSON,
		)
	}
}

func TestConvertVersionField(t *testing.T) {
	thresholdSet := types.ThresholdSet{}
	var err error

	versionString := types.String("1.0.0")
	extension := types.Extension{
		URL:         &domain.ThresholdSetExtensions.VersionURL,
		ValueString: &versionString,
	}
	thresholdSet, err = domain.ExtensionToThresholdSetField(thresholdSet, extension)
	if err != nil {
		t.Fatalf("Failed to convert version-extension to version-field")
	}
	if thresholdSet.Version == nil {
		t.Fatalf("Failed to convert version-extension to version-field")
	}
	if *thresholdSet.Version != "1.0.0" {
		t.Fatalf("Failed to convert version-extension to version-field. Version-field contains: %v, expected: %v", *thresholdSet.Version, versionString)
	}
}

func TestConvertStatus(t *testing.T) {
	thresholdSet := types.ThresholdSet{}
	var err error

	statusCode := types.Code("code")
	extension := types.Extension{
		URL:       &domain.ThresholdSetExtensions.StatusURL,
		ValueCode: &statusCode,
	}
	thresholdSet, err = domain.ExtensionToThresholdSetField(thresholdSet, extension)
	if err != nil {
		t.Fatalf("Failed to convert status-extension to status-field")
	}
	if thresholdSet.Status == nil {
		t.Fatalf("Failed to convert status-extension to status-field")
	}
	if *thresholdSet.Status != statusCode {
		t.Fatalf("Failed to convert status-extension to status-field. Status-field contains: %v, expected: %v", *thresholdSet.Status, statusCode)
	}
}

func TestConvertApprovalDate(t *testing.T) {
	thresholdSet := types.ThresholdSet{}
	var err error

	approvalDate := types.DateTime("1990-05-22T05:23:14Z")
	extension := types.Extension{
		URL:           &domain.ThresholdSetExtensions.ApprovalDateURL,
		ValueDateTime: &approvalDate,
	}
	thresholdSet, err = domain.ExtensionToThresholdSetField(thresholdSet, extension)
	if err != nil {
		t.Fatalf("Failed to convert approvalDate-extension to approvalDate-field")
	}
	if thresholdSet.ApprovalDate == nil {
		t.Fatalf("Failed to convert approvalDate-extension to approvalDate-field")
	}
	if *thresholdSet.ApprovalDate != approvalDate {
		t.Fatalf("Failed to convert approvalDate-extension to approvalDate-field. ApprovalDate-field contains: %v, expected: %v", *thresholdSet.ApprovalDate, approvalDate)
	}
}

func TestConvertApprovingPractitioner(t *testing.T) {
	thresholdSet := types.ThresholdSet{}
	var err error

	approvingPractitionerReference := types.String("reference")
	approvingPractitionerType := types.String("Practitioner")
	approvingPractitioner := types.Reference{
		Reference: &approvingPractitionerReference,
		Type:      &approvingPractitionerType,
	}
	extension := types.Extension{
		URL:            &domain.ThresholdSetExtensions.ApprovingPractitionerURL,
		ValueReference: &approvingPractitioner,
	}
	thresholdSet, err = domain.ExtensionToThresholdSetField(thresholdSet, extension)
	if err != nil {
		t.Fatalf("Failed to convert approvingPractitioner-extension to approvingPractitioner-field")
	}
	if thresholdSet.ApprovingPractitioner == nil {
		t.Fatalf("Failed to convert approvingPractitioner-extension to approvingPractitioner-field")
	}
	if *thresholdSet.ApprovingPractitioner != approvingPractitioner {
		t.Fatalf("Failed to convert approvingPractitioner-extension to approvingPractitioner-field. ApprovingPractitioner-field contains: %v, expected: %v", *thresholdSet.ApprovingPractitioner, approvingPractitioner)
	}
}

func TestConvertAppliesTo(t *testing.T) {
	thresholdSet := types.ThresholdSet{}
	var err error

	appliesTo := types.Canonical("canonical://uuid/QuestionnaireResponse/3b3754cf-91c1-4e1d-a879-32fef7f1f177|1.0.0")
	extension := types.Extension{
		URL:            &domain.ThresholdSetExtensions.AppliesToURL,
		ValueCanonical: &appliesTo,
	}
	thresholdSet, err = domain.ExtensionToThresholdSetField(thresholdSet, extension)
	if err != nil {
		t.Fatalf("Failed to convert appliesTo-extension to appliesTo-field")
	}
	if thresholdSet.AppliesTo == nil {
		t.Fatalf("Failed to convert appliesTo-extension to appliesTo-field")
	}
	if *thresholdSet.AppliesTo != appliesTo {
		t.Fatalf("Failed to convert appliesTo-extension to appliesTo-field. AppliesTo-field contains: %v, expected: %v", *thresholdSet.AppliesTo, appliesTo)
	}
}

func TestConvertType(t *testing.T) {
	threshold := types.Threshold{}
	var err error

	thresholdTypeSystem := types.URI("system")
	thresholdTypeCode := types.Code("code")
	thresholdTypeCoding := types.Coding{
		System: &thresholdTypeSystem,
		Code:   &thresholdTypeCode,
	}
	thresholdTypeCodingArray := types.Codings{}
	thresholdTypeCodingArray = append(thresholdTypeCodingArray, thresholdTypeCoding)
	thresholdType := types.CodeableConcept{}
	extension := types.Extension{
		URL:                  &domain.ThresholdExtensions.TypeURL,
		ValueCodeableConcept: &thresholdType,
	}

	threshold, err = domain.ExtensionToThresholdField(threshold, extension)
	if err != nil {
		t.Fatalf("Failed to convert type-extension to type-field. Error: %v", err.Error())
	}
	if threshold.Type == nil {
		t.Fatalf("Failed to convert type-extension to type-field")
	}
	if *threshold.Type != thresholdType {
		t.Fatalf("Failed to convert type-extension to type-field. Type-field contains: %v, expected: %v", *threshold.Type, thresholdType)
	}
}

func TestConvertEffectiveBehavior(t *testing.T) {
	threshold := types.Threshold{}
	var err error

	effectiveBehavior := types.Code("all")
	extension := types.Extension{
		URL:       &domain.ThresholdExtensions.EffectiveBehaviorURL,
		ValueCode: &effectiveBehavior,
	}

	threshold, err = domain.ExtensionToThresholdField(threshold, extension)
	if err != nil {
		t.Fatalf("Failed to convert effectiveBehavior-extension to effectiveBehavior-field. Error: %v", err.Error())
	}
	if threshold.EffectiveBehavior == nil {
		t.Fatalf("Failed to convert effectiveBehavior-extension to effectiveBehavior-field")
	}
	if *threshold.EffectiveBehavior != effectiveBehavior {
		t.Fatalf("Failed to convert effectiveBehavior-extension to effectiveBehavior-field. EffectiveBehavior-field contains: %v, expected: %v", *threshold.EffectiveBehavior, effectiveBehavior)
	}
}

func TestConvertComponent(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	component := types.Code("question-1")
	extension := types.Extension{
		URL: &domain.EffectiveWhenExtension.ComponentURL,
		ValueCoding: &types.Coding{
			System: &domain.EffectiveWhenCodingSystems.ComponentQRCodingSystemURL,
			Code:   &component,
		},
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert effectiveBehavior-extension to effectiveBehavior-field. Error: %v", err.Error())
	}
	if effectiveWhen.Component == nil {
		t.Fatalf("Failed to convert component-extension to component-field")
	}
	if effectiveWhen.Component.System != &domain.EffectiveWhenCodingSystems.ComponentQRCodingSystemURL {
		t.Fatalf("Failed to convert component-extension to component-field")
	}
	if *effectiveWhen.Component.Code != component {
		t.Fatalf("Failed to convert component-extension to component-field. Component-field contains: %v, expected: %v", *effectiveWhen.Component, component)
	}
}

func TestConvertValueBoolean(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueBoolean := types.Boolean(false)
	extension := types.Extension{
		URL:          &domain.EffectiveWhenExtension.ValueBooleanURL,
		ValueBoolean: &valueBoolean,
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueBoolean-extension to valueBoolean-field. Error: %v", err.Error())
	}
	if effectiveWhen.ValueBoolean == nil {
		t.Fatalf("Failed to convert valueBoolean-extension to valueBoolean-field")
	}
	if *effectiveWhen.ValueBoolean != valueBoolean {
		t.Fatalf("Failed to convert valueBoolean-extension to valueBoolean-field. ValueBoolean-field contains: %v, expected: %v", *effectiveWhen.ValueBoolean, valueBoolean)
	}
}

func TestConvertValueDecimal(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueDecimal := types.Decimal(2.2)
	extension := types.Extension{
		URL:          &domain.EffectiveWhenExtension.ValueDecimalURL,
		ValueDecimal: &valueDecimal,
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueDecimal-extension to valueDecimal-field. Error: %v", err.Error())
	}
	if effectiveWhen.ValueDecimal == nil {
		t.Fatalf("Failed to convert valueDecimal-extension to valueDecimal-field")
	}
	if *effectiveWhen.ValueDecimal != valueDecimal {
		t.Fatalf("Failed to convert valueDecimal-extension to valueDecimal-field. ValueDecimal-field contains: %v, expected: %v", *effectiveWhen.ValueDecimal, valueDecimal)
	}
}

func TestConvertValueInteger(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueInteger := types.Integer(2)
	extension := types.Extension{
		URL:          &domain.EffectiveWhenExtension.ValueIntegerURL,
		ValueInteger: &valueInteger,
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueDecimal-extension to valueDecimal-field. Error: %v", err.Error())
	}
	if effectiveWhen.ValueInteger == nil {
		t.Fatalf("Failed to convert valueInteger-extension to valueInteger-field")
	}
	if *effectiveWhen.ValueInteger != valueInteger {
		t.Fatalf("Failed to convert valueInteger-extension to valueInteger-field. ValueInteger-field contains: %v, expected: %v", *effectiveWhen.ValueInteger, valueInteger)
	}
}

func TestConvertValueString(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueString := types.String("string")
	extension := types.Extension{
		URL:         &domain.EffectiveWhenExtension.ValueStringURL,
		ValueString: &valueString,
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueString-extension to valueString-field. Error: %v", err.Error())
	}
	if effectiveWhen.ValueString == nil {
		t.Fatalf("Failed to convert valueString-extension to valueString-field")
	}
	if *effectiveWhen.ValueString != valueString {
		t.Fatalf("Failed to convert valueString-extension to valueString-field. ValueString-field contains: %v, expected: %v", *effectiveWhen.ValueString, valueString)
	}
}

func TestConvertValueDate(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueDate := types.Date("1990-05-22")
	extension := types.Extension{
		URL:       &domain.EffectiveWhenExtension.ValueDateURL,
		ValueDate: &valueDate,
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueDate-extension to valueDate-field. Error: %v", err.Error())
	}
	if effectiveWhen.ValueDate == nil {
		t.Fatalf("Failed to convert valueDate-extension to valueDate-field")
	}
	if *effectiveWhen.ValueDate != valueDate {
		t.Fatalf("Failed to convert valueDate-extension to valueDate-field. ValueDate-field contains: %v, expected: %v", *effectiveWhen.ValueDate, valueDate)
	}
}

func TestConvertValueDateTime(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueDateTime := types.DateTime("1990-05-22T05:23:14Z")
	extension := types.Extension{
		URL:           &domain.EffectiveWhenExtension.ValueDateTimeURL,
		ValueDateTime: &valueDateTime,
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueDateTime-extension to valueDateTime-field. Error: %v", err.Error())
	}
	if effectiveWhen.ValueDateTime == nil {
		t.Fatalf("Failed to convert valueDateTime-extension to valueDateTime-field")
	}
	if *effectiveWhen.ValueDateTime != valueDateTime {
		t.Fatalf("Failed to convert valueDateTime-extension to valueDateTime-field. ValueDate-field contains: %v, expected: %v", *effectiveWhen.ValueDate, valueDateTime)
	}
}

func TestConvertValueURI(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueURI := types.URI("http://unitsofmeasure.org")
	extension := types.Extension{
		URL:      &domain.EffectiveWhenExtension.ValueURIURL,
		ValueURI: &valueURI,
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueURI-extension to valueURI-field. Error: %v", err.Error())
	}
	if effectiveWhen.ValueURI == nil {
		t.Fatalf("Failed to convert valueURI-extension to valueURI-field")
	}
	if *effectiveWhen.ValueURI != valueURI {
		t.Fatalf("Failed to convert valueURI-extension to valueURI-field. ValueURI-field contains: %v, expected: %v", *effectiveWhen.ValueURI, valueURI)
	}
}

func TestConvertValueCodeableConcept(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueCodeableConceptSystem := types.URI("system")
	valueCodeableConceptCode := types.Code("code")
	valueCodeableConceptCoding := types.Coding{
		System: &valueCodeableConceptSystem,
		Code:   &valueCodeableConceptCode,
	}
	valueCodeableConceptCodingArray := types.Codings{}
	valueCodeableConceptCodingArray = append(valueCodeableConceptCodingArray, valueCodeableConceptCoding)
	valueCodeableConcept := types.CodeableConcept{}
	extension := types.Extension{
		URL:                  &domain.EffectiveWhenExtension.ValueCodeableConceptURL,
		ValueCodeableConcept: &valueCodeableConcept,
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueCodeableConcept-extension to valueCodeableConcept-field. Error: %v", err.Error())
	}
	if effectiveWhen.ValueCodeableConcept == nil {
		t.Fatalf("Failed to convert valueCodeableConcept-extension to valueCodeableConcept-field")
	}
	if *effectiveWhen.ValueCodeableConcept != valueCodeableConcept {
		t.Fatalf("Failed to convert type-extension to valueCodeableConcept-field. ValueCodeableConcept-field contains: %v, expected: %v", *effectiveWhen.ValueCodeableConcept, valueCodeableConcept)
	}
}

func TestConvertValueQuantity(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueQuantitySystem := types.URI("http://unitsofmeasure.org")
	valueQuantityCode := types.Code("kg")
	valueQuantityValue := types.Decimal(2.3)
	valueQuantity := types.Quantity{
		System: &valueQuantitySystem,
		Code:   &valueQuantityCode,
		Value:  &valueQuantityValue,
	}
	extension := types.Extension{
		URL:           &domain.EffectiveWhenExtension.ValueQuantityURL,
		ValueQuantity: &valueQuantity,
	}

	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueQuantity-extension to valueQuantity-field. Error: %v", err.Error())
	}
	if effectiveWhen.ValueQuantity == nil {
		t.Fatalf("Failed to convert valueQuantity-extension to valueQuantity-field")
	}
	if *effectiveWhen.ValueQuantity != valueQuantity {
		t.Fatalf("Failed to convert valueQuantity-extension to valueQuantity-field. ValueQuantity-field contains: %v, expected: %v", *effectiveWhen.ValueQuantity, valueQuantity)
	}
}

func TestConvertValueReference(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	valueReferenceReference := types.String("reference")
	valueReferenceType := types.String("Practitioner")
	valueReference := types.Reference{
		Reference: &valueReferenceReference,
		Type:      &valueReferenceType,
	}
	extension := types.Extension{
		URL:            &domain.EffectiveWhenExtension.ValueReferenceURL,
		ValueReference: &valueReference,
	}
	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert valueReference-extension to valueReference-field")
	}
	if effectiveWhen.ValueReference == nil {
		t.Fatalf("Failed to convert valueReference-extension to valueReference-field")
	}
	if *effectiveWhen.ValueReference != valueReference {
		t.Fatalf("Failed to convert valueReference-extension to valueReference-field. ValueReference-field contains: %v, expected: %v", *effectiveWhen.ValueReference, valueReference)
	}
}

func TestConvertOperator(t *testing.T) {
	effectiveWhen := types.EffectiveWhen{}
	var err error

	operator := types.Code(">")
	extension := types.Extension{
		URL:       &domain.EffectiveWhenExtension.OperatorURL,
		ValueCode: &operator,
	}
	effectiveWhen, err = domain.ExtensionToEffectiveWhenField(effectiveWhen, extension)
	if err != nil {
		t.Fatalf("Failed to convert operator-extension to operator-field")
	}
	if effectiveWhen.Operator == nil {
		t.Fatalf("Failed to convert operator-extension to operator-field")
	}
	if *effectiveWhen.Operator != operator {
		t.Fatalf("Failed to convert operator-extension to operator-field. Operator-field contains: %v, expected: %v", *effectiveWhen.Operator, operator)
	}
}
