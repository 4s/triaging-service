package test

import (
	"context"
	"strings"
	"testing"

	messaging "bitbucket.org/4s/go-messaging"
	"bitbucket.org/4s/triaging-service/domain/types"
	"bitbucket.org/4s/triaging-service/processing"
)

func TestCreateTask(t *testing.T) {
	ctx := context.Background()
	myEventProcessor, err := processing.NewMyEventProcessor(ctx)
	if err != nil {
		t.Fatalf("MyEventProcessor could not be created: %v", err)
	}

	consumedTopic := messaging.Topic{}
	messageProcessedTopic := &messaging.Topic{}
	receivedMessage := messaging.Message{}
	outgoingMessage := &messaging.Message{}

	// focus.type
	receivedMessage.SetBodyType("QuestionnaireResponse")

	// basedOn
	basedOn := []types.Reference{}
	basedOnType := types.String("Careplan")
	basedOnReference := types.Reference{
		Type: &basedOnType,
	}
	basedOnSystem := types.URI("urn:ietf:rfc:3986")
	basedOnValue := types.String("38ac5aaa-53c8-4873-913e-4b54a73f78db")
	basedOnReference.Identifier = &types.Identifier{
		System: &basedOnSystem,
		Value:  &basedOnValue,
	}
	basedOn = append(basedOn, basedOnReference)
	processing.BasedOn = &basedOn

	// identifier
	identifierSystem := types.URI("urn:ietf:rfc:3986")
	identifierValue := types.String("38ac5aaa-53c8-4873-913e-4b54a73f78db")
	processing.Identifier = &types.Identifier{
		System: &identifierSystem,
		Value:  &identifierValue,
	}

	// subject
	subjectType := types.String("Patient")
	subject := types.Reference{
		Type: &subjectType,
	}
	subjectSystem := types.URI("urn:ietf:rfc:3986")
	subjectValue := types.String("38ac5aaa-53c8-4873-913e-4b54a73f78db")
	subject.Identifier = &types.Identifier{
		System: &subjectSystem,
		Value:  &subjectValue,
	}
	processing.Subject = &subject

	myEventProcessor.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))

	if outgoingMessage != nil && *outgoingMessage == (messaging.Message{}) {
		t.Errorf("Outgoing message is empty")
	}

	if !strings.Contains(outgoingMessage.GetBody(), string("identifier")) ||
		!strings.Contains(outgoingMessage.GetBody(), string("basedOn")) ||
		!strings.Contains(outgoingMessage.GetBody(), string("status")) ||
		!strings.Contains(outgoingMessage.GetBody(), string("code")) ||
		!strings.Contains(outgoingMessage.GetBody(), string("focus")) ||
		!strings.Contains(outgoingMessage.GetBody(), string("QuestionnaireResponse")) ||
		!strings.Contains(outgoingMessage.GetBody(), string("for")) ||
		!strings.Contains(outgoingMessage.GetBody(), string("authoredOn")) ||
		!strings.Contains(outgoingMessage.GetBody(), string("lastModified")) ||
		!strings.Contains(outgoingMessage.GetBody(), string("reasonCode")) {
		t.Errorf("Outgoing message does not contain all the necessary elements")
		t.Errorf(outgoingMessage.GetBody())
	}
}
