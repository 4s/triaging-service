package main

import (
	"context"
	"errors"
	"os"

	log "bitbucket.org/4s/go-logging"
	"bitbucket.org/4s/triaging-service/service"
)

// Main is the bootstrap-method that creates and starts the Triaging service
func main() {
	var ctx = context.Background()

	err := checkEnv()
	if err != nil {
		log.WithContext(ctx).Errorf("%v - shutting down", err)
		os.Exit(1)
	}

	triagingService := service.NewTriagingService(ctx)
	log.WithContext(ctx).Infof("Starting %s", os.Getenv("SERVICE_NAME"))
	err = triagingService.Triage(ctx)
	if err != nil {
		log.WithContext(ctx).Errorf("An error occurred while attempting to start %s: %v", os.Getenv("SERVICE_NAME"), err)
		os.Exit(1)
	}

	log.WithContext(ctx).Infof("%s stopped", os.Getenv("SERVICE_NAME"))
}

func checkEnv() error {
	envs := []string{
		"SERVICE_NAME",
		"FHIR_VERSION",
		"LOG_LEVEL",
		"KAFKA_BOOTSTRAP_SERVER",
		"KAFKA_SESSION_TIMEOUT_MS",
		"KAFKA_GROUP_ID",
		"KAFKA_ACKS",
		"KAFKA_RETRIES",
		"ENABLE_HEALTH",
		"PATIENTCARE_SERVICE_URL",
		"OFFICIAL_TASK_IDENTIFIER_SYSTEM",
		"TASK_TYPE_CODING_SYSTEM",
		"TASK_REASON_CODE_SYSTEM",
		"ENABLE_METRICS",
	}

	conditionalEnvs := map[string][]string{
		"ENABLE_HEALTH": []string{
			"HEALTH_FILE_PATH",
			"HEALTH_INTERVAL_MS",
		},
		"ENABLE_METRICS": []string{
			"PROMETHEUS_PUSHGATEWAY",
			"PROMETHEUS_JOB",
		},
	}

	for _, env := range envs {
		if os.Getenv(env) == "" {
			return errors.New(env + " not defined")
		}
	}

	for conditionalEnv, envSlice := range conditionalEnvs {
		if os.Getenv(conditionalEnv) == "true" {
			for _, env := range envSlice {
				if os.Getenv(env) == "" {
					return errors.New(conditionalEnv + " is true but " + env + " is not defined")
				}
			}
		}
	}

	return nil
}
