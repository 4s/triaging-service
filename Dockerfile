FROM golang:alpine

RUN apk add --update --no-cache alpine-sdk bash ca-certificates \
      libressl \
      tar \
      git openssh openssl yajl-dev zlib-dev cyrus-sasl-dev openssl-dev build-base coreutils
WORKDIR /root
RUN git clone https://github.com/edenhill/librdkafka.git
WORKDIR /root/librdkafka
RUN /root/librdkafka/configure
RUN make
RUN make install
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

ENV SRC_DIR=/go/src/bitbucket.org/4s/triaging-service
ADD . $SRC_DIR
WORKDIR $SRC_DIR

RUN go get -u github.com/google/go-cmp/cmp
RUN go get

ADD scripts/health.sh .

RUN go build -o triaging-service
CMD ["triaging-service"]