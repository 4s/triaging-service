# Design review af triageringsservice

Dato: d. 10.7.2018

Tilstede: JA, SHM, MC

_TS = TriageringsService_

  * Topic design: Have design session were we design and evaluate current topic design and how/if input/output topics 
 are configured.
  * CE-mærkningshensyn:
    * Skal være opmærksom på, at der hvor man udfylder/definerer grænseværdier skal CE-mærkes på samme niveau som 
Triageringsservice
    * Vi antager at andre laver valide og korrekte grænseværdier (Obs.def. med reference range)
  * Det skal ikke være andres ansvar at berige observationer med information om grænseværdier, da de så ender i samme
   CE-mærknings kategori som TS. Betyder at TS skal hente grænseværdi for patient fra database. Overvejelse: Skal 
   denne database være en del af TS? Nej, vi laver en ekstern FHIR dataservice til Obs.def. med reference ranges. 
   Sidstnævnte service skal ikke CE-mærkes og vi har således isoleret de ting som er CE-mærkningskrævende i TS.
  * Vi vender referencen mellem Obs.def. og patient om, således at Obs.def refererer til patient (muligvis via et 
  "associeringsobject"
  * Hvis obs. ikke har nogen obs.def, som passer til målingstypen, så gør den bare ingenting
  * Hvis obs. bruger én enhed og obs.def. bruger en anden, så bør den konvertere. Hvis den ikke ved, hvordan man 
  konverterer ml. to enheder smider den en notifikation omkring dette på beskedsystem
  
  * Fra Use Cases (https://issuetracker4s.atlassian.net/wiki/spaces/KOM/pages/1146943/Use+Cases):
	"Følgende kategorisering/farver tildeles i dag(heraf de første 4 identificeret som de vigtigste):
    * Grøn: normale målinger
    * Gul: afvigende eller undladte målinger
    * Rød: patologiske målinger
    * Orange: CTG eller blodsukker(vigtig pga. man kan se at man skal tjekke ctg, som systemet ikke selv kan 
		triagere)
    * Grå: Hvis der er sat svarfrister på og frist ikek overholdes så markeres med grå"

  * Problematik: ReferenceRange på obs. er ment som normal/optimal områder - 
  jf. http://hl7.org/fhir/2018May/valueset-referencerange-meaning.html. FHIR har ment flere ReferenceRange's som 
  forholdende sig til forskellige lidelser o.l. 
    * Vores bud er at vi udvider "ReferenceRange.type", så man kan have et warningområde. Så er alt udenfor 
		warningområdet en alarm (rød)
    * MC skriv på FHIR zulip og forklar use case
  * Hvis der ikke er en type eller der på anden vis mangler data eller der kræves manuel indgriben, så sætter vi 
  Observation.interpretation til IE ("Insuf. Evidence)

  * Hvad gør vi med triagering af QuestionnaireResponse? På Questionnaire lave mulighed for ReferenceAnswer's m. 
  "enableWhen" (som ved branching i Questionnaire's)
  * Composition: FHIR skriver, at skal bruges til at lave et klinisk dokument og optræder altid i sammenhæng med 
	Bundle af typen Document.