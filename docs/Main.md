# Triaging service

The Triaging Service is a service that triages incoming Observation and QuestionnaireResponse resources 
on Kafka, using ThresholdSets.

## Maturity / Status

4S maturity level: Prototype

## Use cases

[todo]

## Design

This service subscribes to incoming FHIR Observations and Questionnaire Responses on the kafka topics
"DataCreated_FHIR\_\<Observation/QuestionnaireResponse>\_\<Datacode>". 

Once the service has received the Observation or the Questionnaire Response, the service retrieves the CarePlan, 
based on which the Observation or QuestionnaireResponse is produced, and from that, extracts a ThresholdSet resource 
(in the form of a FHIR Basic Resource) that should be used to triage the Observation or the Questionnaire Response. If no ThresholdSet is found, the Triaging Service will not triage the resource, but will instead create a Task resource specifying the need for manual intervention by a clinician. 

Once the ThresholdSet has been retrieved, the Triaging service will extract the value/values from the 
Observation/Questionnaire Response and evaluate these against the thresholds specified in the ThresholdSet. Based on 
this evaluation, the Triaging service will append an interpretation to the Observation or QuestionnaireResponse as 
either "normal", "abnormal" or "pathological", based on which threshold, the value/values of the Observation or 
QuestionnaireResponse falls into. If either the value does not fit within one of the thresholds, or the value is 
missing, the Triaging Service will not triage the resource, and instead a Task will be created specifying the need for 
manual intervention by a clinician. 

Once the resource has been triaged and an interpretation is appended to the resource, a canonical reference to the 
ThresholdSet used to interpret the resource is appended to the resource, and the resource is produced back to kafka on 
the topic "Update_FHIR\_\<Resource-type>\_\<Datacode>".

![Sequence diagram](seq.png)

## Messaging API

The specification of the Kafka messaging API for this service is desribed in the file [triaging-service.yaml](triaging-service.yaml) 
(also see API [README.md](apis/README.md))

## Known errors / shortcomings

- So far, no authorization has been implemented.

## Decisionmaking
- Quantities will only be accepted if both a system and a code for the unit is defined.
- The comparator-field of quantity is not used