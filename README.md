# Triaging Service:


| Summary    |                                                                                                                     |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------|
| Service name (computer friendly) | triaging-service                                                                                                        |
| Maturity level                   | Concept [ ] Prototype [ ] Demonstration [x] Product [ ]                                                                                                         |
| Used externally                  | no                                                                  |
| Database                         | no                                                                                                                  |
| RESTful dependencies             | patientcare-service   |
| Messaging dependencies           | Kafka                                                                                                               |
| Other dependencies               | n/a                                                            |
| License                          | Apache 2.0                                                                                                          |

The triaging-service is a microservice intented for use in a microservice based system as the one described at 
[GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md). The service is a 
_stateless computation services_ as described in [GMK Backend Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/HFS.md). 
It is an asynchronous service written in Golang (https://golang.org/) that consumes messages 
on Kafka on the input topics DataCreated_FHIR_Observation and DataCreated_FHIR_QuestionnaireResponse and produces 
messages to the output topics Update_FHIR_Observation and Update_FHIR_QuestionnaireResponse.

Upon receiving a FHIR Observation or QuestionnaireResponse resource through Kafka, the service retrieves the CarePlan, 
based on which the Observation or QuestionnaireResponse is produced, and from that, extracts a ThresholdSet resource 
(in the form of a FHIR Basic Resource). 
See more about the use of these FHIR resources and how they relate to other resources in the system on the pages about
the [FHIR data model](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview) 

Using the thresholds in the ThresholdSet, the service calculates an interpretation of the values of the 
Observation or QuestionnaireResponse resource as either "normal", "abnormal", or "pathological". The interpretation
is appended to the Observation or QuestionnaireResponse resource, along with a reference to the ThresholdSet used
to conduct the interpretation, and the resource is the produced back to Kafka as an Update-event.

## Documentation

  - [GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md)
  - Design documentation, FHIR API and messaging API documentation: See [docs](docs//) folder.
  - Use of these FHIR resources and how they relate to other resources in the system on the pages about the [FHIR data model](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview) 
  - Public classes and methods are documented with godoc 
  - More on [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/).

## Prerequisites
You need to have the following software installed
Either:

 * Docker 1.13+ (https://www.docker.com/)

Or:

 * Go (golang) (https://golang.org/)
 * librdkafka (https://github.com/confluentinc/confluent-kafka-go#installing-librdkafka)

If you are running the service using docker-compose (see below) you also need to have a docker network by the name of
`opentele3net` setup:

`docker network create opentele3net`

## Building and running

Running the triaging service requires that you fulfill the prerequisites listed above. Furthermore
there must be a running instance of Apache Kafka (https://kafka.apache.org/), from and to which this 
service can consume and produce.

### Docker

To build and run with docker (and docker-compose) follow the steps below in the root folder:

1. Find the current version of the Triaging-service in the VERSION-file
2. Execute:
```bash
docker build -t triaging-service:<current-version> .
```
3. Change the "image"-field in the docker-compose.yml file to triaging-service:<current-version>
4. Execute:
```
bash docker-compose up
``` 

### Go

To build and run with go follow the steps below in the root folder:

1. Execute:
```bash
go get
```
2. Execute:
```bash
go build -o triaging-service
```
3. Execute:
```bash
./triaging-service
```
## Monitoring
With the following environment variables you may enable that instances of the KafkaProcessAndConsume class touch a file at specified intervals:

ENABLE_HEALTH=true
HEALTH_FILE_PATH=/tmp/health
HEALTH_INTERVAL_MS=5000

This can be used to check the health of the KafkaProcessAndConsume thread - typically by having a shell script check on 
the timestamp of the health file.

## Environment variables

In the file `service.env`, you must provide the correct values for the necessary environment variables used in this 
microservice.

| Environment variable            | Required  | Description                                                                                                                     |
|---------------------------------|-----------|----------------------------------------------------------------------
| SERVICE_NAME                    | y         | Computer friendly name of this service. 
| FHIR_VERSION                    | y         | Version of HL7 FHIR supported by this service
| LOG_LEVEL                       | y         | The log-level of this service
| LOGGER_NAME                     | n         | A name that will be added to each log-message
| *Messaging related:*            ||
| KAFKA_BOOTSTRAP_SERVER          | y         | see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_SESSION_TIMEOUT_MS        | y         | see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_GROUP_ID                  | y         | see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_ACKS                      | y         | see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_RETRIES                   | y         | see https://bitbucket.org/4s/messaging/src/master/
| *Healthcheck related:*          ||
| ENABLE_HEALTH                   | y         | see https://bitbucket.org/4s/messaging/src/master/
| HEALTH_FILE_PATH                | n         | Required if ENABLE_HEALTH=true see https://bitbucket.org/4s/messaging/src/master/
| HEALTH_INTERVAL_MS              | n         | Required if ENABLE_HEALTH=true see https://bitbucket.org/4s/messaging/src/master/
| *Other Services:*               ||
| PATIENTCARE_SERVICE_URL        | y         | The address of the Patientcare-service
| *Coding Systems*                ||
| OFFICIAL_TASK_IDENTIFIER_SYSTEM | y         | The official identifier system of tasks
| TASK_TYPE_CODING_SYSTEM         | y         | The coding system of the task type
| TASK_REASON_CODE_SYSTEM         | y         | The coding system of the task reasonCode
| *Metrics*                       ||
| ENABLE_METRICS                  | y         | Whether to enable Prometheus Metrics through a Pushgateway
| PROMETHEUS_PUSHGATEWAY          | n         | Required if ENABLE_METRICS=true - The URL of the Prometheus Pushgateway
| PROMETHEUS_JOB                  | n         | Required if ENABLE_METRICS=true - The job on which to push the metrics

*N.B. Be aware that the environment variable ENABLE_KAFKA (mentioned here: https://bitbucket.org/4s/messaging/src/master/)
is intentionally left out, as this service only communicates through Kafka and therefore does not function without Kafka 
enabled. ENABLE_KAFKA is on by default and cannot be disabled.*

When running the service with go (instead of docker), the variables defined in `service.env` must be set on the machine, on which it is run

## Test
To do a basic test of the service you may run

```bash
go test -v ./test
```

To do full test with integration tests, an instance of patientcare-service 
(https://bitbucket.org/4s/patientcare-service/src/master/) and Apache Kafka (https://kafka.apache.org/) must be 
running and accessible to this service. To make patientcare-service accesible to this service, the entry in 
service.env named "PATIENTCARE_SERVICE_URL" must point to the URL of the patientcare-service. 
Furthermore, the patientcare-service must have a specific careplan in its database. The careplan can be found 
(embedded in a message) in the test/testdata-directory under the name "it_test_careplan_message.json". 
To save the careplan to the patientcare-service, you must send the careplan through kafka on the topic 
"Create_FHIR_CarePlan". This can be done by using kafkacat (https://github.com/edenhill/kafkacat) using 
the following command 
```bash
kafkacat -P -b <url-to-kafka> -t Create_FHIR_CarePlan <path-to-testdata>/it_test_careplan_message.json
```
or in docker by using the following command 
```bash
docker run --network <network> --volume <path-to-testdata>/it_test_careplan_message.json:/data/message.json confluentinc/cp-kafkacat kafkacat -b <url-to-kafka> -t Create_FHIR_CarePlan -P -l /data/message.json
```

once the patientcare-service is running, run the following to execute the tests with integration tests:

```bash
go test -v -tags integration ./test
```

The test can also be executed in a docker container. To do so run the following

1. Find the current version of the Triaging-service in the VERSION file
2. Execute:
```bash
docker build -t triaging-service:<current-version> .
```
1. Execute:
```bash
docker run --rm -it --name triaging-test --network --env-file service.env opentele3net triaging-service:<current-version> go test -v ./test
```
or
```bash
docker run --rm -it --name triaging-test --network --env-file service.env opentele3net triaging-service:<current-version> go test -v -tags integration ./test
```