package processing

import (
	"context"
	"encoding/json"
	"errors"
	"os"
	"time"

	"github.com/google/uuid"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	"bitbucket.org/4s/triaging-service/domain"
	"bitbucket.org/4s/triaging-service/domain/types"
	"bitbucket.org/4s/triaging-service/network"
)

// MyEventProcessor is a struct representing MyEventProcessor which processes events received from
// Kafka, and returns a new message based on this processing
type MyEventProcessor struct {
	requestHandler network.RequestHandler
}

// NewMyEventProcessor functions as a constructor for MyEventProcessor that returns a new instance of MyEventProcessor
func NewMyEventProcessor(ctx context.Context) (MyEventProcessor, error) {

	// get URL for patient care service service
	patientCareServiceURL := os.Getenv("PATIENTCARE_SERVICE_URL")
	if patientCareServiceURL == "" {
		return MyEventProcessor{}, errors.New("PATIENTCARE_SERVICE_URL is not defined")
	}
	log.WithContext(ctx).Debugf("PATIENTCARE_SERVICE_URL set to: %v", patientCareServiceURL)

	requestHandler := network.NewRestRequestHandler(ctx, patientCareServiceURL)

	mep := MyEventProcessor{
		requestHandler,
	}

	return mep, nil
}

// SetRequestHandler sets request handler for MyEventProcessor
func (mep *MyEventProcessor) SetRequestHandler(requestHandler network.RequestHandler) {
	mep.requestHandler = requestHandler
}

// Processing variables

// BasedOn is the reference to the careplan from which the outcomeresource was ordered
var BasedOn *[]types.Reference

// Identifier is the identifier for the outcome resource
var Identifier *types.Identifier

// Subject is the patientreference from the outcome resource
var Subject *types.Reference

// Organization is the organization in charge of the patient in this context
var Organization *types.Reference

// ProcessMessage reads the value from the incoming message, retrieves a ThresholdSet for
// that particular Observation or QuestionnaireResponse resource, calculates an interpretation and inserts that into
// the Observation or QuestionnaireResponse resource, along with a reference to the thresholdset used to calculate the
// interpretation.
func (mep MyEventProcessor) ProcessMessage(
	ctx context.Context,
	consumedTopic messaging.Topic,
	receivedMessage messaging.Message,
	messageProcessedTopic *messaging.Topic,
	outgoingMessage *messaging.Message,
) error {
	ctx = log.NewContext(ctx, map[string]interface{}{"processingId": uuid.New().String()})
	log.WithContext(ctx).Infof("Processing incoming resource on topic: %s", consumedTopic.String())
	log.WithContext(ctx).Tracef("Incoming message: %s", receivedMessage.ToByteArray()[:])

	// Set output topic
	messageProcessedTopic.SetOperation(messaging.Update)
	messageProcessedTopic.SetDataCategory(messaging.FHIR)
	messageProcessedTopic.SetDataType(consumedTopic.GetDataType())

	log.WithContext(ctx).Debugf("Output topic %v set in MyEventProcessor", messageProcessedTopic.String())

	var triagedResource []byte

	switch consumedTopic.GetDataType() {
	// OBSERVATION
	case "Observation":
		ctx = log.NewContext(ctx, map[string]interface{}{"resourceType": "Observation"})
		log.WithContext(ctx).Debugf("Received resource is of type Observation")
		// unmarshall messagebody to Observation resource
		var observation types.Observation
		err := json.Unmarshal([]byte(receivedMessage.GetBody()), &observation)
		if err != nil {
			log.WithContext(ctx).Warn("Unmarshalling message body to Observation failed: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Observation unmarshalled successfully")

		// add observation code to logs
		if observation.Code != nil ||
			*(observation).Code != (types.CodeableConcept{}) ||
			len(*(*(observation).Code).Coding) != 0 ||
			(*(*(observation).Code).Coding)[0] != (types.Coding{}) ||
			(*(*(observation).Code).Coding)[0].Code != nil ||
			*(*(*(observation).Code).Coding)[0].Code != "" {
			ctx = log.NewContext(ctx, map[string]interface{}{"code": *(*(*(observation).Code).Coding)[0].Code})
		}

		// check that Observation contains the necessary information to continue

		// careplan reference
		if observation.BasedOn == nil || len((*(observation).BasedOn)) < 1 {
			err := errors.New("Cannot retrieve ThresholdSet for Observation: No careplan defined")
			log.WithContext(ctx).Error(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		if (*observation.BasedOn)[0].Identifier == nil {
			err := errors.New(
				"Cannot retrieve ThresholdSet for Observation: Reference to careplan does not include an identifier",
			)
			log.WithContext(ctx).Warn(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		// set basedOn variable for possible future use
		BasedOn = observation.BasedOn
		log.WithContext(ctx).Tracef("Observation contains a reference to careplan")

		// identifier
		if observation.Identifier == nil || len(*observation.Identifier) < 1 {
			log.WithContext(ctx).Warn("Malformed resource: no Identifier set for Observation")
		} else {
			Identifier = &(*(observation).Identifier)[0]
			log.WithContext(ctx).Tracef("Observation contains an identifier")
		}

		// patient reference
		if observation.Subject == nil {
			log.WithContext(ctx).Warn("Malformed resource: no Subject set for Observation")
		} else {
			Subject = observation.Subject
			log.WithContext(ctx).Tracef("Observation contains a subject")
		}

		// observation definition reference
		observationDefinitionCanonical := mep.getObservationDefinitionCanonical(observation)
		if observationDefinitionCanonical == nil {
			err := errors.New(
				"Cannot retrieve ThresholdSet for Observation: no or malformed canonical reference to observation definition",
			)
			log.WithContext(ctx).Warn(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		// retrieve CarePlan bundle from PatientCare-service
		response, err := mep.requestHandler.GetCarePlan(
			ctx,
			*(*observation.BasedOn)[0].Identifier,
			receivedMessage.GetSession(),
			receivedMessage.GetSecurity(),
		)
		if err != nil {
			log.WithContext(ctx).Warn("Error while retrieving CarePlan: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Careplan bundle succesfully retrieved from patientcare-service")

		// unmarshal CarePlan Bundle
		var bundle types.Bundle
		err = json.Unmarshal(response, &bundle)
		if err != nil {
			log.WithContext(ctx).Warn("Error while unmarshalling Bundle containing CarePlan: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Careplan bundle unmarshalled succesfully")

		// extract CarePlan from bundle
		if bundle.Entry == nil || len((*(bundle).Entry)) < 1 {
			err := errors.New("Error extracting CarePlan from bundle: bundle contains no entries")
			log.WithContext(ctx).Warn(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		if (*(bundle).Entry)[0].Resource == nil {
			err := errors.New("Error extracting Basic from bundle: entry in bundle contains no resource")
			log.WithContext(ctx).Warn(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}

		// extract Basic from CarePlan
		careplan := (*(bundle).Entry)[0].Resource
		if careplan.Author == nil || *careplan.Author == (types.Reference{}) {
			err := errors.New(
				"CarePlan does not have an author",
			)
			log.WithContext(ctx).Warn(err)
		} else {
			Organization = careplan.Author

			log.WithContext(ctx).Tracef("CarePlan Author is %v", *(careplan).Author)

		}

		basic, err := domain.ExtractThresholdSetFromCarePlan(ctx, *careplan, observationDefinitionCanonical)
		if err != nil || basic == (types.Basic{}) {
			log.WithContext(ctx).Errorf("Error while extracting ThresholdSet for ObservationDefinition from CarePlan: %v", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Basic succesfully extracted from careplan")
		if basic.ID != nil {
			log.WithContext(ctx).Tracef("Basic ID is %v", *(basic).ID)
		}

		// convert Basic to ThresholdSet
		thresholdSet, err := domain.ConvertToThresholdSet(basic)
		if err != nil {
			log.WithContext(ctx).Warn("Error while converting Basic resource to ThresholdSet: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Basic succesfully converted to thresholdset resource")

		// interpret observation
		interpretation, err := domain.InterpretObservation(ctx, observation, thresholdSet)
		if err != nil {
			log.WithContext(ctx).Error("Error calculating interpretation: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Interpretation of Observation completed succesfully")
		if observation.Interpretation == nil {
			interpretationArray := []types.CodeableConcept{}
			observation.Interpretation = &interpretationArray
		}

		// add interpretation to resource
		*observation.Interpretation = append(*observation.Interpretation, interpretation)

		// add reference to thresholdset
		if observation.Resource == nil {
			resource := new(types.Resource)
			observation.Resource = resource
		}
		if observation.DomainResource == nil {
			domainResource := new(types.DomainResource)
			observation.DomainResource = domainResource
		}

		if observation.Extension == nil {
			extensionArray := new([]types.Extension)
			observation.Extension = extensionArray
		}
		thresholdSetCanonical, err := mep.CreateThresholdSetCanonical(thresholdSet.Identifier, thresholdSet.Version)
		if err == nil {
			*observation.Extension = append(*observation.Extension, thresholdSetCanonical)
		} else {
			log.WithContext(ctx).Error("An error occurred while adding the canonical url to the Observation", err)
		}

		// marshal observation back into JSON
		triagedResource, err = json.Marshal(observation)
		if err != nil {
			log.WithContext(ctx).Error("Error marshalling Observation back into JSON: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Observation succesfully marshalled back into json")
	// QUESTIONNAIRERESPONSE
	case "QuestionnaireResponse":
		ctx = log.NewContext(ctx, map[string]interface{}{"resourceType": "QuestionnaireResponse"})
		log.WithContext(ctx).Debugf("Received resource is of type QuestionnaireResponse")

		// unmarshal messagebody to QuestionnaireResponse resource
		var questionnaireResponse types.QuestionnaireResponse
		err := json.Unmarshal([]byte(receivedMessage.GetBody()), &questionnaireResponse)
		if err != nil {
			log.WithContext(ctx).Warn("Unmarshalling message body to QuestionnaireResponse failed: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("QuestionnaireResponse unmarshalled successfully")

		// check if questionnaire response has a careplan reference
		if questionnaireResponse.BasedOn == nil || len((*(questionnaireResponse).BasedOn)) < 1 {
			err := errors.New("Cannot retrieve ThresholdSet for QuestionnaireResponse: No careplan defined")
			log.WithContext(ctx).Warn(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		if (*questionnaireResponse.BasedOn)[0].Identifier == nil {
			err := errors.New(
				"Cannot retrieve ThresholdSet for QuestionnaireResponse: Reference to careplan does not include an identifier",
			)
			log.WithContext(ctx).Warn(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		// set basedOn variable for possible future use
		BasedOn = questionnaireResponse.BasedOn
		log.WithContext(ctx).Trace("QuestionnaireResponse contains a reference to careplan")

		// check if questionnaire response has a questionnaire reference
		if questionnaireResponse.Questionnaire == nil {
			err := errors.New(
				"Cannot retrieve ThresholdSet for QuestionnaireResponse: no Questionnaire set for QuestionnaireResponse",
			)
			log.WithContext(ctx).Warn(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		// add Questionnaire canonical to logs
		ctx = log.NewContext(ctx, map[string]interface{}{"questionnaire": *(questionnaireResponse).Questionnaire})
		log.WithContext(ctx).Tracef("QuestionnaireResponse contains a reference to questionnaire")

		// check if questionnaire response has an identifier
		if questionnaireResponse.Identifier == nil {
			log.WithContext(ctx).Warn("Malformed resource: no Identifier set for QuestionnaireResponse")
		} else {
			Identifier = questionnaireResponse.Identifier
			log.WithContext(ctx).Tracef("QuestionnaireResponse contains an identifier")
		}

		// check if questionnaire response has a patient reference
		if questionnaireResponse.Subject == nil {
			log.WithContext(ctx).Warn("Malformed resource: no Subject set for QuestionnaireResponse")
		} else {
			Subject = questionnaireResponse.Subject
			log.WithContext(ctx).Tracef("QuestionnaireResponse contains a subject")
		}

		// retrieve CarePlan bundle from PatientCare-service
		response, err := mep.requestHandler.GetCarePlan(
			ctx,
			*(*questionnaireResponse.BasedOn)[0].Identifier,
			receivedMessage.GetSession(),
			receivedMessage.GetSecurity(),
		)
		if err != nil {
			log.WithContext(ctx).Warn("Error while retrieving CarePlan: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Careplan bundle succesfully retrieved from patientcare-service")

		// unmarshal CarePlan Bundle
		var bundle types.Bundle
		err = json.Unmarshal(response, &bundle)
		if err != nil {
			log.WithContext(ctx).Warn("Error while unmarshalling Bundle containing CarePlan: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Careplan bundle unmarshalled succesfully")

		// extract CarePlan from bundle
		if bundle.Entry == nil || len((*(bundle).Entry)) < 1 {
			err := errors.New("Error extracting CarePlan from bundle: bundle contains no entries")
			log.WithContext(ctx).Warn(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		if (*(bundle).Entry)[0].Resource == nil {
			err := errors.New("Error extracting Basic from bundle: entry in bundle contains no resource")
			log.WithContext(ctx).Warn(err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}

		// extract Basic from CarePlan
		log.WithContext(ctx).Debug("Extracting basic from careplan")
		careplan := (*(bundle).Entry)[0].Resource
		basic, err := domain.ExtractThresholdSetFromCarePlan(ctx, *careplan, questionnaireResponse.Questionnaire)
		if err != nil {
			log.WithContext(ctx).Warn("Error while extracting thresholdset from careplan: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Basic successfully extracted from careplan")
		if basic.Resource != nil && basic.ID != nil {
			log.WithContext(ctx).Tracef("Basic ID is %v", *(basic).ID)
		}

		// convert Basic to ThresholdSet
		log.WithContext(ctx).Debug("Converting basic to thresholdset")
		thresholdSet, err := domain.ConvertToThresholdSet(basic)
		if err != nil {
			log.WithContext(ctx).Warn("Error while converting Basic resource to ThresholdSet for QuestionnaireResponse: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Basic succesfully converted to thresholdset resource")

		// calculate interpretation
		interpretation, err := domain.InterpretQuestionnaireResponse(ctx, questionnaireResponse, thresholdSet)
		if err != nil {
			log.WithContext(ctx).Error("Error calculating interpretation: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
		log.WithContext(ctx).Debug("Interpretation of QuestionnaireResponse completed succesfully")

		// add interpretation to resource
		interpretationExtension := new(types.Extension)
		interpretationExtensionURL := domain.QuestionnaireResponseExtensions.InterpretationURL
		interpretationExtension.URL = &interpretationExtensionURL
		interpretationExtension.ValueCodeableConcept = &interpretation
		if questionnaireResponse.Resource == nil {
			resource := new(types.Resource)
			questionnaireResponse.Resource = resource
		}
		if questionnaireResponse.DomainResource == nil {
			domainResource := new(types.DomainResource)
			questionnaireResponse.DomainResource = domainResource
		}

		if questionnaireResponse.Extension == nil {
			extensionArray := new([]types.Extension)
			questionnaireResponse.Extension = extensionArray
		}

		*questionnaireResponse.Extension = append(*questionnaireResponse.Extension, *interpretationExtension)

		// add reference to thresholdset
		thresholdSetCanonical, err := mep.CreateThresholdSetCanonical(thresholdSet.Identifier, thresholdSet.Version)
		if err == nil {
			*questionnaireResponse.Extension = append(*questionnaireResponse.Extension, thresholdSetCanonical)
		} else {
			log.WithContext(ctx).Error("En error occurred while adding the canonical url to the QuestionnaireResponse", err)
			return err
		}

		// marshall questionnaire response back into JSON
		triagedResource, err = json.Marshal(questionnaireResponse)
		if err != nil {
			log.WithContext(ctx).Error("Error marshalling questionnaire response back into JSON: ", err)
			mep.CreateTask(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage, types.Code("missing-information"))
			return err
		}
	}

	log.WithContext(ctx).Info("Triaging of resource completed succesfully")
	outgoingMessage.SetBody(string(triagedResource))
	log.WithContext(ctx).Tracef("Outgoing message: %s", outgoingMessage.ToByteArray()[:])

	// reset processing variables
	BasedOn = nil
	Identifier = nil
	Subject = nil

	return nil
}

// CreateTask creates a Task for clinicians to manually intervene based on the received message, and sets that as the
// body of the outgoingMessage as well as changes the topic to Create_FHIR_Task
func (mep *MyEventProcessor) CreateTask(
	ctx context.Context,
	consumedTopic messaging.Topic,
	receivedMessage messaging.Message,
	messageProcessedTopic *messaging.Topic,
	outgoingMessage *messaging.Message,
	reason types.Code,
) {

	log.WithContext(ctx).Infof("Creating Task for CorrelationID %v with reason %v", receivedMessage.GetCorrelationID(), reason)

	messageProcessedTopic.SetOperation(messaging.Create)
	messageProcessedTopic.SetDataCategory(messaging.FHIR)
	messageProcessedTopic.SetDataType("Task")
	log.WithContext(ctx).Infof("Outputtopic set to %v", messageProcessedTopic.String())

	task := new(types.Task)
	resourceType := "Task"
	resource := new(types.Resource)
	resource.ResourceType = &resourceType
	task.Resource = resource

	// identifier
	taskSystem := types.URI(os.Getenv("OFFICIAL_TASK_IDENTIFIER_SYSTEM"))
	if taskSystem == "" {
		taskSystem = types.URI("urn:ietf:rfc:3986")
	}
	identifierSystem := taskSystem
	identifierValue := types.String(uuid.New().String())
	identifiers := []types.Identifier{}
	identifier := types.Identifier{
		System: &identifierSystem,
		Value:  &identifierValue,
	}
	identifiers = append(identifiers, identifier)
	task.Identifier = &identifiers

	// basedOn
	task.BasedOn = BasedOn

	// status
	status := types.Code("requested")
	task.Status = &status

	// code
	codeCoding := new(types.Coding)
	codeCodingSystem := types.URI(os.Getenv("TASK_TYPE_CODING_SYSTEM"))
	codeCodingCode := types.Code("review")
	codeCoding.System = &codeCodingSystem
	codeCoding.Code = &codeCodingCode

	code := new(types.CodeableConcept)
	codeCodings := new(types.Codings)
	*codeCodings = append(*codeCodings, *codeCoding)
	code.Coding = codeCodings
	task.Code = code

	// focus
	if Identifier != nil {
		focus := new(types.Reference)
		focus.Identifier = Identifier
		bodyType := types.String(receivedMessage.GetBodyType())
		focus.Type = &bodyType
		task.Focus = focus
	}

	// for
	task.For = Subject

	// authoredOn
	now := time.Now()
	authoredOn := types.DateTime(now.Format(time.RFC3339))
	task.AuthoredOn = &authoredOn

	// lastModified
	task.LastModified = &authoredOn

	// owner
	task.Owner = Organization

	// reasonCode
	reasonCodeCoding := new(types.Coding)
	reasonCodeCodingSystem := types.URI(os.Getenv("TASK_REASON_CODE_SYSTEM"))
	reasonCodeCoding.System = &reasonCodeCodingSystem
	reasonCodeCoding.Code = &reason

	reasonCode := new(types.CodeableConcept)
	reasonCodeCodings := new(types.Codings)
	*reasonCodeCodings = append(*reasonCodeCodings, *reasonCodeCoding)
	reasonCode.Coding = reasonCodeCodings
	task.ReasonCode = reasonCode

	// marshall task into JSON
	taskJSON, err := json.Marshal(task)
	if err != nil {
		log.WithContext(ctx).Error("Error marshalling task into JSON: ", err)
		*outgoingMessage = messaging.Message{}
	}

	outgoingMessage.SetBodyType("Task")
	outgoingMessage.SetBody(string(taskJSON))
}

// CreateThresholdSetCanonical creates a canonical reference to the thresholdset provided
func (mep *MyEventProcessor) CreateThresholdSetCanonical(identifier *[]types.Identifier, version *types.String) (types.Extension, error) {
	canonicalReference := types.Extension{}

	if identifier == nil ||
		len(*identifier) == 0 ||
		(*identifier)[0].System == nil ||
		*(*identifier)[0].System == "" ||
		(*identifier)[0].Value == nil ||
		*(*identifier)[0].Value == "" {
		return canonicalReference, errors.New("Cannot create ThresholdSet canonical - identifier malformed")
	}

	if *(*identifier)[0].System != "urn:ietf:rfc:3986" {
		return canonicalReference, errors.New("Cannot create ThresholdSet canonical - No UUID identifier")
	}
	canonicalString := "canonical://uuid/Basic/"
	canonicalString += string(*(*identifier)[0].Value)
	if version != nil && *version != "" {
		canonicalString += "|" + string(*version)
	}
	canonical := types.Canonical(canonicalString)

	canonicalReference.URL = &domain.OutcomeExtensions.ThresholdSetCanonicalURL
	canonicalReference.ValueCanonical = &canonical

	return canonicalReference, nil
}

func (mep *MyEventProcessor) getObservationDefinitionCanonical(observation types.Observation) *types.Canonical {
	var canonical types.Canonical
	if observation.DomainResource == nil {
		return nil
	}
	if observation.Extension == nil {
		return nil
	}
	for _, extension := range *observation.Extension {
		if extension.URL == nil || *extension.URL != "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110723073/Observation+definition+extension" {
			continue
		}
		if extension.ValueCanonical == nil ||
			*extension.ValueCanonical == "" {
			continue
		}

		canonical = *extension.ValueCanonical
	}

	return &canonical
}
