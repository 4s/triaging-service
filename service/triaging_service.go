package service

import (
	"context"
	"os"
	"os/signal"
	"regexp"
	"sync"
	"syscall"
	"time"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	"bitbucket.org/4s/go-messaging/implementations/kafka"
	"bitbucket.org/4s/triaging-service/metrics"
	"bitbucket.org/4s/triaging-service/processing"
)

// TriagingService is a struct that handles creating and initiating the KafkaConsumeAndProcess struct
// which runs the main loop of this service.
type TriagingService struct {
	topics            messaging.Topics
	producer          messaging.EventProducer
	eventProcessor    messaging.EventProcessor
	consumeAndProcess messaging.ConsumeAndProcess
	timeout           time.Duration
}

// NewTriagingService functions as a constructor that returns a new instance of TriagingService
func NewTriagingService(ctx context.Context) TriagingService {

	// Create topics
	regex := regexp.MustCompile("DataCreated_FHIR_QuestionnaireResponse|DataCreated_FHIR_Observation_(.*)")

	// Create event processor
	eventProcessor, err := processing.NewMyEventProcessor(ctx)
	if err != nil {
		log.WithContext(ctx).Error("Error while creating MyEventProcessor: ", err)
		os.Exit(1)
	}

	// Create kafka consumer
	consumer, err := kafka.NewEventConsumer(ctx, eventProcessor)
	if err != nil {
		log.WithContext(ctx).Error("Error while creating KafkaEventProducer: ", err)
		os.Exit(1)
	}
	// consumer.SubscribeTopics(topics)
	consumer.SubscribeTopicPattern(regex)

	// Create kafka producer
	producer, err := kafka.NewEventProducer(ctx, os.Getenv("SERVICE_NAME"))
	if err != nil {
		log.WithContext(ctx).Error("Error while creating KafkaEventProducer: ", err)
		os.Exit(1)
	}

	// Create and return triaging service
	triagingService := TriagingService{}
	triagingService.SetProducer(&producer)
	triagingService.SetEventProcessor(eventProcessor)
	consumeAndProcess, err := messaging.NewConsumeAndProcess(ctx, &consumer, &producer)
	if err != nil {
		log.WithContext(ctx).Error("Error while attempting to create KafkaConsumeAndProcess: ", err)
		os.Exit(1)
	}

	if os.Getenv("ENABLE_METRICS") == "true" {
		interceptorAdaptor, err := metrics.NewPrometheusInterceptorAdaptor(ctx)
		if err != nil {
			log.WithContext(ctx).Warnf("An error occurred while creating PrometheusInterceptorAdaptor: %s. continuing without prometheus metrics", err)
		} else {
			consumeAndProcess.SetInterceptors(interceptorAdaptor)
		}
	}
	triagingService.SetConsumeAndProcess(consumeAndProcess)

	return triagingService
}

// SetInputTopics sets the inputtopics for this service
func (ts *TriagingService) SetInputTopics(topics messaging.Topics) {
	ts.topics = topics
}

// SetProducer sets the producer used in KafkaConsumeAndProcess, allowing a producer different from
// the one created in the constructor to be injected.
func (ts *TriagingService) SetProducer(producer messaging.EventProducer) {
	ts.producer = producer
}

// SetEventProcessor sets the eventprocessor used in KafkaConsumeAndProcess, allowing an eventprocessor different from
// the one created in the constructor to be injected.
func (ts *TriagingService) SetEventProcessor(eventProcessor messaging.EventProcessor) {
	ts.eventProcessor = eventProcessor
}

// SetConsumeAndProcess sets the consumeAndProcess struct used in triagingservice, allowing a consumeAndProcess
// different from the one created in the constructor to be injected.
func (ts *TriagingService) SetConsumeAndProcess(consumeAndProcess messaging.ConsumeAndProcess) {
	ts.consumeAndProcess = consumeAndProcess
}

// SetTimeout sets the timeout variable in the TriagingService struct. This variable allows for the
// caller to decide a specific amount of time, this TriagingService should run. This comes in handy
// when testing.
func (ts *TriagingService) SetTimeout(timeout time.Duration) {
	ts.timeout = timeout
}

// Triage is the main method of the TriagingService, which initiates the KafkaConsumeAndProcess run-
// goroutine and handles system interrupts.
func (ts *TriagingService) Triage(ctx context.Context) error {
	// Start service with created topic and producer
	var wg sync.WaitGroup
	wg.Add(1)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	log.WithContext(ctx).Debug("Starting KafkaConsumeAndProcess goroutine")
	go ts.consumeAndProcess.Run(ctx, sigs, &wg)

	// Check if timeout has been specified - if so, send an interrupt signal after timeout has passed
	if ts.timeout != 0 {
		log.WithContext(ctx).Debug("Timeout set to: " + ts.timeout.String())
		time.Sleep(ts.timeout)
		p, err := os.FindProcess(os.Getpid())
		if err != nil {
			log.WithContext(ctx).Error("Error when attempting to find the consumeAndRun goroutine: ", err)
			os.Exit(1)
		}
		err = p.Signal(os.Interrupt)
	}

	wg.Wait()
	return nil
}
