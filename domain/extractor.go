package domain

import (
	"context"
	"encoding/json"
	"errors"
	"strings"

	"bitbucket.org/4s/triaging-service/domain/types"
)

// ExtractThresholdSetFromCarePlan extracts a thresholdSet from a CarePlan's contained array based on provided canonical
func ExtractThresholdSetFromCarePlan(
	ctx context.Context,
	careplan types.CarePlan,
	canonical *types.Canonical,
) (types.Basic, error) {

	var thresholdSetReferences = []types.String{}
	if careplan.Extension == nil || len(*careplan.Extension) < 1 {
		return types.Basic{}, errors.New("CarePlan contains no extensions")
	}
	for _, extension := range *careplan.Extension {
		if extension.URL == nil || *extension.URL != "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728833/CarePlan+thresholdset+extension" {
			continue
		}
		if extension.ValueReference == nil ||
			*extension.ValueReference == (types.Reference{}) ||
			*extension.ValueReference.Reference == "" {
			continue
		}
		thresholdSetReferences = append(thresholdSetReferences, *extension.ValueReference.Reference)
	}

	var basic = types.Basic{}
	for _, reference := range thresholdSetReferences {
		candidate := getContainedBasicByID(careplan, reference)

		for _, extension := range *candidate.Extension {
			if extension.URL == nil || *extension.URL != ThresholdSetExtensions.AppliesToURL {
				continue
			}
			if extension.ValueCanonical == nil ||
				*extension.ValueCanonical == "" {
				continue
			}
			if *extension.ValueCanonical == *canonical {
				basic = candidate
			} else {
			}
		}
	}

	return basic, nil
}

// getContainedBasicByID retrieves a Basic resource from careplan's contained array based on ID
func getContainedBasicByID(careplan types.CarePlan, id types.String) types.Basic {
	var basic types.Basic

	idString := strings.TrimPrefix(string(id), "#")

	for _, resource := range *careplan.Contained {
		if resource["resourceType"] != "Basic" {
			continue
		}
		var candidate types.Basic
		// convert resource back to byteslice
		byteSlice, err := json.Marshal(resource)
		if err != nil {
			// handle error
		}
		// unmarshall byteslice to Basic
		err = json.Unmarshal(byteSlice, &candidate)
		if err != nil {
			// handle error
		}
		if string(*candidate.ID) != idString {
			continue
		}
		basic = candidate
	}
	return basic
}
