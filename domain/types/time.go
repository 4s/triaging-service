package types

import (
	"errors"
	"time"
)

// Time is a time during the day, in the format hh:mm:ss. There is no date specified.
type Time string

// CompareWithOperator is a function used to compare a time to another time using an operator
func (inputTime *Time) CompareWithOperator(otherInputTime *Time, operator Code) (bool, error) {
	comparison, err := inputTime.CompareTo(otherInputTime)
	if err != nil {
		return false, err
	}
	return new(Comparator).SetComparison(comparison).MatchOperator(operator)
}

// CompareTo compares a time with another time and returns an integer indicating the relationship between this time, and
// the other time.
// if the integer is zero, they are equal
// if the integer is > 0, inputTime is larger than otherInputTime
// if the integer is < 0, inputTime is smaller than otherInputTime
// if the integer is nil, they are not comparable beyond =, != and exists
func (inputTime *Time) CompareTo(otherInputTime *Time) (*int32, error) {
	var comparison int32

	if (inputTime == nil && otherInputTime != nil) || (inputTime != nil && otherInputTime == nil) {
		return nil, nil
	}
	if inputTime == otherInputTime {
		comparison = 0
		return &comparison, nil
	}
	parsedTime, err := inputTime.parse()
	if err != nil {
		return nil, errors.New("InputTime is not in correct format: " + err.Error())
	}
	parsedOtherTime, err := otherInputTime.parse()
	if err != nil {
		return nil, errors.New("OtherInputTime is not in correct format: " + err.Error())
	}

	if parsedTime.After(parsedOtherTime) {
		comparison = 1
	} else if parsedTime.Before(parsedOtherTime) {
		comparison = -1
	} else if parsedTime.Equal(parsedOtherTime) {
		comparison = 0
	}
	return &comparison, nil
}

func (inputTime *Time) parse() (time.Time, error) {
	var err error
	var output time.Time

	if inputTime == nil {
		return time.Time{}, errors.New("Time is nil and cannot be parsed")
	}

	output, err = time.Parse("15:04:05", string(*inputTime))
	if err == nil {
		return output, nil
	}

	return time.Time{}, err
}
