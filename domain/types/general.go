package types

// BackboneElement is the base definition for complex elements defined as part of a resource definition.
type BackboneElement struct {
	*Element
	ModifierExtension *[]Extension `json:"modifierExtension,omitempty"`
}

// Basic is used for handling concepts not yet defined in FHIR, narrative-only resources that don't map to an existing resource,
// and custom resources not appropriate for inclusion in the FHIR specification.
type Basic struct {
	*Resource
	*DomainResource
	Identifier *[]Identifier    `json:"identifier,omitempty"`
	Code       *CodeableConcept `json:"code,omitempty"`
	Subject    *Reference       `json:"subject,omitempty"`
	Created    *Date            `json:"created,omitempty"`
	Author     *Reference       `json:"author,omitempty"`
}

// Bundle is a container for a collection of resources.
type Bundle struct {
	*Resource
	*DomainResource
	Identifier *[]Identifier  `json:"identifier,omitempty"`
	Type       *Code          `json:"type,omitempty"`
	Timestamp  *Instant       `json:"timestamp,omitempty"`
	Total      *UnsignedInt   `json:"total,omitempty"`
	Link       *[]BundleLink  `json:"link,omitempty"`
	Entry      *[]BundleEntry `json:"entry,omitempty"`
	Signature  *interface{}   `json:"signature,omitempty"`
}

// BundleLink is a link related to a Bundle
type BundleLink struct {
	*BackboneElement
	Relation *String `json:"relation,omitempty"`
	URL      *URI    `json:"url,omitempty"`
}

// BundleEntry is an entry in the bundle - will have a resource or information
type BundleEntry struct {
	*BackboneElement
	Link     *[]BundleLink        `json:"link,omitempty"`
	FullURL  *URI                 `json:"fullUrl,omitempty"`
	Resource *CarePlan            `json:"resource,omitempty"`
	Search   *BundleEntrySearch   `json:"search,omitempty"`
	Request  *BundleEntryRequest  `json:"request,omitempty"`
	Response *BundleEntryResponse `json:"response,omitempty"`
}

// BundleEntrySearch is search related information
type BundleEntrySearch struct {
	*BackboneElement
	Mode  *Code    `json:"mode,omitempty"`
	Score *Decimal `json:"score,omitempty"`
}

// BundleEntryRequest is additional execution information (transaction/batch/history)
type BundleEntryRequest struct {
	*BackboneElement
	Method          *Code    `json:"method,omitempty"`
	URL             *URI     `json:"url,omitempty"`
	IfNoneMatch     *String  `json:"ifNoneMatch,omitempty"`
	IfModifiedSince *Instant `json:"ifModifiedSince,omitempty"`
	IfMatch         *String  `json:"ifMatch,omitempty"`
	IfNoneExist     *String  `json:"ifNoneExist,omitempty"`
}

// BundleEntryResponse is results of execution (transaction/batch/history)
type BundleEntryResponse struct {
	*BackboneElement
	Status       *String   `json:"status,omitempty"`
	Location     *URI      `json:"location,omitempty"`
	Etag         *String   `json:"etag,omitempty"`
	LastModified *Instant  `json:"lastModified,omitempty"`
	Outcome      *Resource `json:"outcome,omitempty"`
}

// CarePlan describes the intention of how one or more practitioners intend to deliver care for a particular patient,
// group or community for a period of time, possibly limited to care for a specific condition or set of conditions.
type CarePlan struct {
	*Resource
	*DomainResource
	Identifier            *[]Identifier       `json:"identifier,omitempty"`
	InstantiatesCanonical *[]Canonical        `json:"instantiatesCanonical,omitempty"`
	InstantiatesURI       *[]URI              `json:"instantiatesUri,omitempty"`
	BasedOn               *[]Reference        `json:"basedOn,omitempty"`
	Replaces              *[]Reference        `json:"replaces,omitempty"`
	PartOf                *[]Reference        `json:"partOf,omitempty"`
	Status                *Code               `json:"status,omitempty"`
	Intent                *Code               `json:"intent,omitempty"`
	Category              *[]CodeableConcept  `json:"category,omitempty"`
	Title                 *String             `json:"title,omitempty"`
	Description           *String             `json:"description,omitempty"`
	Subject               *Reference          `json:"subject,omitempty"`
	Encounter             *Reference          `json:"encounter,omitempty"`
	Period                *Period             `json:"period,omitempty"`
	Created               *DateTime           `json:"created,omitempty"`
	Author                *Reference          `json:"author,omitempty"`
	Contributor           *Reference          `json:"contributor,omitempty"`
	CareTeam              *[]Reference        `json:"careTeam,omitempty"`
	Addresses             *[]Reference        `json:"addresses,omitempty"`
	SupportingInfo        *[]Reference        `json:"supportingInfo,omitempty"`
	Goal                  *[]Reference        `json:"goal,omitempty"`
	Activity              *[]CarePlanActivity `json:"activity,omitempty"`
	// Note                  *[]Annotation - not supported by this service
}

// CarePlanActivity is an action that occurs as part of CarePlan
type CarePlanActivity struct {
	*BackboneElement
	OutcomeCodeableConcept *[]CodeableConcept      `json:"outcomeCodeableConcept,omitempty"`
	OutcomeReference       *[]Reference            `json:"outcomeReference,omitempty"`
	Reference              *Reference              `json:"reference,omitempty"`
	Detail                 *CarePlanActivityDetail `json:"detail,omitempty"`
	// Progress *[]Annotation - not supported by this service
}

// CarePlanActivityDetail is an in-line definition of a CarePlanActivity
type CarePlanActivityDetail struct {
	*BackboneElement
	Kind                   *Code              `json:"kind,omitempty"`
	InstantiatesCanonical  *[]Canonical       `json:"instantiatesCanonical,omitempty"`
	InstantiatesURI        *[]URI             `json:"instantiatesUri,omitempty"`
	Code                   *Code              `json:"code,omitempty"`
	ReasonCode             *[]CodeableConcept `json:"reasonCode,omitempty"`
	ReasonReference        *[]Reference       `json:"reasonReference,omitempty"`
	Goal                   *[]Reference       `json:"goal,omitempty"`
	Status                 *String            `json:"status,omitempty"`
	StatusReason           *CodeableConcept   `json:"statusReason,omitempty"`
	DoNotPerform           *Boolean           `json:"doNotPerform,omitempty"`
	SceduledTiming         *Timing            `json:"sceduledTiming,omitempty"`
	ScheduledPeriod        *Period            `json:"scheduledPeriod,omitempty"`
	ScheduledString        *String            `json:"scheduledString,omitempty"`
	Location               *Reference         `json:"location,omitempty"`
	Performer              *[]Reference       `json:"performer,omitempty"`
	ProductCodeableConcept *CodeableConcept   `json:"productCodeableConcept,omitempty"`
	ProductReference       *Reference         `json:"productReference,omitempty"`
	DailyAmount            *SimpleQuantity    `json:"dailyAmount,omitempty"`
	Quantity               *SimpleQuantity    `json:"quantity,omitempty"`
	Description            *String            `json:"description,omitempty"`
}

// DomainResource is an abstract base resource that builds upon "Resource" and is extended by other resources
type DomainResource struct {
	Text              *Narrative                `json:"text,omitempty"`
	Contained         *[]map[string]interface{} `json:"contained,omitempty"`
	Extension         *[]Extension              `json:"extension,omitempty"`
	ModifierExtension *[]Extension              `json:"modifierExtension,omitempty"`
}

// Duration is a quantity that specifies a duration. Duration must have a code if there is a value, and it must be an
// expression of time
type Duration Quantity

// Element is the base definition for all elements contained inside a resource.
type Element struct {
	ID        *ID          `json:"id,omitempty"`
	Extension *[]Extension `json:"extension,omitempty"`
}

// Extension represents additional information that is not part of the basic definition of the resource.
type Extension struct {
	*Element
	URL                  *URI             `json:"url,omitempty"`
	ValueBoolean         *Boolean         `json:"valueBoolean,omitempty"`
	ValueCanonical       *Canonical       `json:"valueCanonical,omitempty"`
	ValueCode            *Code            `json:"valueCode,omitempty"`
	ValueDate            *Date            `json:"valueDate,omitempty"`
	ValueDateTime        *DateTime        `json:"valueDateTime,omitempty"`
	ValueDecimal         *Decimal         `json:"valueDecimal,omitempty"`
	ValueID              *ID              `json:"valueId,omitempty"`
	ValueInstant         *Instant         `json:"valueInstant,omitempty"`
	ValueInteger         *Integer         `json:"valueInteger,omitempty"`
	ValuePositiveInt     *PositiveInt     `json:"valuePositiveInt,omitempty"`
	ValueString          *String          `json:"valueString,omitempty"`
	ValueTime            *Time            `json:"valueTime,omitempty"`
	ValueUnsignedInt     *UnsignedInt     `json:"valueUnsignedInt,omitempty"`
	ValueURI             *URI             `json:"valueUri,omitempty"`
	ValueCodeableConcept *CodeableConcept `json:"valueCodeableConcept,omitempty"`
	ValueCoding          *Coding          `json:"valueCoding,omitempty"`
	ValueDuration        *Duration        `json:"valueDuration,omitempty"`
	ValueIdentifier      *Identifier      `json:"valueIdentifier,omitempty"`
	ValuePeriod          *Period          `json:"valuePeriod,omitempty"`
	ValueQuantity        *Quantity        `json:"valueQuantity,omitempty"`
	ValueRange           *Range           `json:"valueRange,omitempty"`
	ValueReference       *Reference       `json:"valueReference,omitempty"`
	ValueTiming          *Timing          `json:"valueTiming,omitempty"`
	// ValueBase64Binary        base64Binary - not supported by this service
	// ValueMarkdown            markdown - not supported by this service
	// ValueOid                 oid - not supported by this service
	// ValueUrl                 url - not supported by this service
	// ValueUuid                uuid - not supported by this service
	// ValueAddress             Address - not supported by this service
	// ValueAge                 Age - not supported by this service
	// ValueAnnotation          Annotation - not supported by this service
	// ValueAttachment          Attachment - not supported by this service
	// ValueContactPoint        ContactPoint - not supported by this service
	// ValueCount               Count - not supported by this service
	// ValueDistance            Distance - not supported by this service
	// ValueHumanName           HumanName - not supported by this service
	// ValueMoney               Money - not supported by this service
	// ValueRatio               Ratio - not supported by this service
	// ValueSampledData         SampledData - not supported by this service
	// ValueSignature           Signature - not supported by this service
	// ValueParameterDefinition ParameterDefinition - not supported by this service
	// ValueDataRequirement     DataRequirement - not supported by this service
	// ValueRelatedArtifact     RelatedArtifact - not supported by this service
	// ValueContactDetail       ContactDetail - not supported by this service
	// ValueContributor         Contributor - not supported by this service
	// ValueTriggerDefinition   TriggerDefinition - not supported by this service
	// ValueExpression          Expression - not supported by this service
	// ValueUsageContext        UsageContext - not supported by this service
	// ValueDosage              Dosage - not supported by this service
}

// Meta is a set of metadata that provides technical and workflow context to the resource.
type Meta struct {
	VersionID   *ID          `json:"versionId,omitempty"`
	LastUpdated *Instant     `json:"lastUpdated,omitempty"`
	Source      *URI         `json:"source,omitempty"`
	Profile     *[]Canonical `json:"profile,omitempty"`
	Security    *Coding      `json:"security,omitempty"`
	Tag         *Coding      `json:"tag,omitempty"`
}

// Narrative is a human-readable narrative that contains a summary of the resource.
type Narrative struct {
	Status *Code  `json:"status,omitempty"`
	Div    *XHTML `json:"div,omitempty"`
}

// Resource is the base resource on which all other resources are built
type Resource struct {
	ResourceType  *string `json:"resourceType,omitempty"`
	ID            *ID     `json:"id,omitempty"`
	Meta          *Meta   `json:"meta,omitempty"`
	ImplicitRules *URI    `json:"uri,omitempty"`
	Language      *Code   `json:"language,omitempty"`
}

// SimpleQuantity is a Quantity, in which the comparator is not used
type SimpleQuantity Quantity

// Task is a task to be performed.
type Task struct {
	*Resource
	*DomainResource
	Identifier            *[]Identifier      `json:"identifier,omitempty"`
	InstantiatesCanonical *Canonical         `json:"instantiatesCanonical,omitempty"`
	InstantiatesURI       *URI               `json:"instantiatesUri,omitempty"`
	BasedOn               *[]Reference       `json:"basedOn,omitempty"`
	GroupIdentifier       *Identifier        `json:"groupIdentifier,omitempty"`
	PartOf                *[]Reference       `json:"partOf,omitempty"`
	Status                *Code              `json:"status,omitempty"`
	StatusReason          *CodeableConcept   `json:"statusReason,omitempty"`
	BusinessStatus        *CodeableConcept   `json:"businessStatus,omitempty"`
	Intent                *Code              `json:"intent,omitempty"`
	Priority              *Code              `json:"priority,omitempty"`
	Code                  *CodeableConcept   `json:"code,omitempty"`
	Description           *String            `json:"description,omitempty"`
	Focus                 *Reference         `json:"focus,omitempty"`
	For                   *Reference         `json:"for,omitempty"`
	Encounter             *Reference         `json:"encounter,omitempty"`
	ExecutionPeriod       *Period            `json:"executionPeriod,omitempty"`
	AuthoredOn            *DateTime          `json:"authoredOn,omitempty"`
	LastModified          *DateTime          `json:"lastModified,omitempty"`
	Requester             *Reference         `json:"requester,omitempty"`
	PerformerType         *[]CodeableConcept `json:"performerType,omitempty"`
	Owner                 *Reference         `json:"owner,omitempty"`
	Location              *Reference         `json:"location,omitempty"`
	ReasonCode            *CodeableConcept   `json:"reasonCode,omitempty"`
	ReasonReference       *Reference         `json:"reasonReference,omitempty"`
	Insurance             *[]Reference       `json:"insurance,omitempty"`
	RelevantHistory       *Reference         `json:"relevantHistory,omitempty"`
	Restriction           *TaskRestriction   `json:"restriction,omitempty"`
	Input                 *[]TaskInputOutput `json:"input,omitempty"`
	Output                *[]TaskInputOutput `json:"output,omitempty"`
	// Note *Annotation
}

// TaskRestriction is a constraint on fulfillment of a task
type TaskRestriction struct {
	*BackboneElement
	Repetitions *PositiveInt `json:"repetitions,omitempty"`
	Period      *Period      `json:"period,omitempty"`
	Recipient   *[]Reference `json:"recipient,omitempty"`
}

// TaskInputOutput is a struct for referring to both information used to perform task and
// information produced as part of task
type TaskInputOutput struct {
	*BackboneElement
	Type           *CodeableConcept `json:"type,omitempty"`
	ValueCanonical *Canonical       `json:"valueCanonical,omitempty"`
	ValueReference *Reference       `json:"valueReference,omitempty"`
}

// Timing describes the occurrence of an event that may occur multiple times.
type Timing struct {
	*BackboneElement
	Event  *[]DateTime      `json:"event,omitempty"`
	Repeat *TimingRepeat    `json:"repeat,omitempty"`
	Code   *CodeableConcept `json:"code,omitempty"`
}

// TimingRepeat is a set of rules that describe when the event is scheduled.
type TimingRepeat struct {
	*Element
	BoundsDuration *Duration    `json:"boundsDuration,omitempty"`
	BoundsRange    *Range       `json:"boundsRange,omitempty"`
	BoundsPeriod   *Period      `json:"boundsPeriod,omitempty"`
	Count          *PositiveInt `json:"count,omitempty"`
	CountMax       *PositiveInt `json:"countMax,omitempty"`
	Duration       *Decimal     `json:"duration,omitempty"`
	DurationMax    *Decimal     `json:"durationMax,omitempty"`
	DurationUnit   *Code        `json:"durationUnit,omitempty"`
	Frequency      *PositiveInt `json:"frequency,omitempty"`
	FrequencyMax   *PositiveInt `json:"frequencyMax,omitempty"`
	Period         *Decimal     `json:"period,omitempty"`
	PeriodMax      *Decimal     `json:"periodMax,omitempty"`
	PeriodUnit     *Code        `json:"periodUnit,omitempty"`
	DayOfWeek      *[]Code      `json:"dayOfWeek,omitempty"`
	TimeOfDay      *[]Time      `json:"timeOfDay,omitempty"`
	When           *[]Code      `json:"when,omitempty"`
	Offset         *UnsignedInt `json:"offset,omitempty"`
}
