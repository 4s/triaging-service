package types

// URI is a Uniform Resource Locator (RFC 1738).
type URI string

// CompareWithOperator is a function used to compare a uri to another uri using an operator
func (uri *URI) CompareWithOperator(otherURI *URI, operator Code) (bool, error) {
	return new(Comparator).SetComparison(uri.CompareTo(otherURI)).MatchOperator(operator)
}

// CompareTo compares a URI with another URI and returns an integer indicating the relationship between this URI, and
// the other URI.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (uri *URI) CompareTo(otherURI *URI) *int32 {
	var comparison int32
	if (uri == nil && otherURI != nil) || (uri != nil && otherURI == nil) {
		return &comparison
	}
	if uri == otherURI || *uri == *otherURI {
		comparison = 0
	} else {
		return nil
	}
	return &comparison
}
