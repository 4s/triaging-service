package types

// Identifier is a numeric or alphanumeric string that is associated with a single object or entity within a given
// system.
type Identifier struct {
	Use    *Code            `json:"use,omitempty"`
	Type   *CodeableConcept `json:"type,omitempty"`
	System *URI             `json:"system,omitempty"`
	Value  *String          `json:"value,omitempty"`
	Period *Period          `json:"period,omitempty"`
	// Assigner Reference       `json:"assigner"` - creates illegal cyclic dependency
}

// CompareWithOperator is a function used to compare a identifier to another identifier using an operator
func (identifier *Identifier) CompareWithOperator(otherIdentifier *Identifier, operator Code) (bool, error) {
	comparison, err := identifier.CompareTo(otherIdentifier)
	if err != nil {
		return false, err
	}
	return new(Comparator).SetComparison(comparison).MatchOperator(operator)
}

// CompareTo compares an identifier with another identifier and returns an integer indicating the
// relationship between this identifier, and the other identifier.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (identifier *Identifier) CompareTo(otherIdentifier *Identifier) (*int32, error) {
	var comparison int32
	if (identifier == nil && otherIdentifier != nil) || (identifier != nil && otherIdentifier == nil) {
		return nil, nil
	}
	if identifier == otherIdentifier {
		comparison = 0
		return &comparison, nil
	}

	useComparison := identifier.Use.CompareTo(otherIdentifier.Use)
	typeComparison := identifier.Type.CompareTo(otherIdentifier.Type)
	systemComparison := identifier.System.CompareTo(otherIdentifier.System)
	valueComparison := identifier.Value.CompareTo(otherIdentifier.Value)
	periodComparison, err := identifier.Period.CompareTo(otherIdentifier.Period)
	if err != nil {
		return nil, err
	}

	if useComparison != nil && *useComparison == 0 &&
		typeComparison != nil && *typeComparison == 0 &&
		systemComparison != nil && *systemComparison == 0 &&
		valueComparison != nil && *valueComparison == 0 &&
		periodComparison != nil && *periodComparison == 0 {
		comparison = 0
	} else {
		return nil, nil
	}

	return &comparison, nil
}
