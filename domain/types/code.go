package types

// Code indicates that the value is taken from a set of controlled strings defined elsewhere
type Code string

// CompareWithOperator is a function used to compare a date to another date using an operator
func (code *Code) CompareWithOperator(otherCode *Code, operator Code) (bool, error) {
	return new(Comparator).SetComparison(code.CompareTo(otherCode)).MatchOperator(operator)
}

// CompareTo compares a code with another code and returns an integer indicating the relationship between this code, and
// the other code.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (code *Code) CompareTo(otherCode *Code) *int32 {
	var comparison int32
	if (code == nil && otherCode != nil) || (code != nil && otherCode == nil) {
		return nil
	}
	if code == otherCode || *code == *otherCode {
		comparison = 0
	} else {
		return nil
	}
	return &comparison
}
