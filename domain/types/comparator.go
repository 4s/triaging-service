package types

import (
	"errors"
)

// Comparator receives
type Comparator struct {
	comparison *int32
}

// SetComparison sets the comparison value in the comparator
func (c *Comparator) SetComparison(comparison *int32) *Comparator {
	c.comparison = comparison
	return c
}

// MatchOperator matches the result of the comparison with the operator and returns a boolean
func (c *Comparator) MatchOperator(operator Code) (bool, error) {
	var boolean bool

	if c.comparison == nil {
		if operator == "!=" || operator == "exists" {
			return true, nil
		} else if operator == "=" {
			return false, nil
		} else {
			return false, errors.New("Operator '" + string(operator) + "' not applicable when values are incomparable")
		}
	} else if *c.comparison > 0 {
		if operator == ">" || operator == ">=" || operator == "exists" || operator == "!=" {
			boolean = true
		} else {
			boolean = false
		}
	} else if *c.comparison < 0 {
		if operator == "<" || operator == "<=" || operator == "exists" || operator == "!=" {
			boolean = true
		} else {
			boolean = false
		}
	} else if *c.comparison == 0 {
		if operator == "=" || operator == "exists" || operator == "<=" || operator == ">=" {
			boolean = true
		} else {
			boolean = false
		}
	}

	return boolean, nil
}
