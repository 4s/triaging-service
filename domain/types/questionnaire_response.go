package types

// QuestionnaireResponse is a structured set of questions and their answers
type QuestionnaireResponse struct {
	*Resource
	*DomainResource
	Identifier    *Identifier                  `json:"identifier,omitempty"`
	BasedOn       *[]Reference                 `json:"basedOn,omitempty"`
	PartOf        *[]Reference                 `json:"partOf,omitempty"`
	Questionnaire *Canonical                   `json:"questionnaire,omitempty"`
	Status        *Code                        `json:"status,omitempty"`
	Subject       *Reference                   `json:"subject,omitempty"`
	Context       *Reference                   `json:"context,omitempty"`
	Authored      *String                      `json:"authored,omitempty"`
	Author        *Reference                   `json:"author,omitempty"`
	Source        *Reference                   `json:"source,omitempty"`
	Item          *[]QuestionnaireResponseItem `json:"item,omitempty"`
}

// QuestionnaireResponseItem is a group or question item from the original questionnaire for which answers are provided.
type QuestionnaireResponseItem struct {
	*BackboneElement
	LinkID     *String                      `json:"linkId,omitempty"`
	Definition *String                      `json:"definition,omitempty"`
	Text       *String                      `json:"text,omitempty"`
	Answer     *[]Answer                    `json:"answer,omitempty"`
	Item       *[]QuestionnaireResponseItem `json:"item,omitempty"`
}

// Answer is the respondent's answer(s) to the question.
type Answer struct {
	*BackboneElement
	ValueBoolean   *Boolean                     `json:"valueBoolean,omitempty"`
	ValueDecimal   *Decimal                     `json:"valueDecimal,omitempty"`
	ValueInteger   *Integer                     `json:"valueInteger,omitempty"`
	ValueDate      *Date                        `json:"valueDate,omitempty"`
	ValueDateTime  *DateTime                    `json:"valueDateTime,omitempty"`
	ValueTime      *Time                        `json:"valueTime,omitempty"`
	ValueString    *String                      `json:"valueString,omitempty"`
	ValueURI       *URI                         `json:"valueURI,omitempty"`
	ValueCoding    *Coding                      `json:"valueCoding,omitempty"`
	ValueQuantity  *Quantity                    `json:"valueQuantity,omitempty"`
	ValueReference *Reference                   `json:"valueReference,omitempty"`
	Item           *[]QuestionnaireResponseItem `json:"item,omitempty"`
	// ValueAttachment Attachment      `json:"valueAttachment,omitempty"` - not supported by this service
}

// GetValue returns the first value of a 'value'-field it encounters
func (answer *Answer) GetValue() interface{} {

	if answer.ValueBoolean != nil {
		return answer.ValueBoolean
	} else if answer.ValueDecimal != nil {
		return answer.ValueDecimal
	} else if answer.ValueInteger != nil {
		return answer.ValueInteger
	} else if answer.ValueDate != nil {
		return answer.ValueDate
	} else if answer.ValueDateTime != nil {
		return answer.ValueDateTime
	} else if answer.ValueTime != nil {
		return answer.ValueTime
	} else if answer.ValueString != nil {
		return answer.ValueString
	} else if answer.ValueURI != nil {
		return answer.ValueURI
	} else if answer.ValueCoding != nil {
		return answer.ValueCoding
	} else if answer.ValueQuantity != nil {
		return answer.ValueQuantity
	} else if answer.ValueReference != nil {
		return answer.ValueReference
	}

	return nil
}
