package types

import (
	"errors"
	"time"
)

// Date is a date, or partial date (e.g. just year or year + month) as used in human communication.
// The format is YYYY, YYYY-MM, or YYYY-MM-DD
type Date string

// CompareWithOperator is a function used to compare a date to another date using an operator
func (date *Date) CompareWithOperator(otherDate *Date, operator Code) (bool, error) {
	comparison, err := date.CompareTo(otherDate)
	if err != nil {
		return false, err
	}
	return new(Comparator).SetComparison(comparison).MatchOperator(operator)
}

// CompareTo compares a date with another date and returns an integer indicating the relationship between this date, and
// the other date.
// if the integer is zero, they are equal
// if the integer is > 0, date is larger than otherDate
// if the integer is < 0, date is smaller than otherDate
// if the integer is nil, they are not comparable beyond =, != and exists
func (date *Date) CompareTo(otherDate *Date) (*int32, error) {
	var comparison int32

	if (date == nil && otherDate != nil) || (date != nil && otherDate == nil) {
		return nil, nil
	}
	if date == otherDate {
		comparison = 0
		return &comparison, nil
	}
	parsedDate, err := date.parse()
	if err != nil {
		return nil, errors.New("Date is not in correct format: " + err.Error())
	}
	parsedOtherDate, err := otherDate.parse()
	if err != nil {
		return nil, errors.New("OtherDate is not in correct format: " + err.Error())
	}

	if parsedDate.After(parsedOtherDate) {
		comparison = 1
	} else if parsedDate.Before(parsedOtherDate) {
		comparison = -1
	} else if parsedDate.Equal(parsedOtherDate) {
		comparison = 0
	}
	return &comparison, nil
}

func (date *Date) parse() (time.Time, error) {
	var err error
	var output time.Time

	if date == nil {
		return time.Time{}, errors.New("Date is nil and cannot be parsed")
	}

	output, err = time.Parse("2006-01-02", string(*date))
	if err == nil {
		return output, nil
	}
	output, err = time.Parse("2006-01", string(*date))
	if err == nil {
		return output, nil
	}
	output, err = time.Parse("2006", string(*date))
	if err == nil {
		return output, nil
	}

	return time.Time{}, err
}
