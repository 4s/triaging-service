package types

// ComparableType is an interface for types that can be compared to another instance of itself
type ComparableType interface {
	CompareWithOperator(otherValue ComparableType, operator Code) bool
}
