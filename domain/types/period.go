package types

import (
	"errors"
)

// Period is a time period defined by a start and end date/time.
type Period struct {
	Start *DateTime `json:"start,omitempty"`
	End   *DateTime `json:"end,omitempty"`
}

// CompareWithOperator is a function used to compare a period to another period using an operator
func (period *Period) CompareWithOperator(otherPeriod *Period, operator Code) (bool, error) {
	if operator != "exists" && operator != "=" && operator != "!=" {
		return false, errors.New("Operator not applicable to type")
	}
	comparison, err := period.CompareTo(otherPeriod)
	if err != nil {
		return false, err
	}
	return new(Comparator).SetComparison(comparison).MatchOperator(operator)
}

// CompareTo compares a period with another period and returns an integer indicating the
// relationship between this period, and the other period.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (period *Period) CompareTo(otherPeriod *Period) (*int32, error) {
	var comparison int32
	if (period == nil && otherPeriod != nil) || (period != nil && otherPeriod == nil) {
		return nil, nil
	}
	if period == otherPeriod {
		comparison = 0
		return &comparison, nil
	}

	startComparison, err := period.Start.CompareTo(otherPeriod.Start)
	if err != nil {
		return nil, err
	}
	endComparison, err := period.End.CompareTo(otherPeriod.End)
	if err != nil {
		return nil, err
	}

	if startComparison != nil && *startComparison == 0 && endComparison != nil && *endComparison == 0 {
		comparison = 0
	} else {
		return nil, nil
	}

	return &comparison, nil
}
