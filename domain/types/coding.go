package types

// Coding is a representation of a defined concept using a symbol from a defined "code system"
type Coding struct {
	System       *URI     `json:"system,omitempty"`
	Version      *String  `json:"version,omitempty"`
	Code         *Code    `json:"code,omitempty"`
	Display      *String  `json:"display,omitempty"`
	UserSelected *Boolean `json:"userSelected,omitempty"`
}

// CompareWithOperator is a function used to compare a coding to another coding using an operator
func (coding *Coding) CompareWithOperator(otherCoding *Coding, operator Code) (bool, error) {
	return new(Comparator).SetComparison(coding.CompareTo(otherCoding)).MatchOperator(operator)
}

// CompareTo compares a coding with another coding and returns an integer indicating the relationship between this coding, and
// the other coding.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (coding *Coding) CompareTo(otherCoding *Coding) *int32 {
	var comparison int32
	if (coding == nil && otherCoding != nil) || (coding != nil && otherCoding == nil) {
		return nil
	}
	if coding == otherCoding {
		comparison = 0
		return &comparison
	}
	systemComparison := coding.System.CompareTo(otherCoding.System)
	versionComparison := coding.Version.CompareTo(otherCoding.Version)
	codeComparison := coding.Code.CompareTo(otherCoding.Code)
	displayComparison := coding.Display.CompareTo(otherCoding.Display)
	userSelectedComparison := coding.UserSelected.CompareTo(otherCoding.UserSelected)

	if systemComparison != nil && *systemComparison == 0 &&
		versionComparison != nil && *versionComparison == 0 &&
		codeComparison != nil && *codeComparison == 0 &&
		displayComparison != nil && *displayComparison == 0 &&
		userSelectedComparison != nil && *userSelectedComparison == 0 {
		comparison = 0
	} else {
		return nil
	}
	return &comparison
}

// Codings is an array of Coding
type Codings []Coding

// CompareWithOperator is a function used to compare a coding array to another coding array using an operator
func (codingArray *Codings) CompareWithOperator(otherCodingArray *Codings, operator Code) (bool, error) {
	return new(Comparator).SetComparison(codingArray.CompareTo(otherCodingArray)).MatchOperator(operator)
}

// CompareTo compares an array of codomg with another array of coding and returns an integer indicating the
// relationship between this array of coding, and the other array of coding.
// if the integer is zero, the two arrays of coding are equal. If the integer is different from zero, they are not.
func (codingArray *Codings) CompareTo(otherCodingArray *Codings) *int32 {
	var comparison int32
	if (codingArray == nil && otherCodingArray != nil) || (codingArray != nil && otherCodingArray == nil) {
		return &comparison
	}
	codingComparison := true
	if codingArray != otherCodingArray {
		if len(*codingArray) != len(*otherCodingArray) {
			codingComparison = false
		}
		for _, coding := range *codingArray {
			for _, otherCoding := range *otherCodingArray {
				equals, _ := coding.CompareWithOperator(&otherCoding, "=")
				if !equals {
					codingComparison = false
				}
			}
		}
	}
	if codingComparison {
		comparison = 0
	} else {
		return nil
	}
	return &comparison
}
