package types

// CodeableConcept represents a value that is usually supplied by providing a reference to one or more terminologies or
// ontologies but may also be defined by the provision of text.
type CodeableConcept struct {
	Coding *Codings `json:"coding,omitempty"`
	Text   *String  `json:"text,omitempty"`
}

// CompareWithOperator is a function used to compare a reference to another reference using an operator
func (codeableConcept *CodeableConcept) CompareWithOperator(otherCodeableConcept *CodeableConcept, operator Code) (bool, error) {
	return new(Comparator).SetComparison(codeableConcept.CompareTo(otherCodeableConcept)).MatchOperator(operator)
}

// CompareTo compares a codeable concept with another codeable concept and returns an integer indicating the
// relationship between this codeable concept, and the other codeable concept.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (codeableConcept *CodeableConcept) CompareTo(otherCodeableConcept *CodeableConcept) *int32 {
	var comparison int32
	if (codeableConcept == nil && otherCodeableConcept != nil) || (codeableConcept != nil && otherCodeableConcept == nil) {
		return nil
	}
	if codeableConcept == otherCodeableConcept {
		comparison = 0
		return &comparison
	}

	codingComparison := codeableConcept.Coding.CompareTo(otherCodeableConcept.Coding)
	textComparison := codeableConcept.Text.CompareTo(otherCodeableConcept.Text)

	if codingComparison != nil && *codingComparison == 0 && textComparison != nil && *textComparison == 0 {
		comparison = 0
	} else {
		return nil
	}

	return &comparison
}
