package types

// Decimal is a rational number that has a decimal representation.
type Decimal float64

// CompareWithOperator is a function used to compare a decimal to another decimal using an operator
func (decimal *Decimal) CompareWithOperator(otherDecimal *Decimal, operator Code) (bool, error) {
	return new(Comparator).SetComparison(decimal.CompareTo(otherDecimal)).MatchOperator(operator)
}

// CompareTo compares a decimal with another decimal and returns an integer indicating the relationship between this decimal, and
// the other decimal.
// if the integer is zero, they are equal
// if the integer is > 0, decimal is larger than otherDecimal
// if the integer is < 0, decimal is smaller than otherDecimal
// if the integer is nil, they are not comparable beyond =, != and exists
func (decimal *Decimal) CompareTo(otherDecimal *Decimal) *int32 {
	var comparison int32
	if (decimal == nil && otherDecimal != nil) || (decimal != nil && otherDecimal == nil) {
		return nil
	}
	if decimal == otherDecimal || *decimal == *otherDecimal {
		comparison = 0
	} else if *decimal < *otherDecimal {
		comparison = -1
	} else if *decimal > *otherDecimal {
		comparison = 1
	}
	return &comparison
}
