package types

// Boolean is a value that can either be true or false
type Boolean bool

// CompareWithOperator is a function used to compare an boolean to another boolean using an operator
func (boolean *Boolean) CompareWithOperator(otherBoolean *Boolean, operator Code) (bool, error) {
	comparison := boolean.CompareTo(otherBoolean)
	return new(Comparator).SetComparison(comparison).MatchOperator(operator)
}

// CompareTo compares a boolean with another boolean and returns an integer indicating the relationship between this boolean, and
// the other boolean.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (boolean *Boolean) CompareTo(otherBoolean *Boolean) *int32 {
	var comparison int32
	if (boolean == nil && otherBoolean != nil) || (boolean != nil && otherBoolean == nil) {
		return nil
	}
	if boolean == otherBoolean || *boolean == *otherBoolean {
		comparison = 0
	} else {
		return nil
	}

	return &comparison
}
