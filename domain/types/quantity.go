package types

import "errors"

// Quantity is a measured amount (or an amount that can potentially be measured).
type Quantity struct {
	*Element
	Value      *Decimal `json:"value,omitempty"`
	Comparator *Code    `json:"comparator,omitempty"`
	Unit       *String  `json:"unit,omitempty"`
	System     *URI     `json:"system,omitempty"`
	Code       *Code    `json:"code,omitempty"`
}

// CompareWithOperator is a function used to compare a quantity to another quantity using an operator
func (quantity *Quantity) CompareWithOperator(otherQuantity *Quantity, operator Code) (bool, error) {
	comparison, err := quantity.CompareTo(otherQuantity)
	if err != nil {
		return false, err
	}
	return new(Comparator).SetComparison(comparison).MatchOperator(operator)
}

// CompareTo compares a quantity with another quantity and returns an integer indicating the
// relationship between this quantity, and the other quantity.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (quantity *Quantity) CompareTo(otherQuantity *Quantity) (*int32, error) {
	var comparison int32
	if (quantity == nil && otherQuantity != nil) || (quantity != nil && otherQuantity == nil) {
		return nil, nil
	}
	if quantity == otherQuantity {
		comparison = 0
		return &comparison, nil
	}
	if quantity.System == nil || quantity.Code == nil || otherQuantity.System == nil || otherQuantity.Code == nil {
		return nil, errors.New("missing unit-system and unit-code. Unable to make comparison")
	}
	if (quantity == nil && otherQuantity != nil) || (quantity != nil && otherQuantity == nil) {
		return nil, nil
	}
	if quantity == otherQuantity {
		comparison = 0
		return &comparison, nil
	}

	valueComparison := quantity.Value.CompareTo(otherQuantity.Value)
	systemComparison := quantity.System.CompareTo(otherQuantity.System)
	codeComparison := quantity.Code.CompareTo(otherQuantity.Code)

	if systemComparison != nil && *systemComparison == 0 && codeComparison != nil && *codeComparison == 0 {
		comparison = *valueComparison
	} else {
		return nil, nil
	}

	return &comparison, nil
}
