package types

// Range is a set of ordered Quantity values defined by a low and high limit.
type Range struct {
	Low  *Quantity `json:"low,omitempty"`
	High *Quantity `json:"high,omitempty"`
}

// CompareWithOperator is a function used to compare a reference to another reference using an operator
func (rng *Range) CompareWithOperator(otherRng *Range, operator Code) (bool, error) {
	comparison, err := rng.CompareTo(otherRng)
	if err != nil {
		return false, err
	}
	return new(Comparator).SetComparison(comparison).MatchOperator(operator)
}

// CompareTo compares a range with another range and returns an integer indicating the
// relationship between this range, and the other range.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (rng *Range) CompareTo(otherRng *Range) (*int32, error) {
	var comparison int32
	if (rng == nil && otherRng != nil) || (rng != nil && otherRng == nil) {
		return nil, nil
	}
	if rng == otherRng {
		comparison = 0
		return &comparison, nil
	}

	lowComparison, err := rng.Low.CompareWithOperator(otherRng.Low, "=")
	if err != nil {
		return nil, err
	}
	highComparison, err := rng.High.CompareWithOperator(otherRng.High, "=")
	if err != nil {
		return nil, err
	}

	if lowComparison && highComparison {
		comparison = 0
	} else {
		return nil, nil
	}

	return &comparison, nil
}
