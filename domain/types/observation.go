package types

// Observation is a measurement or simple assertion made about a patient, device or other subject
type Observation struct {
	*Resource
	*DomainResource
	Identifier           *[]Identifier           `json:"identifier,omitempty"`
	BasedOn              *[]Reference            `json:"basedOn,omitempty"`
	PartOf               *[]Reference            `json:"partOf,omitempty"`
	Status               *Code                   `json:"status,omitempty"`
	Category             *[]CodeableConcept      `json:"category,omitempty"`
	Code                 *CodeableConcept        `json:"code,omitempty"`
	Subject              *Reference              `json:"subject,omitempty"`
	Focus                *[]Reference            `json:"focus,omitempty"`
	Encounter            *Reference              `json:"encounter,omitempty"`
	EffectiveDateTime    *DateTime               `json:"effectiveDateTime,omitempty"`
	EffectivePeriod      *Period                 `json:"effectivePeriod,omitempty"`
	EffectiveTiming      *Timing                 `json:"effectiveTiming,omitempty"`
	EffectiveInstant     *Instant                `json:"effectiveInstant,omitempty"`
	Issued               *Instant                `json:"issued,omitempty"`
	Performer            *[]Reference            `json:"performer,omitempty"`
	ValueQuantity        *Quantity               `json:"valueQuantity,omitempty"`
	ValueCodeableConcept *CodeableConcept        `json:"valueCodeableConcept,omitempty"`
	ValueString          *String                 `json:"valueString,omitempty"`
	ValueBoolean         *Boolean                `json:"valueBoolean,omitempty"`
	ValueInteger         *Integer                `json:"valueInteger,omitempty"`
	ValueRange           *Range                  `json:"valueRange,omitempty"`
	ValueTime            *Time                   `json:"valueTime,omitempty"`
	ValueDateTime        *DateTime               `json:"valueDateTime,omitempty"`
	ValuePeriod          *Period                 `json:"valuePeriod,omitempty"`
	DataAbsentReason     *CodeableConcept        `json:"dataAbsentReason,omitempty"`
	Interpretation       *[]CodeableConcept      `json:"interpretation,omitempty"`
	Comment              *String                 `json:"comment,omitempty"`
	BodySite             *CodeableConcept        `json:"bodySite,omitempty"`
	Method               *CodeableConcept        `json:"method,omitempty"`
	Specimen             *Reference              `json:"specimen,omitempty"`
	Device               *Reference              `json:"device,omitempty"`
	HasMember            *[]Reference            `json:"hasMember,omitempty"`
	DerivedFrom          *[]Reference            `json:"derivedFrom,omitempty"`
	Component            *[]ObservationComponent `json:"component,omitempty"`
	// ValueRatio           Ratio           `json:"valueRatio,omitempty"` - not supported by this service
	// ValueSampledData     SampledData     `json:"valueSampledData,omitempty"` - not supported by this service
	// ReferenceRange       []ObservationReferenceRange         `json:"referenceRange"` - not supported by this service
}

// ObservationComponent is a component of an observation. Some observations have multiple component observations.
// These component observations are expressed as separate code value pairs that share the same attributes.
type ObservationComponent struct {
	Code                 *CodeableConcept   `json:"code,omitempty"`
	ValueQuantity        *Quantity          `json:"valueQuantity,omitempty"`
	ValueCodeableConcept *CodeableConcept   `json:"valueCodeableConcept,omitempty"`
	ValueString          *String            `json:"valueString,omitempty"`
	ValueBoolean         *Boolean           `json:"valueBoolean,omitempty"`
	ValueInteger         *Integer           `json:"valueInteger,omitempty"`
	ValueRange           *Range             `json:"valueRange,omitempty"`
	ValueTime            *Time              `json:"valueTime,omitempty"`
	ValueDateTime        *DateTime          `json:"valueDateTime,omitempty"`
	ValuePeriod          *Period            `json:"valuePeriod,omitempty"`
	DataAbsentReason     *[]CodeableConcept `json:"dataAbsentReason,omitempty"`
	Interpretation       *[]CodeableConcept `json:"interpretation,omitempty"`
	// ReferenceRange       Element         `json:"referenceRange"` - not supported by this service
}

// ContainsSingleValue returns true if effectiveWhen contains a single value and false if it contains 0 or more than one
func (observation *Observation) ContainsSingleValue() bool {
	noOfValues := 0

	if observation.ValueQuantity != nil {
		noOfValues++
	}
	if observation.ValueCodeableConcept != nil {
		noOfValues++
	}
	if observation.ValueString != nil {
		noOfValues++
	}
	if observation.ValueBoolean != nil {
		noOfValues++
	}
	if observation.ValueInteger != nil {
		noOfValues++
	}
	if observation.ValueRange != nil {
		noOfValues++
	}
	if observation.ValueTime != nil {
		noOfValues++
	}
	if observation.ValueDateTime != nil {
		noOfValues++
	}
	if observation.ValuePeriod != nil {
		noOfValues++
	}

	if noOfValues == 1 {
		return true
	}

	return false
}

// GetValue returns the first value of a 'value'-field it encounters
func (observation *Observation) GetValue() interface{} {

	if observation.ValueQuantity != nil {
		return observation.ValueQuantity
	} else if observation.ValueCodeableConcept != nil {
		return observation.ValueCodeableConcept
	} else if observation.ValueString != nil {
		return observation.ValueString
	} else if observation.ValueBoolean != nil {
		return observation.ValueBoolean
	} else if observation.ValueInteger != nil {
		return observation.ValueInteger
	} else if observation.ValueRange != nil {
		return observation.ValueRange
	} else if observation.ValueTime != nil {
		return observation.ValueTime
	} else if observation.ValueDateTime != nil {
		return observation.ValueDateTime
	} else if observation.ValuePeriod != nil {
		return observation.ValuePeriod
	}

	return nil
}

// ContainsSingleValue returns true if effectiveWhen contains a single value and false if it contains 0 or more than one
func (component *ObservationComponent) ContainsSingleValue() bool {
	noOfValues := 0

	if component.ValueQuantity != nil {
		noOfValues++
	}
	if component.ValueCodeableConcept != nil {
		noOfValues++
	}
	if component.ValueString != nil {
		noOfValues++
	}
	if component.ValueBoolean != nil {
		noOfValues++
	}
	if component.ValueInteger != nil {
		noOfValues++
	}
	if component.ValueRange != nil {
		noOfValues++
	}
	if component.ValueTime != nil {
		noOfValues++
	}
	if component.ValueDateTime != nil {
		noOfValues++
	}
	if component.ValuePeriod != nil {
		noOfValues++
	}

	if noOfValues == 1 {
		return true
	}

	return false
}

// GetValue returns the first value of a 'value'-field it encounters
func (component *ObservationComponent) GetValue() interface{} {

	if component.ValueQuantity != nil {
		return component.ValueQuantity
	} else if component.ValueCodeableConcept != nil {
		return component.ValueCodeableConcept
	} else if component.ValueString != nil {
		return component.ValueString
	} else if component.ValueBoolean != nil {
		return component.ValueBoolean
	} else if component.ValueInteger != nil {
		return component.ValueInteger
	} else if component.ValueRange != nil {
		return component.ValueRange
	} else if component.ValueTime != nil {
		return component.ValueTime
	} else if component.ValueDateTime != nil {
		return component.ValueDateTime
	} else if component.ValuePeriod != nil {
		return component.ValuePeriod
	}

	return nil
}
