package types

// Canonical is a URI that refers to a canonical URI
type Canonical string

// ID is any combination of upper or lower case ASCII letters ('A'..'Z', and 'a'..'z', numerals ('0'..'9'), '-' and '.',
// with a length limit of 64 characters.
type ID string

// Instant is an instant in time in the format YYYY-MM-DDThh:mm:ss.sss+zz:zz
type Instant string

// PositiveInt is any positive integer in the range 1..2,147,483,647.
type PositiveInt uint

// UnsignedInt is any non-negative integer in the range 0..2,147,483,647.
type UnsignedInt uint

// XHTML is a type used for XHTML content
type XHTML string
