package types

import (
	"errors"
	"time"
)

// DateTime is a date, date-time or partial dateTime (e.g. just year or year + month) as used in human communication.
// The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz
type DateTime string

// CompareWithOperator is a function used to compare a dateTime to another dateTime using an operator
func (dateTime *DateTime) CompareWithOperator(otherDateTime *DateTime, operator Code) (bool, error) {
	comparison, err := dateTime.CompareTo(otherDateTime)
	if err != nil {
		return false, err
	}
	return new(Comparator).SetComparison(comparison).MatchOperator(operator)
}

// CompareTo compares a dateTime with another dateTime and returns an integer indicating the relationship between this date, and
// the other date.
// if the integer is zero, they are equal
// if the integer is > 0, dateTime is larger than otherDateTime
// if the integer is < 0, dateTime is smaller than otherDateTime
// if the integer is nil, they are not comparable beyond =, != and exists
func (dateTime *DateTime) CompareTo(otherDateTime *DateTime) (*int32, error) {
	var comparison int32
	if (dateTime == nil && otherDateTime != nil) || (dateTime != nil && otherDateTime == nil) {
		return nil, nil
	}
	if dateTime == otherDateTime {
		comparison = 0
		return &comparison, nil
	}
	parsedDateTime, err := dateTime.parse()
	if err != nil {
		return nil, errors.New("DateTime is not in correct format: " + err.Error())
	}
	parsedOtherDateTime, err := otherDateTime.parse()
	if err != nil {
		return nil, errors.New("OtherDateTime is not in correct format: " + err.Error())
	}

	if parsedDateTime.After(parsedOtherDateTime) {
		comparison = 1
	} else if parsedDateTime.Before(parsedOtherDateTime) {
		comparison = -1
	} else if parsedDateTime.Equal(parsedOtherDateTime) {
		comparison = 0
	}
	return &comparison, nil
}

func (dateTime *DateTime) parse() (time.Time, error) {
	var err error
	var output time.Time

	if dateTime == nil {
		return time.Time{}, errors.New("DateTime is nil and cannot be parsed")
	}

	output, err = time.Parse(time.RFC3339, string(*dateTime))
	if err == nil {
		return output, nil
	}
	output, err = time.Parse("2006-01-02", string(*dateTime))
	if err == nil {
		return output, nil
	}
	output, err = time.Parse("2006-01", string(*dateTime))
	if err == nil {
		return output, nil
	}
	output, err = time.Parse("2006", string(*dateTime))
	if err == nil {
		return output, nil
	}

	return time.Time{}, err
}
