package types

// Integer is A signed integer in the range −2,147,483,648..2,147,483,647
type Integer uint32

// CompareWithOperator is a function used to compare an integer to another integer using an operator
func (integer *Integer) CompareWithOperator(otherInteger *Integer, operator Code) (bool, error) {
	return new(Comparator).SetComparison(integer.CompareTo(otherInteger)).MatchOperator(operator)
}

// CompareTo compares a integer with another integer and returns an integer indicating the relationship between this integer, and
// the other integer.
// if the integer is zero, they are equal
// if the integer is > 0, integer is larger than otherInteger
// if the integer is < 0, integer is smaller than otherInteger
// if the integer is nil, they are not comparable beyond =, != and exists
func (integer *Integer) CompareTo(otherInteger *Integer) *int32 {
	var comparison int32
	if (integer == nil && otherInteger != nil) || (integer != nil && otherInteger == nil) {
		return nil
	}
	if integer == otherInteger || *integer == *otherInteger {
		comparison = 0
	} else if *integer < *otherInteger {
		comparison = -1
	} else if *integer > *otherInteger {
		comparison = 1
	}
	return &comparison
}
