package types

// Reference is a reference to another resource
type Reference struct {
	Reference  *String     `json:"reference,omitempty"`
	Type       *String     `json:"type,omitempty"`
	Identifier *Identifier `json:"identifier,omitempty"`
	Display    *String     `json:"display,omitempty"`
}

// CompareWithOperator is a function used to compare a reference to another reference using an operator
func (reference *Reference) CompareWithOperator(otherReference *Reference, operator Code) (bool, error) {
	comparison, err := reference.CompareTo(otherReference)
	if err != nil {
		return false, err
	}
	return new(Comparator).SetComparison(comparison).MatchOperator(operator)
}

// CompareTo compares a reference with another reference and returns an integer indicating the
// relationship between this reference, and the other reference.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (reference *Reference) CompareTo(otherReference *Reference) (*int32, error) {
	var comparison int32
	if (reference == nil && reference != nil) || (reference != nil && reference == nil) {
		return nil, nil
	}
	if reference == otherReference {
		comparison = 0
		return &comparison, nil
	}
	referenceComparison := reference.Reference.CompareTo(otherReference.Reference)
	typeComparison := reference.Type.CompareTo(otherReference.Type)
	displayComparison := reference.Display.CompareTo(otherReference.Display)
	identifierComparison, err := reference.Identifier.CompareTo(otherReference.Identifier)
	if err != nil {
		return nil, err
	}

	if referenceComparison != nil && *referenceComparison == 0 &&
		typeComparison != nil && *typeComparison == 0 &&
		displayComparison != nil && *displayComparison == 0 &&
		identifierComparison != nil && *identifierComparison == 0 {
		comparison = 0
	} else {
		return nil, nil
	}

	return &comparison, nil
}
