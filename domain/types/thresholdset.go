package types

// ThresholdSet consists of an unordered collection of objects describing how to interpret Observation- and
// QuestionnaireResponse resources in relation to their immediate urgency levels
type ThresholdSet struct {
	Identifier            *[]Identifier `json:"identifier,omitempty"`
	Version               *String       `json:"version,omitempty"`
	Status                *Code         `json:"status,omitempty"`
	ApprovalDate          *DateTime     `json:"approvalDate,omitempty"`
	ApprovingPractitioner *Reference    `json:"approvingPractitioner,omitempty"`
	AppliesTo             *Canonical    `json:"appliesTo,omitempty"`
	Threshold             *[]Threshold  `json:"threshold,omitempty"`
}

// Threshold is an object describing how to interpret Observation- and QuestionnaireResponse resources
type Threshold struct {
	Type              *CodeableConcept `json:"type,omitempty"`
	EffectiveWhen     *[]EffectiveWhen `json:"effectiveWhen,omitempty"`
	EffectiveBehavior *Code            `json:"effectiveBehavior,omitempty"`
}

// EffectiveWhen determines when a threshold is in effect
type EffectiveWhen struct {
	Component            *Coding          `json:"component,omitempty"`
	ValueBoolean         *Boolean         `json:"valueBoolean,omitempty"`
	ValueDecimal         *Decimal         `json:"valueDecimal,omitempty"`
	ValueInteger         *Integer         `json:"valueInteger,omitempty"`
	ValueString          *String          `json:"valueString,omitempty"`
	ValueDate            *Date            `json:"valueDate,omitempty"`
	ValueDateTime        *DateTime        `json:"valueDateTime,omitempty"`
	ValueURI             *URI             `json:"valueURI,omitempty"`
	ValueCodeableConcept *CodeableConcept `json:"valueCodeableConcept,omitempty"`
	ValueQuantity        *Quantity        `json:"valueQuantity,omitempty"`
	ValueReference       *Reference       `json:"valueReference,omitempty"`
	Operator             *Code            `json:"operator,omitempty"`
	// ValueAttachment      Attachment `json:"valueAttachment,omitempty"` - not supported by this library
}

// ContainsSingleValue returns true if effectiveWhen contains a single value and false if it contains 0 or more than one
func (effectiveWhen *EffectiveWhen) ContainsSingleValue() bool {
	noOfValues := 0

	if effectiveWhen.ValueBoolean != nil {
		noOfValues++
	}
	if effectiveWhen.ValueDecimal != nil {
		noOfValues++
	}
	if effectiveWhen.ValueInteger != nil {
		noOfValues++
	}
	if effectiveWhen.ValueString != nil {
		noOfValues++
	}
	if effectiveWhen.ValueDate != nil {
		noOfValues++
	}
	if effectiveWhen.ValueDateTime != nil {
		noOfValues++
	}
	if effectiveWhen.ValueURI != nil {
		noOfValues++
	}
	if effectiveWhen.ValueCodeableConcept != nil {
		noOfValues++
	}
	if effectiveWhen.ValueQuantity != nil {
		noOfValues++
	}
	if effectiveWhen.ValueReference != nil {
		noOfValues++
	}

	if noOfValues == 1 {
		return true
	}
	return false
}

// GetValue returns the first value of a 'value'-field it encounters
func (effectiveWhen *EffectiveWhen) GetValue() interface{} {
	if effectiveWhen.ValueBoolean != nil {
		return effectiveWhen.ValueBoolean
	} else if effectiveWhen.ValueDecimal != nil {
		return effectiveWhen.ValueDecimal
	} else if effectiveWhen.ValueInteger != nil {
		return effectiveWhen.ValueInteger
	} else if effectiveWhen.ValueString != nil {
		return effectiveWhen.ValueString
	} else if effectiveWhen.ValueDate != nil {
		return effectiveWhen.ValueDate
	} else if effectiveWhen.ValueDateTime != nil {
		return effectiveWhen.ValueDateTime
	} else if effectiveWhen.ValueURI != nil {
		return effectiveWhen.ValueURI
	} else if effectiveWhen.ValueCodeableConcept != nil {
		return effectiveWhen.ValueCodeableConcept
	} else if effectiveWhen.ValueQuantity != nil {
		return effectiveWhen.ValueQuantity
	} else if effectiveWhen.ValueReference != nil {
		return effectiveWhen.ValueReference
	}
	return nil
}
