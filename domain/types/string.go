package types

// String is a sequence of Unicode characters
type String string

// CompareWithOperator is a function used to compare a string to another string using an operator
func (str *String) CompareWithOperator(otherStr *String, operator Code) (bool, error) {
	return new(Comparator).SetComparison(str.CompareTo(otherStr)).MatchOperator(operator)
}

// CompareTo compares a string with another string and returns an integer indicating the relationship between this string, and
// the other string.
// if the integer is zero, they are equal
// if the integer is different from zero, they are not equal
// if the integer is nil, they are not comparable beyond =, != and exists
func (str *String) CompareTo(otherStr *String) *int32 {
	var comparison int32
	if (str == nil && otherStr != nil) || (str != nil && otherStr == nil) {
		return nil
	}
	if str == otherStr || *str == *otherStr {
		comparison = 0
	} else {
		return nil
	}

	return &comparison
}
