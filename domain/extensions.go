package domain

import "bitbucket.org/4s/triaging-service/domain/types"

// QuestionnaireResponseExtensions contains the URL's for the systems of the extensions of QuestionnaireResponse
var QuestionnaireResponseExtensions = struct {
	InterpretationURL types.URI
}{
	InterpretationURL: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/111771649/QuestionnaireResponse+interpretation+extension",
}

// OutcomeExtensions contains the URL's for the systems of the extensions shared by Observations and QuestionnaireResponses
var OutcomeExtensions = struct {
	ThresholdSetCanonicalURL types.URI
}{
	ThresholdSetCanonicalURL: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/111706117/Outcome+thresholdSetCanonical+extension",
}

// ThresholdSetExtensions contains the URL's for the systems of the extensions of ThresholdSet
var ThresholdSetExtensions = struct {
	VersionURL               types.URI
	StatusURL                types.URI
	ApprovalDateURL          types.URI
	ApprovingPractitionerURL types.URI
	AppliesToURL             types.URI
	ThresholdURL             types.URI
}{
	VersionURL:               "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100499511/ThresholdSet+version+extension",
	StatusURL:                "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100433991/ThresholdSet+status+extension",
	ApprovalDateURL:          "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100433995/ThresholdSet+approvalDate+extension",
	ApprovingPractitionerURL: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100565062/ThresholdSet+approvingPractitioner+extension",
	AppliesToURL:             "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728906/ThresholdSet+appliesTo+extension",
	ThresholdURL:             "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100499515/ThresholdSet+threshold+extension",
}

// ThresholdExtensions contains the URL's for the systems of the extensions of Threshold (a field of ThresholdSet)
var ThresholdExtensions = struct {
	TypeURL              types.URI
	EffectiveWhenURL     types.URI
	EffectiveBehaviorURL types.URI
}{
	TypeURL:              "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110460929/ThresholdSet+type+extension",
	EffectiveWhenURL:     "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728921/ThresholdSet+effectiveWhen+extension",
	EffectiveBehaviorURL: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728931/ThresholdSet+effectiveBehavior+extension",
}

// EffectiveWhenExtension contains the URL's for the systems of the extensions of EffectiveWhen (a field of Threshold)
var EffectiveWhenExtension = struct {
	ComponentURL            types.URI
	ValueBooleanURL         types.URI
	ValueDecimalURL         types.URI
	ValueIntegerURL         types.URI
	ValueStringURL          types.URI
	ValueDateURL            types.URI
	ValueDateTimeURL        types.URI
	ValueURIURL             types.URI
	ValueCodeableConceptURL types.URI
	ValueQuantityURL        types.URI
	ValueReferenceURL       types.URI
	OperatorURL             types.URI
}{
	ComponentURL:            "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728925/ThresholdSet+component+extension",
	ValueBooleanURL:         "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100565090/ThresholdSet+valueBoolean+extension",
	ValueDecimalURL:         "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110395417/ThresholdSet+valueDecimal+extension",
	ValueIntegerURL:         "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110428187/ThresholdSet+valueInteger+extension",
	ValueStringURL:          "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110395423/ThresholdSet+valueString+extension",
	ValueDateURL:            "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110395427/ThresholdSet+valueDate+extension",
	ValueDateTimeURL:        "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110395433/ThresholdSet+valueDateTime+extension",
	ValueURIURL:             "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110428237/ThresholdSet+valueUri+extension",
	ValueCodeableConceptURL: "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110460977/ThresholdSet+valueCodeableConcept+extension",
	ValueQuantityURL:        "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110460983/ThresholdSet+valueQuantity+extension",
	ValueReferenceURL:       "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/110460998/ThresholdSet+valueReference+extension",
	OperatorURL:             "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100499530/ThresholdSet+operator+extension",
}

// EffectiveWhenCodingSystems contains the URL's for the codingsystems of EffectiveWhen (a field of Threshold)
var EffectiveWhenCodingSystems = struct {
	ComponentQRCodingSystemURL  types.URI
	ComponentObsCodingSystemURL types.URI
}{
	ComponentQRCodingSystemURL:  "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100433987/ThresholdSet+component+coding+system",
	ComponentObsCodingSystemURL: "http://hl7.org/fhir/2018Sep/valueset-observation-codes.html",
}
