package domain

import (
	"errors"

	"bitbucket.org/4s/triaging-service/domain/types"
)

// ConvertToThresholdSet converts a Basic resource with the proper extensions to a ThresholdSet resource
func ConvertToThresholdSet(basic types.Basic) (types.ThresholdSet, error) {
	thresholdSet := types.ThresholdSet{}
	var err error

	// Identifier
	if basic.Identifier != nil {
		thresholdSet.Identifier = basic.Identifier
	} else {
		return types.ThresholdSet{}, errors.New("Basic has no identifier. cannot convert to thresholdset")
	}

	if basic.DomainResource == nil || basic.Extension == nil {
		return types.ThresholdSet{}, errors.New("Basic has no extension-field. cannot convert to thresholdset")
	}

	for _, extension := range *basic.Extension {
		if extension.URL != nil {
			thresholdSet, err = ExtensionToThresholdSetField(thresholdSet, extension)
			if err != nil {
				return types.ThresholdSet{}, err
			}
		}
	}
	if thresholdSet == (types.ThresholdSet{}) {
		return thresholdSet, errors.New("Conversion to thresholdset failed")
	}
	return thresholdSet, nil
}

// ExtensionToThresholdSetField converts an extension to the corresponding field on a ThresholdSet and sets it on the
// provided ThresholdSet
func ExtensionToThresholdSetField(thresholdSet types.ThresholdSet, extension types.Extension) (types.ThresholdSet, error) {
	switch *extension.URL {
	// Version
	case ThresholdSetExtensions.VersionURL:
		if extension.ValueString == nil {
			return types.ThresholdSet{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in version-extension")
		}
		thresholdSet.Version = extension.ValueString
	// Status
	case ThresholdSetExtensions.StatusURL:
		if extension.ValueCode == nil {
			return types.ThresholdSet{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in status-extension")
		}
		thresholdSet.Status = extension.ValueCode
	// ApprovalDate
	case ThresholdSetExtensions.ApprovalDateURL:
		if extension.ValueDateTime == nil {
			return types.ThresholdSet{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in approvalDate-extension")
		}
		thresholdSet.ApprovalDate = extension.ValueDateTime
	// ApprovingPractitioner
	case ThresholdSetExtensions.ApprovingPractitionerURL:
		if extension.ValueReference == nil {
			return types.ThresholdSet{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in approvingPractitioner-extension")
		}
		thresholdSet.ApprovingPractitioner = extension.ValueReference
	// AppliesTo
	case ThresholdSetExtensions.AppliesToURL:
		if extension.ValueCanonical == nil {
			return types.ThresholdSet{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in appliesTo-extension")
		}
		thresholdSet.AppliesTo = extension.ValueCanonical
	// Threshold
	case ThresholdSetExtensions.ThresholdURL:
		threshold := types.Threshold{}
		for _, thresholdExtension := range *extension.Extension {
			var err error
			if thresholdExtension.URL != nil {
				threshold, err = ExtensionToThresholdField(threshold, thresholdExtension)
				if err != nil {
					return types.ThresholdSet{}, err
				}
			}
		}
		if thresholdSet.Threshold == nil || len(*thresholdSet.Threshold) < 1 {
			thresholdArray := []types.Threshold{}
			thresholdSet.Threshold = &thresholdArray
		}
		*thresholdSet.Threshold = append(*thresholdSet.Threshold, threshold)
	}
	return thresholdSet, nil
}

// ExtensionToThresholdField converts an extension to the corresponding field on a Threshold and sets it on the provided
// Threshold
func ExtensionToThresholdField(threshold types.Threshold, extension types.Extension) (types.Threshold, error) {
	switch *extension.URL {
	// Type
	case ThresholdExtensions.TypeURL:
		if extension.ValueCodeableConcept == nil {
			return types.Threshold{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.type-extension")
		}
		threshold.Type = extension.ValueCodeableConcept
	// EffectiveBehavior
	case ThresholdExtensions.EffectiveBehaviorURL:
		if extension.ValueCode == nil {
			return types.Threshold{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveBehavior-extension")
		}
		threshold.EffectiveBehavior = extension.ValueCode
	// EffectiveWhen
	case ThresholdExtensions.EffectiveWhenURL:
		effectiveWhen := types.EffectiveWhen{}
		for _, effectiveWhenExtension := range *extension.Extension {
			var err error
			if extension.URL != nil {
				effectiveWhen, err = ExtensionToEffectiveWhenField(effectiveWhen, effectiveWhenExtension)
				if err != nil {
					return types.Threshold{}, err
				}
			}
		}
		if threshold.EffectiveWhen == nil || len(*threshold.EffectiveWhen) < 1 {
			effectiveWhenArray := []types.EffectiveWhen{}
			threshold.EffectiveWhen = &effectiveWhenArray
		}
		*threshold.EffectiveWhen = append(*threshold.EffectiveWhen, effectiveWhen)
	}
	return threshold, nil
}

// ExtensionToEffectiveWhenField converts an extension to the corresponding field on a Threshold and sets it on the
// provided EffectiveWhen
func ExtensionToEffectiveWhenField(effectiveWhen types.EffectiveWhen, extension types.Extension) (types.EffectiveWhen, error) {
	switch *extension.URL {
	// Component
	case EffectiveWhenExtension.ComponentURL:
		if extension.ValueCoding == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.component-extension")
		}
		effectiveWhen.Component = extension.ValueCoding
	// ValueBoolean
	case EffectiveWhenExtension.ValueBooleanURL:
		if extension.ValueBoolean == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueBoolean = extension.ValueBoolean
	// ValueDecimal
	case EffectiveWhenExtension.ValueDecimalURL:
		if extension.ValueDecimal == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueDecimal = extension.ValueDecimal
	// ValueInteger
	case EffectiveWhenExtension.ValueIntegerURL:
		if extension.ValueInteger == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueInteger = extension.ValueInteger
	// ValueString
	case EffectiveWhenExtension.ValueStringURL:
		if extension.ValueString == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueString = extension.ValueString
	// ValueDate
	case EffectiveWhenExtension.ValueDateURL:
		if extension.ValueDate == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueDate = extension.ValueDate
	// ValueDateTime
	case EffectiveWhenExtension.ValueDateTimeURL:
		if extension.ValueDateTime == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueDateTime = extension.ValueDateTime
	// ValueURI
	case EffectiveWhenExtension.ValueURIURL:
		if extension.ValueURI == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueURI = extension.ValueURI
	// ValueCodeableConcept
	case EffectiveWhenExtension.ValueCodeableConceptURL:
		if extension.ValueCodeableConcept == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueCodeableConcept = extension.ValueCodeableConcept
	// ValueQuantity
	case EffectiveWhenExtension.ValueQuantityURL:
		if extension.ValueQuantity == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueQuantity = extension.ValueQuantity
	// ValueReference
	case EffectiveWhenExtension.ValueReferenceURL:
		if extension.ValueReference == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.ValueReference = extension.ValueReference
	// Operator
	case EffectiveWhenExtension.OperatorURL:
		if extension.ValueCode == nil {
			return types.EffectiveWhen{}, errors.New("Error converting Basic to ThresholdSet: Malformed JSON. Missing value in threshold.effectiveWhen.value-extension")
		}
		effectiveWhen.Operator = extension.ValueCode
	}
	return effectiveWhen, nil
}
