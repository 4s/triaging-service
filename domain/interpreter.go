package domain

import (
	"context"
	"errors"
	"fmt"
	"reflect"

	log "bitbucket.org/4s/go-logging"
	"bitbucket.org/4s/triaging-service/domain/types"
)

var interpretationSystem = types.URI("http://hl7.org/fhir/v2/0078")

//////////////////////////////////////////////////////////////////////////////////////////////////////
//																																																	//
// 																	         OBSERVATION																						//
// 																																																	//
//////////////////////////////////////////////////////////////////////////////////////////////////////

// InterpretObservation calculates an interpretation based on the provided Observation and ThresholdSet
func InterpretObservation(
	ctx context.Context,
	observation types.Observation,
	thresholdSet types.ThresholdSet,
) (types.CodeableConcept, error) {

	if thresholdSet.Threshold == nil || len(*thresholdSet.Threshold) < 1 {
		return types.CodeableConcept{}, errors.New("No thresholds in ThresholdSet")
	}

	effectiveTypes := []types.Code{}

	// loop through thresholds
	for _, threshold := range *thresholdSet.Threshold {

		// effectiveBehavior is any unless "all" is specified
		inEffect := false
		if threshold.EffectiveBehavior != nil && *threshold.EffectiveBehavior == "all" {
			inEffect = true
		}

		if threshold.EffectiveWhen == nil || len(*threshold.EffectiveWhen) < 1 {
			continue
		}

		// loop through effectiveWhens
		exists := false
		for _, effectiveWhen := range *threshold.EffectiveWhen {
			if effectiveWhen.Operator == nil || *effectiveWhen.Operator == "" {
				break
			}
			if !effectiveWhen.ContainsSingleValue() {
				break
			}
			match := false
			var err error
			if effectiveWhen.Component == nil || *effectiveWhen.Component == (types.Coding{}) {
				exists = true
				// handle different valuetypes
				match, err = HandleTypes(observation.GetValue(), effectiveWhen.GetValue(), *effectiveWhen.Operator)
				if err != nil {
					log.WithContext(ctx).Errorf(err.Error())
				}
				if observation.Code == nil ||
					*(observation).Code == (types.CodeableConcept{}) ||
					len(*(*(observation).Code).Coding) == 0 ||
					(*(*(observation).Code).Coding)[0] == (types.Coding{}) ||
					(*(*(observation).Code).Coding)[0].Code == nil ||
					*(*(*(observation).Code).Coding)[0].Code == "" {
					log.WithContext(ctx).Debug("The value in the Observation and the value in the EffectiveWhen was found to match")
				} else {
					log.WithContext(ctx).Debugf("The value in the Observation with code '%v' and the value in the EffectiveWhen was found to match", *(*(*(observation).Code).Coding)[0].Code)
				}
			} else if *effectiveWhen.Component.System == EffectiveWhenCodingSystems.ComponentObsCodingSystemURL && *effectiveWhen.Component.Code == *(*(*(observation).Code).Coding)[0].Code {
				match, err = HandleTypes(observation.GetValue(), effectiveWhen.GetValue(), *effectiveWhen.Operator)
				if err != nil {
					log.WithContext(ctx).Errorf(err.Error())
				}
			} else if observation.Component != nil && len(*observation.Component) != 0 {
				for _, component := range *observation.Component {

					// check if component has codings
					if component.Code == nil ||
						*(component).Code == (types.CodeableConcept{}) ||
						len(*(*(component).Code).Coding) == 0 {
						continue
					}

					proceed := false

					// check if any of the components codings matches effectiveWhen's component
					for _, coding := range *(*(component).Code).Coding {
						if coding.System != nil &&
							*(coding).System != "" &&
							coding.Code != nil &&
							*(coding).Code != "" &&
							*effectiveWhen.Component.System == *(coding).System &&
							*effectiveWhen.Component.Code == *(coding).Code {
							proceed = true
						}
					}

					if !proceed {
						continue
					}

					exists = true
					match, err = HandleTypes(component.GetValue(), effectiveWhen.GetValue(), *effectiveWhen.Operator)
					if err != nil {
						log.WithContext(ctx).Errorf(err.Error())
					}
					if match {
						log.WithContext(ctx).Debugf("The value of the component of the Observation, matches the value of the EffectiveWhen for Observation with code '%v'", *(*(*(component).Code).Coding)[0].Code)
					}
				}
			}
			if threshold.EffectiveBehavior != nil && *threshold.EffectiveBehavior == "all" {
				inEffect = inEffect && match
			} else {
				inEffect = inEffect || match
			}
		}
		// if observation has no value, the effectiveWhen is not in effect
		if !exists {
			if threshold.EffectiveBehavior != nil && *threshold.EffectiveBehavior == "all" {
				inEffect = false
			} else {
				inEffect = inEffect || false
			}
		}
		// if a match was found, save the type of the threshold
		if threshold.Type == nil ||
			*threshold.Type == (types.CodeableConcept{}) ||
			threshold.Type.Coding == nil ||
			len(*threshold.Type.Coding) < 1 {
			continue
		}
		if inEffect {
			for _, coding := range *threshold.Type.Coding {
				if coding.Code != nil && *coding.Code != "" {
					if inEffect {
						log.WithContext(ctx).Debugf("The following type was found to be inEffect: %v", *coding.Code)
					}
					effectiveTypes = append(effectiveTypes, *coding.Code)
				}
			}
		}
	}
	log.WithContext(ctx).Debugf("The following interpretations were found to be in effect '%v'", effectiveTypes)
	interpretation, err := CreateInterpretationFromEffectiveTypes(effectiveTypes)

	return *interpretation, err
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//																																																	//
// 																	     QUESTIONNAIRERESPONSE																			//
// 																																																	//
//////////////////////////////////////////////////////////////////////////////////////////////////////

// InterpretQuestionnaireResponse calculates an interpretation based on the provided QuestionnaireResponse and ThresholdSet
func InterpretQuestionnaireResponse(
	ctx context.Context,
	questionnaireResponse types.QuestionnaireResponse,
	thresholdSet types.ThresholdSet,
) (types.CodeableConcept, error) {

	if thresholdSet.Threshold == nil || len(*thresholdSet.Threshold) < 1 {
		return types.CodeableConcept{}, errors.New("No thresholds in ThresholdSet")
	}

	effectiveTypes := []types.Code{}
	if questionnaireResponse.Item == nil || len(*questionnaireResponse.Item) < 1 {
		return types.CodeableConcept{}, errors.New("No items in QuestionnaireResponse")
	}

	// loop through thresholds
	for _, threshold := range *thresholdSet.Threshold {

		// effectiveBehavior is "any" unless "all" is specified
		inEffect := false
		if threshold.EffectiveBehavior != nil && *threshold.EffectiveBehavior == "all" {
			inEffect = true
		}

		if threshold.EffectiveWhen == nil || len(*threshold.EffectiveWhen) < 1 {
			continue
		}

		// loop through effectiveWhens
		for _, effectiveWhen := range *threshold.EffectiveWhen {
			var exists bool
			var err error

			if effectiveWhen.Operator == nil || *effectiveWhen.Operator == "" {
				break
			}
			if !effectiveWhen.ContainsSingleValue() {
				continue
			}

			// loop through questionniare response items
			inEffect, exists, err = LoopThroughItems(
				ctx,
				*questionnaireResponse.Item,
				effectiveWhen,
				threshold.EffectiveBehavior,
				inEffect,
			)
			if err != nil {
				return types.CodeableConcept{}, err
			}

			// if the item wasn't found, this effectiveWhen is not in effect
			if !exists {
				if threshold.EffectiveBehavior != nil && *threshold.EffectiveBehavior == "all" {
					inEffect = false
				} else {
					inEffect = inEffect || false
				}
			}
		}

		// if a match was found, save the type of the threshold
		if threshold.Type == nil ||
			*threshold.Type == (types.CodeableConcept{}) ||
			threshold.Type.Coding == nil ||
			len(*threshold.Type.Coding) < 1 {
			continue
		}
		if inEffect {
			for _, coding := range *threshold.Type.Coding {
				if coding.Code != nil && *coding.Code != "" {
					log.WithContext(ctx).Debugf("The following type was found to be inEffect: %v", *coding.Code)
					effectiveTypes = append(effectiveTypes, *coding.Code)
				}
			}
		}
	}
	interpretation, err := CreateInterpretationFromEffectiveTypes(effectiveTypes)

	return *interpretation, err
}

// LoopThroughItems loops through a nested QuestionnaireResponseItem slice until it finds the questionnaire response
// item with the linkId specified in the component-field of the effectiveWhen, and passes the value on to the
// HandleTypes method.
// The method returns three values. A boolean indicating whether the effectiveWhen is in effect, a boolean indicating
// whether the item specified was found and an error if one occurs.
func LoopThroughItems(
	ctx context.Context,
	items []types.QuestionnaireResponseItem,
	effectiveWhen types.EffectiveWhen,
	effectiveBehavior *types.Code,
	inEffect bool) (bool, bool, error) {
	exists := false
	for _, item := range items {
		if item.LinkID == nil || *item.LinkID == "" {
			continue
		}

		// if item contains no answers and no subitems, continue
		if (item.Answer == nil || len(*item.Answer) < 1) && (item.Item == nil || len(*item.Item) < 1) {
			continue
		}

		if effectiveWhen.Component == nil || *effectiveWhen.Component == (types.Coding{}) || *effectiveWhen.Component.System != EffectiveWhenCodingSystems.ComponentQRCodingSystemURL {
			continue
		}
		componentCode := *effectiveWhen.Component.Code
		if string(componentCode) == "" {
			continue
		}

		if item.Answer != nil && len(*item.Answer) > 0 {
			for _, answer := range *item.Answer {
				if string(*item.LinkID) == string(componentCode) {

					exists = true
					// handle different valuetypes
					match, err := HandleTypes(answer.GetValue(), effectiveWhen.GetValue(), *effectiveWhen.Operator)
					if err != nil {
						log.WithContext(ctx).Errorf(err.Error())
					}
					if match {
						log.WithContext(ctx).Debugf("A match in the ThresholdSet was found for item with linkid '%v'", *item.LinkID)
					}

					if effectiveBehavior != nil && *effectiveBehavior == "all" {
						inEffect = inEffect && match
					} else {
						inEffect = inEffect || match
					}
				} else if answer.Item != nil && len(*answer.Item) > 0 {
					var err error
					inEffect, exists, err = LoopThroughItems(ctx, *answer.Item, effectiveWhen, effectiveBehavior, inEffect)
					if err != nil {
						return false, false, err
					}
				} else {
					break
				}
			}
		}

		if !exists && item.Item != nil && len(*item.Item) > 0 {
			var err error
			inEffect, exists, err = LoopThroughItems(ctx, *item.Item, effectiveWhen, effectiveBehavior, inEffect)
			if err != nil {
				return false, false, err
			}
		}
	}
	return inEffect, exists, nil
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//																																																	//
// 																	     				 HELPERS																						//
// 																																																	//
//////////////////////////////////////////////////////////////////////////////////////////////////////

// HandleTypes finds the type of the value of both the outcome and the effectiveWhen and compares them in relation to
// operator, and returns a boolean indicating whether the effectiveWhen is in effect.
func HandleTypes(value interface{}, comparativeValue interface{}, operator types.Code) (bool, error) {
	switch reflect.TypeOf(value).String() {
	// REFERENCE
	case "*types.Reference":
		if reflect.TypeOf(comparativeValue).String() != "*types.Reference" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Reference)
		effectiveWhenValue := comparativeValue.(*types.Reference)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
	// CODING
	case "*types.Coding":
		if reflect.TypeOf(comparativeValue).String() != "*types.Coding" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Coding)
		effectiveWhenValue := comparativeValue.(*types.Coding)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
	// URI
	case "*types.URI":
		if reflect.TypeOf(comparativeValue).String() != "*types.URI" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.URI)
		effectiveWhenValue := comparativeValue.(*types.URI)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
	// DECIMAL
	case "*types.Decimal":
		if reflect.TypeOf(comparativeValue).String() != "*types.Decimal" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Decimal)
		effectiveWhenValue := comparativeValue.(*types.Decimal)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
	// DATE
	case "*types.Date":
		if reflect.TypeOf(comparativeValue).String() != "*types.Date" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Date)
		effectiveWhenValue := comparativeValue.(*types.Date)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
	// QUANTITY
	case "*types.Quantity":
		if reflect.TypeOf(comparativeValue).String() != "*types.Quantity" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Quantity)
		effectiveWhenValue := comparativeValue.(*types.Quantity)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
		// CODEABLECONCEPT
	case "*types.CodeableConcept":
		if reflect.TypeOf(comparativeValue).String() != "*types.CodeableConcept" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.CodeableConcept)
		effectiveWhenValue := comparativeValue.(*types.CodeableConcept)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
		// STRING
	case "*types.String":
		if reflect.TypeOf(comparativeValue).String() != "*types.String" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.String)
		effectiveWhenValue := comparativeValue.(*types.String)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
		// BOOLEAN
	case "*types.Boolean":
		if reflect.TypeOf(comparativeValue).String() != "*types.Boolean" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Boolean)
		effectiveWhenValue := comparativeValue.(*types.Boolean)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
		// INTEGER
	case "*types.Integer":
		if reflect.TypeOf(comparativeValue).String() != "*types.Integer" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Integer)
		effectiveWhenValue := comparativeValue.(*types.Integer)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
		// RANGE
	case "*types.Range":
		if reflect.TypeOf(comparativeValue).String() != "*types.Range" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Range)
		effectiveWhenValue := comparativeValue.(*types.Range)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
		// TIME
	case "*types.Time":
		if reflect.TypeOf(comparativeValue).String() != "*types.Time" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Time)
		effectiveWhenValue := comparativeValue.(*types.Time)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
		// DATETIME
	case "*types.DateTime":
		if reflect.TypeOf(comparativeValue).String() != "*types.DateTime" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.DateTime)
		effectiveWhenValue := comparativeValue.(*types.DateTime)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
		// PERIOD
	case "*types.Period":
		if reflect.TypeOf(comparativeValue).String() != "*types.Period" {
			return false, fmt.Errorf(
				"Outcome value is of type %v, but effectiveWhen value is of type %v",
				reflect.TypeOf(value).String(),
				reflect.TypeOf(comparativeValue).String(),
			)
		}
		outcomeValue := value.(*types.Period)
		effectiveWhenValue := comparativeValue.(*types.Period)
		return outcomeValue.CompareWithOperator(effectiveWhenValue, operator)
	default:
		return false, errors.New("Unknown value type")
	}
}

// CreateInterpretationFromEffectiveTypes accepts an array of possible types that are in effect and returns an
// interpretation with the most urgent of them
func CreateInterpretationFromEffectiveTypes(effectiveTypes []types.Code) (*types.CodeableConcept, error) {
	coding := types.Coding{}
	coding.System = &interpretationSystem

	if len(effectiveTypes) < 1 {
		return new(types.CodeableConcept), errors.New("No interpretations were found to be in effect")
	}
	for _, effectiveType := range effectiveTypes {
		switch effectiveType {
		case "pathological":
			pathologicalCode := types.Code("AA")
			pathologicalDisplay := types.String("Pathological")
			coding.Code = &pathologicalCode
			coding.Display = &pathologicalDisplay
		case "abnormal":
			if coding.Code == nil || *coding.Code != "AA" {
				abnormalCode := types.Code("A")
				abnormalDisplay := types.String("Abnormal")
				coding.Code = &abnormalCode
				coding.Display = &abnormalDisplay
			}
		case "normal":
			if coding.Code == nil || (*coding.Code != "A" && *coding.Code != "AA") {
				normalCode := types.Code("N")
				normalDisplay := types.String("Normal")
				coding.Code = &normalCode
				coding.Display = &normalDisplay
			}
		}
	}

	if coding.Code == nil {
		insufficientEvidenceCode := types.Code("IE")
		insufficientEvidenceDisplay := types.String("Insufficient evidence")
		coding.Code = &insufficientEvidenceCode
		coding.Display = &insufficientEvidenceDisplay
	}

	interpretation := new(types.CodeableConcept)
	codings := new(types.Codings)
	*codings = append(*codings, coding)
	interpretation.Coding = codings

	return interpretation, nil
}
