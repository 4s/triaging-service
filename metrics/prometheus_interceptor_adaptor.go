package metrics

import (
	"context"
	"errors"
	"os"

	"github.com/prometheus/client_golang/prometheus/push"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	"github.com/prometheus/client_golang/prometheus"
)

// PrometheusInterceptorAdaptor is an implementation of go-messaging's EventConsumerInterceptor that pushes metrics to a prometheus server.
type PrometheusInterceptorAdaptor struct {
	messaging.EventConsumerInterceptorAdaptor
	ctx           context.Context
	pushGateway   *push.Pusher
	requestTimers map[string]*prometheus.Timer
	requestsTotal *prometheus.CounterVec
	responseTime  *prometheus.SummaryVec
}

// NewPrometheusInterceptorAdaptor functions a constructor that returns a new instance of PrometheusInterceptorAdaptor
func NewPrometheusInterceptorAdaptor(ctx context.Context) (PrometheusInterceptorAdaptor, error) {
	requestsTotal := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "requests_total",
			Help: "Total requests.",
		},
		[]string{"method", "service", "handler", "status"},
	)
	responseTime := prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "response_time",
			Help: "Total response time.",
		},
		[]string{"method", "service", "handler", "status"},
	)

	var pusher *push.Pusher
	if os.Getenv("PROMETHEUS_PUSHGATEWAY") != "" {
		pusher = push.New(os.Getenv("PROMETHEUS_PUSHGATEWAY"), os.Getenv("PROMETHEUS_JOB"))
	} else {
		err := errors.New("PROMETHEUS_PUSHGATEWAY is undefined")
		return PrometheusInterceptorAdaptor{}, err
	}

	pusher.Collector(requestsTotal)
	pusher.Collector(responseTime)

	return PrometheusInterceptorAdaptor{
		messaging.EventConsumerInterceptorAdaptor{},
		ctx,
		pusher,
		map[string]*prometheus.Timer{},
		requestsTotal,
		responseTime,
	}, nil
}

// IncomingMessagePreProcessMessage is called for each message, immediately after parsing of incoming topic and message strings and
// before any other processing takes place for each incoming message. It may e.g. be used to provide alternate processing of incoming messages.
func (eventConsumerInterceptorAdaptor PrometheusInterceptorAdaptor) IncomingMessagePreProcessMessage(consumedTopic messaging.Topic, receivedMessage messaging.Message) bool {
	if receivedMessage.GetTransactionID() != "" {
		//Register timer, to time the request
		eventConsumerInterceptorAdaptor.requestTimers[receivedMessage.GetTransactionID()] = prometheus.NewTimer(nil)
	} else {
		log.WithContext(eventConsumerInterceptorAdaptor.ctx).Warn("No request timer metrics for received message - transaction id is missing")
	}

	return true
}

// HandleError is called upon any exception being thrown within the EventConsumer's processing code.
// This includes parse exceptions and message incompatibility exceptions.
func (eventConsumerInterceptorAdaptor PrometheusInterceptorAdaptor) HandleError(consumedTopic messaging.Topic, receivedMessage messaging.Message, err error) bool {
	eventConsumerInterceptorAdaptor.processingCompletedMetrics(consumedTopic, receivedMessage, "error")
	return true
}

// ProcessingCompletedNormally is called after all processing of an incoming message is completed, but only if the
// request completes normally.
func (eventConsumerInterceptorAdaptor PrometheusInterceptorAdaptor) ProcessingCompletedNormally(consumedTopic messaging.Topic, receivedMessage messaging.Message) {
	eventConsumerInterceptorAdaptor.processingCompletedMetrics(consumedTopic, receivedMessage, "processingCompletedNormally")
}

func (eventConsumerInterceptorAdaptor PrometheusInterceptorAdaptor) processingCompletedMetrics(consumedTopic messaging.Topic, receivedMessage messaging.Message, status string) {
	if eventConsumerInterceptorAdaptor.pushGateway == nil {
		return
	}

	method := consumedTopic.String()
	service := os.Getenv("SERVICE_NAME")
	handler := receivedMessage.GetBodyType()

	labels := prometheus.Labels{}
	labels["method"] = method
	labels["service"] = service
	labels["handler"] = handler
	labels["status"] = status

	eventConsumerInterceptorAdaptor.requestsTotal.With(labels).Inc()

	if receivedMessage.GetTransactionID() != "" {
		timer := eventConsumerInterceptorAdaptor.requestTimers[receivedMessage.GetTransactionID()]
		if timer != nil {
			eventConsumerInterceptorAdaptor.responseTime.With(labels).Observe(float64(timer.ObserveDuration()))
			delete(eventConsumerInterceptorAdaptor.requestTimers, receivedMessage.GetTransactionID())
		} else {
			log.WithContext(eventConsumerInterceptorAdaptor.ctx).Warnf("No metrics timer found for transaction id = %v", receivedMessage.GetTransactionID())
		}
	}

	err := eventConsumerInterceptorAdaptor.pushGateway.Push()
	if err != nil {
		log.WithContext(eventConsumerInterceptorAdaptor.ctx).Error("An error occurred while pushing the metrics", err)
	}
}
