module bitbucket.org/4s/triaging-service

go 1.14

require (
	bitbucket.org/4s/go-logging v0.0.0-20191021170502-e989fc70bba0
	bitbucket.org/4s/go-messaging v0.0.0-20191021164707-2a96f565ecd1
	bitbucket.org/4s/go-stability v0.0.0-20190218131603-1dd80fab4437
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/confluentinc/confluent-kafka-go v1.3.0
	github.com/davecgh/go-spew v1.1.1
	github.com/google/go-cmp v0.4.0
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/onrik/logrus v0.5.1 // indirect
	github.com/prometheus/client_golang v1.5.1
)
